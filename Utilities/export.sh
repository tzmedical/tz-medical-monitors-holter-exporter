# This script contains example parameters for using the holter exporter from the command line.
#
# Usage: ./export.sh

'Aera CT Holter Exporter.exe' -o "./$1.dat" -i "./ecgs" --format wfdb32 --channels 1,2 --sample-rate 300 --lsb 3125 --start-time 5 --length 10

