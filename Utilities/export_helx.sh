#!/bin/bash
#
# This script runs the holter converter 3 times, once for each channel,
# for use in exporting to the helx format (which requires one channel per file).
#
# Usage: ./export_helx.sh

for i in 1 2 3; do
    '/cygdrive/c/Users/jsigrist/algorithm_validation/database_builder/holter_exporter/Aera CT Holter Exporter/Release/Aera CT Holter Exporter.exe' --output "C:/Users/jsigrist/TZM_Database/${1}_${i}.dat" -i ./ecgs --format helx --channels ${i}
done
