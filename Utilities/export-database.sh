#!/bin/bash
#
# This program exports all the records from h3r-data-archive. 
# Before using it, edit the export.sh
# script to make sure it has the correct parameters.
#
# Usage: ./export-database.sh

for D in ./*; do
    if [ -d "${D}" ]; then
        echo "Exporting ${D}..."
        cd $D
        export.sh $D
        cd ..
        echo "Done"
    fi
done
