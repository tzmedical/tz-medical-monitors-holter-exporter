#include <stdio.h>
#include <wfdb/wfdb.h>
#include <wfdb/ecgmap.h>
#include <string.h>
#include <inttypes.h>

#define DEFAULT_ANNOTATION ("atr")

#define REQUIRED_ARGS (3)

/*
 * This program converts files from the WFDB annotation format to 
 * a format usable by Northeast Monitoring.
 *
 * The output format is a series of pairs of values (each pair consists
 * of two four byte unsigned integers, stored least significant byte first).
 * The first value is the number of samples since the beginning of the file 
 * the event occurred at. The second value is a label for the event type.
 *
 * The input format is described here:
 * http://www.physionet.org/physiotools/wag/signal-5.htm
 *
 * This program required the wfdb library to be installed in order
 * to run:
 * http://www.physionet.org/physiotools/wfdb.shtml
 *
 * Compile with:
 * gcc ann_convert.c -o ann_convert -lwfdb
 *
 * Company: TZ Medical
 * Author: Jeremy Sigrist
 * Date: 7/15/2016
 */


int main(int argc, char* argv[])
{
    char* record = NULL;

    WFDB_Anninfo ai;
    WFDB_Annotation annot;
    FILE* outfile;
    uint32_t label;
    uint32_t samples;

    if (argc != REQUIRED_ARGS)
    {
        fprintf(stderr, "Usage: %s [record] [output filename]\n", argv[0]);
        exit(1);
    }

    record = argv[1];
    ai.stat = WFDB_READ;
    ai.name = DEFAULT_ANNOTATION;

    outfile = fopen(argv[2], "wb");

    ai.stat = WFDB_READ;
    if (annopen(record, &ai, 1) < 0)	/* open annotation file */
    {
        fprintf(stderr, "Error opening annotation for record %s\n", record);
	exit(1);
    }
    
    // This correctly prints the sample count and type of annotation
    while (getann(0, &annot) == 0)
    {
        fprintf(stdout, "%d %d\n", annot.time, annot.anntyp);
        samples = annot.time;
        label = annot.anntyp;
        
        // Write time (samples from beginning of file)
        fwrite(&samples, 4, 1, outfile);

        // TODO: Convert label from wfdb format to Northeast Monitoring format

        // Write label
        fwrite(&label, 4, 1, outfile);
    }

    fclose(outfile);

    return 0;
}

