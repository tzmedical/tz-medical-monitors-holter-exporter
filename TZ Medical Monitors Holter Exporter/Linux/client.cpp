
#include <sstream>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <iostream>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ftw.h>

#include "stdlib.h"
#include "unistd.h"
#include "scp_parser.h"

// The number of valid formats supported by the application.
#define NUM_FORMATS (4)
#define REQUIRED_ARGS (3)
#define NUM_CHANNELS (12)
#define VERSION_NUMBER (1.0)
#define SCP_EXTENSION (".scp")

std::vector<std::string> file_list;

bool hasEnding (std::string const &fullString, std::string const &ending)
{
  if (fullString.length() >= ending.length()) {
    return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
  } else {
    return false;
  }
}

int file_callback(const char* fpath, const struct stat *sb, int typeflag)
{
    // file list should be cleared prior to calling ftw.
    
    // Add all regular files (not directories) ending with scp to the file list. 
    if ((sb->st_mode & S_IFREG) && (hasEnding(fpath, SCP_EXTENSION)))
    {
        file_list.push_back(fpath);
    }
    
    return 0;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim))
  {
    elems.push_back(item);
  }
  return elems;
}


std::vector<std::string> split(const std::string &s, char delim)
{
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}


bool dirExists(std::string pathname)
{
  struct stat info;
  if (stat(pathname.c_str(), &info) != 0 || !(info.st_mode & S_IFDIR))
  {
    return false;
  }
  return true;    // this is not a directory!
}

void print_usage()
{
  std::cout << "Usage: TZ Medical Monitors Holter Exporter [-c 1,2,3] -f format -o output_file\n\t [-i input_folder]" << std::endl;
  std::cout << "Use --help for more information." << std::endl;
}

void print_help()
{
  std::cout << "TZ Medical Monitors Holter Exporter (version ";
  std::cout << VERSION_NUMBER << ")" << std::endl;

  std::cout << "\nUsage: TZ Medical Monitors Holter Exporter [-c 1,2,3] -f format -o output_file\n\t [-i input_folder]" << std::endl;
  std::cout << "\nRun with no arguments for a GUI." << std::endl;
  std::cout << " --help  \tPrint usage and exit." << std::endl;
  std::cout << " --format \tThe format to use for the output file.\n\t\tOptions are: wfdb8, wfdb16, wfdb212, ishne." << std::endl;
  std::cout << " --channels \tSelect which channels to export.\n\t\tshould be a comma separated list of numbers of the form 1,2,3.\n\t\tNonexistant channels are ignored. Optional, default is all channels." << std::endl;
  std::cout << " --output \tThe name of the file in which to store the exported data." << std::endl;
  std::cout << " --input-folder\tThe location of the ecgs folder containing the data to export. \n\t\tEither drive or input-folder must be specified." << std::endl;
  std::cout << " --max-hours \tThe maximum number of hours worth of data to export.\n\t\tAccepts integer and floating point values. Optional, default is no limit." << std::endl;
}

int export_data(std::string folder, std::string output_filename, std::string format, std::map<uint32_t, bool> channels, double max_hours)
{
  SCP_Parser parser;
  std::string key = "";
  std::string iv = "";
  header_file_info header_info;
  uint32_t ret_val = 0;
  file_format_t file_format = parser.string_to_format(format);
  std::string file;
  std::string extension = ".scp";
  if (NUM_FORMATS == file_format)
  {
    return INVALID_FORMAT;
  }

  file_list.clear();
  ftw(folder.c_str(), file_callback, 20);

  ret_val = parser.export_file_list(output_filename, file_list, 
      file_format, channels, key.c_str(), 
      iv.c_str(), true, max_hours);

  switch (ret_val)
  {
    case FILE_NOT_FOUND:
      std::cout << "The specified input folder could not be found." << std::endl;
      break;
    case CRC_INVALID:
      std::cout << "A file contained an incorrect CRC value." << std::endl;
      break;
    case INVALID_FORMAT:
      std::cout << "The output format specified is invalid." << std::endl;
      break;
    case FILE_IO_ERROR:
      std::cout << "An IO error occurred when writing to the output file." << std::endl;
      break;
    case CANCELLED:
      std::cout << "Cancelled." << std::endl;
      break;
    case UNEXPECTED_FILE_CONTENTS:
      std::cout << "A file's contents did not match the expected format." << std::endl;
      break;
    case NO_SCP_FILES_FOUND:
      std::cout << "The requested folder did not contain any scp files." << std::endl;
      break;
    case WFDB212_TOO_MANY_CHANNELS:
      std::cout << "Please ensure that exactly 2 channels are selected for wfdb212 format." << std::endl;
      break;
    case ARITHMETIC_SHIFTS_NOT_SUPPORTED:
      std::cout << "Arithmetic Shifting not supported by this compiler. Format wfdb212 not supported." << std::endl;
      break;
    default:
      std::cout << "Export complete." << std::endl;
      break;
  }
  return ret_val;
}

int main(int argc, char* argv[])
{
  // If you change this, be sure to update NUM_FORMATS in TZ Medical Monitors Holter Exporter.h
  const char * valid_formats[] = {"wfdb8", "wfdb16", "wfdb212", "ishne"};

  char* temp = NULL;
  std::vector<std::string> channel_list;
  SCP_Parser parser;

  // keep track of if we got enough arguments. 
  int num_arguments = 0;

  std::string folder;
  std::string drive;
  std::string format;
  std::string output_file;
  double max_hours = NO_LIMIT;
  std::map<uint32_t, bool> channels;

  static struct option long_options[] = 
  {
    { ("format"), required_argument, 0, ('f') },
    { ("output"), required_argument, 0, ('o') },
    { ("channels"), required_argument, 0, ('c') },
    { ("input-folder"), required_argument, 0, ('i') },
    { ("max-hours"), required_argument, 0, ('m') },
    { ("help"), no_argument, 0, ('h') },
    { 0, 0, 0, 0 }
  };

  int option_index = 0;
  int c = 0;

  while ((c = getopt_long(argc, argv, ("f:o:c:i:d:"), long_options, &option_index)) != -1)
  {
    switch (c)
    {
      // Process the output file case.
      case ('o'):
        output_file = (optarg);
        if (0 == output_file.size())
        {
          std::cout << "Output filename must be specified." << std::endl;
          return 1;
        }
        ++num_arguments;
        break;

        // Process the input file case.
      case ('i'):
        if (!dirExists(optarg))
      {
        std::cout << "Input folder cannot be found." << std::endl;
        return -1;
      }
        folder = optarg;
        ++num_arguments;
        break;

        // Process the help case.
      case ('h'):
        print_help();
        return -1;
        break;

        // Process the format case.
      case ('f'):

        if (NUM_FORMATS == parser.string_to_format(optarg))
        {
          std::cout << "Invalid Format (use --help for a list of valid formats)" << std::endl;
          return 1;
        }
        format = optarg;

        ++num_arguments;
        break;

        // Process the channels case.
      case ('c'):
        // Any channel that is present in the argument is valid, so add those
        // to the dictionary as true, and everything else as false.
        channel_list = split(optarg, ',');

        // <= since channels are 1 based.
        for (int i = 1; i <= NUM_CHANNELS; ++i)
        {
          channels[i] = std::find(channel_list.begin(), channel_list.end(), std::to_string(i)) != channel_list.end();
        }

        break;

      case ('m'):
        try
      {
        max_hours = std::max(0.0, std::stod(optarg));
      }
        catch (std::invalid_argument)
        {
          print_usage();
          return 1;
        }
        catch (std::out_of_range)
        {
          max_hours = INT32_MAX;
          std::cout << "Warning: max-hours value out of range. Truncating to " << INT32_MAX << "." << std::endl;
        }
        break;

        // If there was a missing argument.
      case (':'):
        print_usage();
        return 1;
        break;

        // Some other error.
      case ('?'):
        print_usage();
        return 1;
        break;

        // Shouldn't happen.
      default:
        print_usage();
        return 1;
        break;
    }
  }

  if (num_arguments < REQUIRED_ARGS)
  {
    std::cout << "Not enough arguments were provided." << std::endl;
    print_usage();
    return 1;
  }

  // If channels hasn't already been set (meaning it wasn't specified in the
  // argument list), set all channels to true.
  if (0 == channels.size())
  {
    for (int i = 1; i <= NUM_CHANNELS; ++i)
    {
      channels[i] = true;
    }
  }

  // Export the data
  return export_data(folder, output_file, format, channels, max_hours);

  return 0;
}


