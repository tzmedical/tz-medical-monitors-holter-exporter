/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*
*
*
*****************************************************************************/

#ifndef SCP_PARSER_H
#define SCP_PARSER_H

#include <memory>
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <inttypes.h>
#include <vector>
#include <algorithm>
#include <map>
#include <math.h>
#include <ctime>

#include "aes.h"

// Maximum number of leads allowed in a file.
#define MAX_LEADCOUNT (12)

// Extention to use for header files
#define WFDB_HEADER_EXTENSION (".hea")

// The number of seconds in an hour.
#define SEC_PER_HOUR (3600)

// The number of seconds in 3 months
#define SEC_IN_3_MONTHS (7884000)

// The value used to represent that there is no limit on the number of hours of data to export.
#define NO_LIMIT (-1)

// The average length of an SCP file, for marking abnormally long gaps in the record.
#define AVG_FILE_LENGTH (10)

// The start location for the data in an ishne file.
#define ISHNE_DATA_START (522)

// Denotes that no lead is present in an ishne header.
#define ISHNE_NO_LEAD_PRESENT (-9)

// The size, in bytes, of a WAVE header.
#define WAVE_HEADER_SIZE (44)

// Standard sample rate for wave files
#define WAVE_SAMPLE_RATE (44100)

class SCP_Parser;
class header_file_info;

typedef enum {
  FILE_NOT_FOUND = 1,
  CRC_INVALID,
  INVALID_FORMAT,
  FILE_IO_ERROR,
  CANCELLED,
  UNEXPECTED_FILE_CONTENTS,
  NO_SCP_FILES_FOUND,
  WFDB212_TOO_MANY_CHANNELS,
  ARITHMETIC_SHIFTS_NOT_SUPPORTED,
  UNKNOWN_ERROR,
  NUM_ERROR_FLAGS
} scp_parser_error_flag_t;

typedef enum
{
  FILE_FORMAT_WFDB8 = 0,
  FILE_FORMAT_WFDB16,
  FILE_FORMAT_WFDB32,
  FILE_FORMAT_WFDB212,
  FILE_FORMAT_ISHNE,
  FILE_FORMAT_WAVE,
  NUM_FORMATS
} file_format_t;

// Typedef for functions to write square waves.
typedef uint32_t(SCP_Parser::*write_square_wave_function)(std::ofstream& outfile, 
    uint32_t num_channels, double frequency_hz, uint32_t duration_seconds, 
    std::vector<int32_t>& current_val, int32_t low_val, int32_t high_val);

// Typedef for functions to write data.
typedef uint32_t (SCP_Parser::*write_data_function)(std::map<uint8_t, 
    std::unique_ptr<std::vector<int32_t> > >& data, std::map<uint32_t, bool> channels,
    std::ofstream& outfile, std::string record_name, std::vector<int32_t>& current_val, 
    header_file_info& header_info);

// Typedef for functions to write file headers.
typedef uint32_t (SCP_Parser::*write_header_function)(uint64_t total_samples, 
    std::map<uint32_t, bool> channels, std::ofstream& output, std::string record_name, 
    header_file_info& header_info);

class header_file_info {
  public:
    uint32_t leadCount;
    uint32_t ampMult;
    uint32_t samplePeriod;
    uint32_t leadID[MAX_LEADCOUNT];
    uint32_t initValue[MAX_LEADCOUNT];
    uint32_t year, month, day, hour, minutes, seconds;
    uint32_t file_res;

    header_file_info(){
      leadCount = 0;
      ampMult = 0;
      samplePeriod = 0;
      unsigned int i;
      for (i = 0; i < 12; i++){
        leadID[i] = 0;
        initValue[i] = 0;
      }
      year = 0;
      month = 0;
      day = 0;
      hour = 0;
      minutes = 0;
      seconds = 0;
      file_res = 0;
    }

    int getSignalCount(){
      return leadCount;
    }

    int getSampleFrequency(){
      return 1000000 / samplePeriod;
    }

    std::string getTime(){
      std::stringstream ss(std::stringstream::in | std::stringstream::out);
      std::string retVal;

      ss << hour << ":" << minutes << ":" << seconds;
      ss >> retVal;

      return retVal;
    }

    std::string getDate(){
      std::stringstream ss(std::stringstream::in | std::stringstream::out);
      std::string retVal;

      ss << day << "/" << month << "/" << year;
      ss >> retVal;

      return retVal;
    }

    float getGain(){
      return 1000000 / (float)ampMult;
    }

    int getInitialValue(unsigned int i){
      // channels are indexed from 1, not 0
      return initValue[i+1];
    }

    std::string getChannelLabel(unsigned int i){
      std::string retVal("ECG UNK");

      switch (leadID[i]){
        case 1:
          retVal.assign("ECG I");
          break;
        case 2:
          retVal.assign("ECG II");
          break;
        case 3:
          retVal.assign("ECG V1");
          break;
        case 4:
          retVal.assign("ECG V2");
          break;
        case 5:
          retVal.assign("ECG V3");
          break;
        case 6:
          retVal.assign("ECG V4");
          break;
        case 7:
          retVal.assign("ECG V5");
          break;
        case 8:
          retVal.assign("ECG V6");
          break;
        case 20:
          retVal.assign("ECG CM5");
          break;
        case 61:
          retVal.assign("ECG III");
          break;
        case 64:
          retVal.assign("ECG aVF");
          break;
        case 87:
          retVal.assign("ECG V");
          break;
        case 131:
          retVal.assign("ECG ES");
          break;
        case 132:
          retVal.assign("ECG AS");
          break;
        case 133:
          retVal.assign("ECG AI");
          break;
      }

      return retVal;
    }

};

// See ISHNE Specification for more information.
typedef struct
{
  int32_t Var_Length_Block_Size;

  // Size (in samples) of ECG
  int32_t Sample_Size_ECG;

  // Offset from beginning of file of
  // variable length block. (usually 522)
  int32_t Offset_var_length_block;

  // Offset from the beginning of the file of the ECG data
  int32_t Offset_ECG_block;

  // Version of the file
  int16_t File_Version;

  // The patient's first name
  char First_Name[40];

  // The patient's last name
  char Last_Name[40];

  // Patient ID
  char ID[20];

  // 0 -> unknown, 1 -> male, 2 -> female
  int16_t Sex;

  // 0 -> unknown, 1 -> caucasian, 2 -> black, 3 -> oriental, 4-9 reserved
  int16_t Race;

  // Patient Birthdate (day, month, year) (yes, backward from American style)
  int16_t Birth_Date[3];

  // Recording start date (day, month, year)
  int16_t Record_Date[3];

  // File creation date. (day, month, year)
  int16_t File_Date[3];

  // hour, minute, second
  int16_t Start_Time[3];

  // number of leads/channels
  int16_t nLeads;

  // Each lead must be identified in order here.
  // 0 -> unknown, 1 -> generic bipolar, 2 -> x bipolar, 3 -> y bipolar, 4 -> z bipolar
  // 5-10 -> I to a VF, 11-16 -> V1 to V6, 17 -> ES, 18 -> AS, 19 -> AI. -9 -> not present.
  int16_t Lead_Spec[12];

  // Lead quality for each lead
  // 0 -> unknown, 1 -> good, ... 5 -> bad. -9 -> not present.
  int16_t Lead_Qual[12];

  // Amplitude resolution for each lead. -9 -> not present.
  int16_t Resolution[12];

  // 0 -> none, 1 -> pacemaker present, type unknown, 2 -> single chamber unpolar
  // 3 -> dual chamber unipolar, 4 -> single chamber bipolar, 5 -> dual chamber bipolar.
  int16_t Pacemaker;

  // analog or digital recorder?
  char Recorder[40];

  // in Hz
  int16_t Sampling_Rate;

  // Proprietary of ECG (if any)
  char Proprietary[80];

  // Copyright and restriction of diffusion (if any)
  char Copyright[80];
  char Reserved[88];
} ishne_header_t;

/*
 * \class SCP_Parser This class provides functions for parsing information
 * out of SCP files and saving them in a variety of formats.
 */
class SCP_Parser
{

  private:

    write_square_wave_function square_wave_functions[NUM_FORMATS];
    write_data_function data_functions[NUM_FORMATS];
    write_header_function header_functions[NUM_FORMATS];

    static uint16_t const ccitt_crc16_table[256];

    static unsigned char read8(unsigned char *pData, uint32_t offset);

    static uint16_t read16(unsigned char *pData, uint32_t offset);

    static uint32_t read32(unsigned char *pData, uint32_t offset);

    static int32_t sectionHeader(unsigned char *pData, uint32_t *length);

    static unsigned char * get_bits(uint32_t *out_data, uint32_t bits,
        unsigned char *in_byte, unsigned char *bit_num, uint32_t sign_extend);

    uint32_t time_difference(header_file_info& newer, header_file_info& older);

    class subSection
    {
      private:
        uint16_t id;
        uint32_t length;
        uint32_t index;
        unsigned char *pData;

      public:
        subSection()
        {
          id = 0;
          length = 0;
          index = 0;
          pData = new unsigned char [2];
        }

        ~subSection()
        {
          delete [] pData;
        }

        void init(uint16_t i, uint32_t len,
            uint32_t ind, unsigned char *pD)
        {
          id = i;
          length = len;
          index = ind;
          delete [] pData;
          pData = new unsigned char [length];
          uint32_t j;
          for(j = 0; j < length; j++)
          {
            pData[j] = pD[j + index - 1];
          }
        }

        unsigned read_8(uint32_t *offset)
        {
          if(*offset > (length-1)) {
            (*SCP_Parser::error) << "Section Read Overflow!" << std::endl;
            return 0;
          }
          *offset += 1;
          return SCP_Parser::read8(pData, *offset-1);
        }

        unsigned read_16(uint32_t *offset)
        {
          if(*offset > (length-2)) {
            (*SCP_Parser::error) << "Section Read Overflow!" << std::endl;
            return 0;
          }
          *offset += 2;
          return SCP_Parser::read16(pData, *offset-2);
        }

        unsigned read_32(uint32_t *offset)
        {
          if(*offset > (length-4)) {
            (*SCP_Parser::error) << "Section Read Overflow!" << std::endl;
            return 0;
          }
          *offset += 4;
          return SCP_Parser::read32(pData, *offset-4);
        }

        void readString(uint32_t *offset, std::string *str, uint32_t len)
        {
          if(*offset > (length-len))
          {
            (*SCP_Parser::error) << "Section Read Overflow!" << std::endl;
            return;
          }
          str->assign((const char*)&(pData[*offset]),(int32_t) len);
          *offset += len;
        }

        char * getPointer(uint32_t *offset)
        {
          return (char *) &pData[*offset];
        }

        int32_t readHeader(uint32_t *length)
        {
          return SCP_Parser::sectionHeader(pData, length);
        }

        bool exists()
        {
          return length?1:0;
        }
    };

    uint32_t wfdb32_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
        std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
        std::vector<int32_t>& current_val, header_file_info& header_info);
    uint32_t wfdb32_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels, 
        std::ofstream& output, std::string record_name, header_file_info& header_info);
    uint32_t wfdb32_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
        double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
        int32_t low_val, int32_t high_val);

    uint32_t wfdb16_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
        std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
        std::vector<int32_t>& current_val, header_file_info& header_info);
    uint32_t wfdb16_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels, 
        std::ofstream& output, std::string record_name, header_file_info& header_info);
    uint32_t wfdb16_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
        double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
        int32_t low_val, int32_t high_val);

    uint32_t wfdb212_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
        std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
        std::vector<int32_t>& current_val, header_file_info& header_info);
    uint32_t wfdb212_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
        std::ofstream& output, std::string record_name, header_file_info& header_info);
    uint32_t wfdb212_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
        double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
        int32_t low_val, int32_t high_val);

    uint32_t wfdb8_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
        std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
        std::vector<int32_t>& current_val, header_file_info& header_info);
    uint32_t wfdb8_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
        std::ofstream& output, std::string record_name, header_file_info& header_info);
    uint32_t wfdb8_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
        double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val,
        int32_t low_val, int32_t high_val);

    uint32_t ishne_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
        std::ofstream& output, std::string record_name, header_file_info& header_info);
    uint32_t ishne_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
        double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val,
        int32_t low_val, int32_t high_val);
    uint32_t ishne_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
        std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
        std::vector<int32_t>& current_val, header_file_info& header_info);

	uint32_t wave_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
		std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
		std::vector<int32_t>& current_val, header_file_info& header_info);
	uint32_t wave_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
		std::ofstream& output, std::string record_name, header_file_info& header_info);
	uint32_t wave_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
		double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val,
		int32_t low_val, int32_t high_val);

  public:
    static std::ostream* error;

	SCP_Parser();

	~SCP_Parser();

	//! \brief Calculates the crc of a block of data
	//! \param data A pointer to the data to calculate the crc on.
	//! \param length The size (in bytes) of the data
	//! \param crcVal A pointer to the location to store the crc value in.
	static int32_t crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal);

    //! \brief Reads the contents of the file specified by "filename" into
    //! the array p_read.
    //! \param filename The name of the file to read
    //! \param p_read The array in which to store the contents
    //! \param fileLength The location in which to store the length of the file.
    //! \return An scp_parser_error_flag_t, or zero for success.
    uint32_t read_file(std::string filename, uint8_t ** p_read,
        uint32_t * fileLength);

    //! \brief Parses an SCP file and fills a map with the data.
    //! \param preliminary_data A pointer to the vector of vectors that should 
    //!               be filled with data.
    //! \param file_data The contents of the file in the form of a giant string.
    //! \param file_length The size in bytes of the file contents.
    //! \param interleaved whether to fill the vector with all the data or only
    //!        the first channel.
    //! \param key_string The encryption string
    //! \param iv_string The encryption iv
    //! \param header_info If supplied, this will be filled with information
    //!        from the file header.
    //! \return An scp_parser_error_flag_t, or zero for success.
    uint32_t process_file_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_data,
        uint8_t *file_data, 
        uint32_t file_length,
        int32_t interleaved,
        const char *key_string,
        const char *iv_string,
        header_file_info* header_file = NULL);

    //! \brief Parses an SCP file and fills a map with the data.
    //! \param preliminary_data A pointer to the vector of vectors that should 
    //!               be filled with data.
    //! \param filename The name of the file containing the data to process.
    //! \param interleaved whether to fill the vector with all the data or only
    //!        the first channel.
    //! \param key_string The encryption string
    //! \param iv_string The encryption iv
    //! \param header_info If supplied, this will be filled with information
    //!        from the file header.
    //! \return An scp_parser_error_flag_t, or zero for success.
    uint32_t process_file(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_data,
        std::string filename,
        int32_t interleaved,
        const char *key_string,
        const char *iv_string,
        header_file_info* header_info = NULL);

    //! \brief Parses a list of files and exports their contents to the outputfile
    //! specified by "output_filename" in the format specified by "format".
    //! \param output_filename The name of the output file.
    //! \param filenames The list of files to process. They will be processed sequentially, so they should be 
    //!		   sorted by date. 
	//! \param format A file_format_t showing what format to use (ISHNE, WFDB16, 8, or 212).
	//! \param channels A dictionary mapping ints to booleans, ints represent channel, true -> keep, false -> discard.
	//!					Channels start at 1 and go up to a maximum of 12.
    //! \param key_string The encryption string
    //! \param iv_string The encryption iv
    //! \param fill_gaps If this is true, gaps between files greater than 2 seconds will be filled with a 
    //!		   1 Hz square wave. 
    //! \param max_hours The maximum number of hours of data to export.
    //! \return An scp_parser_error_flag_t, or zero for success.
    uint32_t export_file_list(std::string output_filename, std::vector<std::string>& filenames,
        file_format_t format, std::map<uint32_t, bool>& channels, const char *key_string, 
        const char *iv_string, bool fill_gaps, double max_hours
#ifdef _WIN32
        , System::ComponentModel::BackgroundWorker^ worker = nullptr
#endif
        );

    // These are the functions which write the output data in the appropriate format.
    uint32_t write_data(file_format_t format, std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, std::map<uint32_t, bool> channels, 
        std::ofstream& outfile, std::string record_name, std::vector<int32_t>& current_val, header_file_info& header_info);
    uint32_t write_square_wave(file_format_t format, std::ofstream& outfile, uint32_t num_channels, double frequency_hz, uint32_t duration_seconds, 
        std::vector<int32_t>& current_val, int32_t low_val, int32_t high_val);
    uint32_t write_header(file_format_t format, uint64_t total_samples, std::map<uint32_t, bool> channels,
        std::ofstream& output, std::string record_name, header_file_info& header_info);

    //! \brief Determines what file format a given string corresponds to ("wfdb 16" => WFDB16)
    //! \param format The string to investigate
    //! \return The correct file_format_t. NUM_FORMATS if the format doesn't match a known value.
    file_format_t string_to_format(std::string format);

	//! \brief Return the header filename corresponding to a given output filename
	//! based on the file format. 
	//! \param format The desired output format.
	//! \data_filename The desired output filename for the data
	//! \return The header filename.
    std::string get_header_filename(file_format_t format, std::string data_filename);

};
#endif // SCP_PARSER_H

