/******************************************************************************
 *       Copyright (c) 2014, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 *
 *
 *****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include "scp_parser.h"

// define LINUX_CMDLINE to compile on Linux.
#if defined(_WIN32) || defined(LINUX_CMDLINE)
std::ostream * SCP_Parser::error = &std::cerr;
#else
#include <phpcpp.h>
std::ostream * SCP_Parser::error = &Php::error;
#endif



#define MIN_FILE_SIZE      (256)

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC          1

#define SCP_ID_STRING      "SCPECG"
#define LEAD_ID_BASE       (127)

///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

uint16_t const SCP_Parser::ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int32_t SCP_Parser::crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal)
{
  uint32_t j;
  if(length)
  {
    for(j = 0; j < length; j++)
    {
      *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
    }
  }
  return 0;
}

unsigned char SCP_Parser::read8(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return *pC;
}

uint16_t SCP_Parser::read16(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return (uint16_t) pC[0] + (uint16_t) pC[1]*256;
}

uint32_t SCP_Parser::read32(unsigned char *pData, uint32_t offset)
{
  unsigned char *pC = &pData[offset];
  return (((uint32_t)pC[3]*256 + (uint32_t)pC[2])*256 
      + (uint32_t)pC[1])*256 + (uint32_t) pC[0];
}

int32_t SCP_Parser::sectionHeader(unsigned char *pData, uint32_t *length)
{
  uint16_t theircrcValue = read16(pData, 0);           // CRC for section
  uint32_t id = read16(pData, 2);                      // Section ID for Section
  *length = read32(pData, 4);                          // Length for Section
  uint32_t sVersion = read8(pData, 8);                 // Section Version (2.2)
  uint32_t pVersion = read8(pData, 9);                 // Protocol Version (2.2)

  uint16_t ourcrcValue = 0xffff;
#if CHECK_CRC == 1
  crcBlock(&pData[2], (*length)-2, &ourcrcValue);
#else
  ourcrcValue = theircrcValue;
#endif

  if(ourcrcValue != theircrcValue)
  {
    (*SCP_Parser::error) << "ERROR: CRC error! file: 0x" << hex << theircrcValue
      << " - calculated: 0x" << ourcrcValue << dec << endl;
    return CRC_INVALID;
  }

  return 0;
}


unsigned char * SCP_Parser::get_bits(uint32_t *out_data, uint32_t bits,
    unsigned char *in_byte, unsigned char *bit_num, uint32_t sign_extend)
{
  uint32_t sign_bit = bits - 1;
  *out_data = 0;

  while(bits)
  {
    if(bits <= *bit_num)
    {
      *out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
      *bit_num -= bits;
      bits = 0;
    }
    else
    {
      *out_data |= ((uint32_t)*in_byte & (0x00ff>>(8-*bit_num))) << (bits - *bit_num);
      bits -= *bit_num;
      *bit_num = 0;
    }

    if(0 == *bit_num)
    {
      *bit_num += 8;
      in_byte++;
    }
  }

  // Sign extend the value we read
  if(sign_extend && (*out_data & (1<<sign_bit)) )
  {
    *out_data |= (~0)<<sign_bit;
  }

  return in_byte;
}

///////////////////////////////////////////////////////////////////////////////
//       Public Functions
///////////////////////////////////////////////////////////////////////////////

uint32_t SCP_Parser::process_file(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_data,
    std::string filename,
    int32_t interleaved,
    const char *key_string,
    const char *iv_string,
    header_file_info* header_info)
{
  uint8_t * p_read = NULL;
  uint32_t file_length;

  uint32_t ret_val = read_file(filename, &p_read, &file_length);
  if (ret_val)
  {
    return ret_val;
  }

  ret_val =  process_file_data(preliminary_data, p_read, file_length, interleaved, 
      key_string, iv_string, header_info);

  delete p_read;
  return ret_val;
}

uint32_t SCP_Parser::time_difference(header_file_info& newer, header_file_info& older)
{
  // returns the difference in seconds between the timestamps of the two header files. 
  // Note that this is the time gap between the beginning of each file.
  return ((newer.year - older.year) * 31540000) + ((newer.month - older.month) * 2628000)
    + ((newer.day - older.day) * 86400) + ((newer.hour - older.hour) * 3600)
    + ((newer.minutes - older.minutes) * 60) + (newer.seconds - older.seconds) ;
}

uint32_t SCP_Parser::read_file(std::string filename, uint8_t ** p_read,
    uint32_t * fileLength)
{
  // open the file.
  std::fstream inFile (filename.c_str(), std::ios::in | std::ios::binary);

  if(!inFile.is_open())
  {
    (*SCP_Parser::error) << "File could not be opened" << std::flush;
  }

  inFile.seekg(0, std::ios::end);
  (*fileLength) = inFile.tellg();
  inFile.seekg(0, std::ios::beg);

  // Make sure the file is not too small for the format
  if((*fileLength) < MIN_FILE_SIZE)
  {
    inFile.close();
    (*SCP_Parser::error) << "File is to small to be a TZR file" << std::flush;
    return UNEXPECTED_FILE_CONTENTS;
  }

  (*p_read) = new uint8_t [*fileLength];   // Allocate enough space to read in the whole file     
  inFile.read((char *) (*p_read), *fileLength);    // Store the remainder of the file in memory
  inFile.close();

  return 0;
}

uint32_t SCP_Parser::process_file_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_results,
    uint8_t *file_data, 
    uint32_t file_length,
    int32_t interleaved,
    const char *key_string,
    const char *iv_string,
    header_file_info* header_info)
{

  uint16_t calculatedCrcValue;           // The CRC we calculate
  uint16_t theircrcValue;                // The CRC from the file
  uint32_t length;                         // The length read from the file

  unsigned char tempKey[16];
  unsigned char tempIV[16];

  uint32_t i;

  aes_context aes_ctx;

  string str;                                  // Check that we are reading an SCP file
  str.assign((const char*)&file_data[16], 6);  // Copy what should be the "SCPECG" string
  if(str.compare(0, 6, SCP_ID_STRING))
  {
    cerr << "File may be encrypted. Attempting to decrypt." << endl;

    for(i = 0; i < 16; i++){
      char c1;
      char c2;

      tempKey[i] = 0;
      c1 = key_string[2*i];
      c2 = key_string[2*i+1];
      if( (c1 >= '0') && (c1 <= '9') ) tempKey[i] |= (c1 - '0') << 4;
      else if( (c1 >= 'a') && (c1 <= 'f') ) tempKey[i] |= (c1 - 'a' + 0x0a) << 4;
      else if( (c1 >= 'A') && (c1 <= 'F') ) tempKey[i] |= (c1 - 'A' + 0x0a) << 4;

      if( (c2 >= '0') && (c2 <= '9') ) tempKey[i] |= (c2 - '0') << 0;
      else if( (c2 >= 'a') && (c2 <= 'f') ) tempKey[i] |= (c2 - 'a' + 0x0a) << 0;
      else if( (c2 >= 'A') && (c2 <= 'F') ) tempKey[i] |= (c2 - 'A' + 0x0a) << 0;

      tempIV[i] = 0;
      c1 = iv_string[2*i];
      c2 = iv_string[2*i+1];
      if( (c1 >= '0') && (c1 <= '9') ) tempIV[i] |= (c1 - '0') << 4;
      else if( (c1 >= 'a') && (c1 <= 'f') ) tempIV[i] |= (c1 - 'a' + 0x0a) << 4;
      else if( (c1 >= 'A') && (c1 <= 'F') ) tempIV[i] |= (c1 - 'A' + 0x0a) << 4;

      if( (c2 >= '0') && (c2 <= '9') ) tempIV[i] |= (c2 - '0') << 0;
      else if( (c2 >= 'a') && (c2 <= 'f') ) tempIV[i] |= (c2 - 'a' + 0x0a) << 0;
      else if( (c2 >= 'A') && (c2 <= 'F') ) tempIV[i] |= (c2 - 'A' + 0x0a) << 0;
    }

    aes_setkey_dec(&aes_ctx, tempKey, 128);
    // Decrypt the first TWO blocks of the file
    if((i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, 32, tempIV, file_data, file_data)))
    {
      (*SCP_Parser::error) << "AES Decrypt Error 1. (0x" << hex << i << ") Aborting." << endl;
      return UNEXPECTED_FILE_CONTENTS;
    }

    // Check to make sure we now have a valid file format string
    str.assign((const char *) &file_data[16], 6);
    if(str.compare(0, 6, SCP_ID_STRING))
    {
      (*SCP_Parser::error) << "Invalid File Format String. Aborting." << endl;
      return UNEXPECTED_FILE_CONTENTS;
    }

    theircrcValue = read16(file_data, 0);
    length = read32(file_data, 2);

    if(length > file_length)
    {
      (*SCP_Parser::error) << "File Corrupted. Invalid Length value. Aborting." << endl;
      return UNEXPECTED_FILE_CONTENTS;
    }

    // Decrypt the remainder of the file
    if((i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, file_length-32, tempIV, &file_data[32], &file_data[32])))
    {
      (*SCP_Parser::error) << "AES Decrypt Error 2. (0x" << hex << i << ") Aborting." << endl;
      return UNEXPECTED_FILE_CONTENTS;
    }

    calculatedCrcValue = 0xffff;                            // CRC is based off an initial value of 0xffff
    crcBlock(&file_data[2], length-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
  }
  else{
    theircrcValue = read16(file_data, 0);
    length = read32(file_data, 2);

    calculatedCrcValue = 0xffff;                            // CRC is based off an initial value of 0xffff
    crcBlock(&file_data[2], length-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
  }

#if CHECK_CRC == 0
  calculcatedCrcValue = theircrcValue;
#endif

  if(calculatedCrcValue == theircrcValue)
  { 
    // If the CRC doesn't match, something has gone wrong
    uint32_t sI = 6;
    uint32_t j;

    /**********************************************************************************************
     *          Section 0 
     **********************************************************************************************/
    if(sectionHeader(&file_data[sI], &length))
    {               // Parse the header for section 0
      return CRC_INVALID;
    }

    subSection sections[12];                              // Array of sections to store file data

    for(j = 16; j < length; j += 10)
    {
      uint16_t id = read16(&file_data[sI], j);         // Section Number: 0 - 11
      uint32_t len = read32(&file_data[sI], j+2);        // Section Length
      uint32_t ind = read32(&file_data[sI], j+6);        // Section Start Index
      if(id < 12)
      {
        sections[id].init(id, len, ind, file_data);   // Copy data into section classes for later access
      }
      else{                                        // We've hit some sort of error
        (*SCP_Parser::error) << "ERROR: Unexpected section index(" << id << ")! Aborting." << endl;
        return UNEXPECTED_FILE_CONTENTS;
      }
    }

    /**********************************************************************************************
     *          Section 1
     **********************************************************************************************/

    if (sections[1].exists()){
      if (sections[1].readHeader(&length)) return CRC_INVALID;
      for (j = 16; j < length;)
      {
        unsigned char tag = sections[1].read_8(&j);
        unsigned short tagLength = sections[1].read_16(&j);
        switch (tag){
          case 0: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                  }break;
          case 1: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                  }break;
          case 2: {                // Patient ID
                    string patientID;
                    sections[1].readString(&j, &patientID, tagLength);
                  }break;
          case 3: {
                    string name;
                    sections[1].readString(&j, &name, tagLength);
                  }break;

          case 14: {              // Device ID
                     unsigned int institution = sections[1].read_16(&j);      // Not sure what this is, set to 0
                     unsigned int department = sections[1].read_16(&j);       // Not sure what this is, set to 0
                     unsigned int deviceID = sections[1].read_16(&j);         // Not sure what this is, set to 0
                     bool deviceType = (1 == sections[1].read_8(&j));         // 0 = cart, 1 = Host
                     unsigned int mfgCode = sections[1].read_8(&j);           // 255 = check string at end
                     string modelDesc;
                     sections[1].readString(&j, &modelDesc, 6);               // ASCII description of device
                     unsigned int protocolRev = sections[1].read_8(&j);       // SCP protocol version
                     unsigned int protocolCompat = sections[1].read_8(&j);    // Category I or II
                     unsigned int languageSupport = sections[1].read_8(&j);   // 0 = ASCII
                     unsigned int deviceCapability = sections[1].read_8(&j);  // print, interpret, store, acquire
                     unsigned int mainsFreq = sections[1].read_8(&j);         // 50 Hz, 60 Hz, Unspecified
                     j += 16;
                     unsigned int apRevLength = sections[1].read_8(&j);       // Length of Revision String
                     string apRev;
                     sections[1].readString(&j, &apRev, apRevLength);         // ASCII Revision of analysis prog.
                     unsigned int devSerNoLength = sections[1].read_8(&j);    // Length of Serial Number String
                     string devSerNo;
                     sections[1].readString(&j, &devSerNo, devSerNoLength);   // ASCII Device Serial Number
                     unsigned int devSoftNoLength = sections[1].read_8(&j);   // Length of software string
                     string devSoftNo;
                     sections[1].readString(&j, &devSoftNo, devSoftNoLength); // ASCII Software Version
                     unsigned int devSCPidLength = sections[1].read_8(&j);    // Length of SCP software ID
                     string devSCPid;
                     sections[1].readString(&j, &devSCPid, devSCPidLength);   // ASCII SCP software ID
                     unsigned int mfgStringLength = sections[1].read_8(&j);   // Length of mfg name
                     string mfgString;
                     sections[1].readString(&j, &mfgString, mfgStringLength); // Manufacturer Name

                   }break;
          case 25: {              // Date of Acquisition
                     if (tagLength != 4){
                       (*SCP_Parser::error) << "Error Parsing Date! Length = " << (int)tagLength << endl;
                       return UNEXPECTED_FILE_CONTENTS;
                     }
                     else{
                       unsigned short year = sections[1].read_16(&j);
                       unsigned char month = sections[1].read_8(&j);
                       unsigned char day = sections[1].read_8(&j);
                       if (nullptr != header_info)
                       {
                         header_info->year = year;
                         header_info->month = month;
                         header_info->day = day;
                       }
                     }
                   }break;
          case 26: {              // Time of Acquisition
                     if (tagLength != 3){
                       (*SCP_Parser::error) << "Error Parsing Time! Length = " << tagLength << endl;
                       return UNEXPECTED_FILE_CONTENTS;
                     }
                     else{
                       unsigned char hour = sections[1].read_8(&j);
                       unsigned char minutes = sections[1].read_8(&j);
                       unsigned char seconds = sections[1].read_8(&j);
                       if (nullptr != header_info)
                       {
                         header_info->hour = hour;
                         header_info->minutes = minutes;
                         header_info->seconds = seconds;
                       }
                     }
                   }break;
          case 27: {              // High Pass Filter
                     if (tagLength != 2){
                       (*SCP_Parser::error) << "Error Parsing HP Filter! Length = " << (int)tagLength << endl;
                       return UNEXPECTED_FILE_CONTENTS;
                     }
                     else{
                       unsigned short hpFilter = sections[1].read_16(&j);
                     }
                   }break;
          case 28: {              // Low Pass Filter
                     if (tagLength != 2){
                       (*SCP_Parser::error) << "Error Parsing LP Filter! Length = " << (int)tagLength << endl;
                       return UNEXPECTED_FILE_CONTENTS;
                     }
                     else{
                       unsigned short lpFilter = sections[1].read_16(&j);
                     }
                   }break;
          case 31:{
                    string ecgSequence;
                    sections[1].readString(&j, &ecgSequence, tagLength);
                  }break;
          case 34:{               // Time Zone
                    signed short offset = sections[1].read_16(&j);
                    unsigned short index = sections[1].read_16(&j);
                    string desc;
                    sections[1].readString(&j, &desc, tagLength - 4);
                  }break;
          case 255: {             // Terminator - we're done.
                      if (tagLength != 0){
                        (*SCP_Parser::error) << "Error Parsing Terminator! Length = " << (int)tagLength << endl;
                      }
                      else{
                        j = length;
                      }
                    }break;
          default: {
                     j += tagLength;
                   }break;
        }
      }

    }

    /**********************************************************************************************
     *          Section 2 
     **********************************************************************************************/

    uint32_t **prefixBits = NULL;
    uint32_t **totalBits = NULL;
    uint32_t **switchByte = NULL;
    uint32_t **baseValue = NULL;
    uint32_t **baseCode = NULL;
    uint32_t tableCount = 0;
    uint32_t *codeCount = NULL;
    uint8_t prefix_min_bits = 0xff;
    uint8_t prefix_max_bits = 0;
    if(sections[2].exists())
    {
      if(sections[2].readHeader(&length))
      {
        return CRC_INVALID;
      }
      j = 16;
      tableCount = sections[2].read_16(&j);
      uint32_t l;
      prefixBits = new uint32_t *[tableCount];
      totalBits = new uint32_t *[tableCount];
      switchByte = new uint32_t *[tableCount];
      baseValue = new uint32_t *[tableCount];
      baseCode = new uint32_t *[tableCount];

      codeCount = new uint32_t [tableCount];

      for(l = 0; l < tableCount; l++)
      {
        codeCount[l] = sections[2].read_16(&j);
        prefixBits[l] = new uint32_t [codeCount[l]];
        totalBits[l] = new uint32_t [codeCount[l]];
        switchByte[l] = new uint32_t [codeCount[l]];
        baseValue[l] = new uint32_t [codeCount[l]];
        baseCode[l] = new uint32_t [codeCount[l]];
        uint32_t m;
        for(m = 0; m < codeCount[l]; m++)
        {
          prefixBits[l][m] = sections[2].read_8(&j);
          if(prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
          if(prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
          totalBits[l][m] = sections[2].read_8(&j);
          switchByte[l][m] = sections[2].read_8(&j);
          baseValue[l][m] = sections[2].read_16(&j);
          baseCode[l][m] = sections[2].read_32(&j);

          // Reverse the bit-order for ease of use later
          uint32_t k;
          uint32_t temp = 0;
          for(k = 0; k < prefixBits[l][m]; k++)
          {
            temp = (temp << 1) | ((baseCode[l][m] & (1<<k))?1:0);
          }
          baseCode[l][m] = temp;

          // Check if this is the largest code
          if(header_info->file_res < (totalBits[l][m] - prefixBits[l][m]) )
          {
            header_info->file_res = (totalBits[l][m] - prefixBits[l][m]);
          }
        }

      }

    }
    else
    {
      header_info->file_res = 16;
    }


    /**********************************************************************************************
     *          Section 3 
     **********************************************************************************************/

    uint32_t leadCount = 0;
    uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
    if(sections[3].exists())
    {
      if(sections[3].readHeader(&length)) return CRC_INVALID;
      j = 16;
      leadCount = sections[3].read_8(&j);
      if (nullptr != header_info)
      {
        header_info->leadCount = leadCount;
      }
      uint32_t flags = sections[3].read_8(&j);
      sampleStart = new uint32_t [leadCount];
      sampleEnd = new uint32_t [leadCount];
      leadID = new uint32_t [leadCount];
      for(i = 0; i < leadCount && i < MAX_LEADCOUNT; i++)
      {
        sampleStart[i] = sections[3].read_32(&j);
        sampleEnd[i] = sections[3].read_32(&j);
        leadID[i] = sections[3].read_8(&j);
        if (nullptr != header_info)
          header_info->leadID[i] = leadID[i];

        // If the leadID was zero, start it at the standard location.
        leadID[i] = (leadID[i]) ? leadID[i] : LEAD_ID_BASE + i;
      }
    }

    /**********************************************************************************************
     *          Section 6 
     **********************************************************************************************/

    uint32_t *leadLength = NULL;
    uint32_t **dataArrays = NULL;
    if(sections[6].exists())
    {
      if(sections[6].readHeader(&length)) return CRC_INVALID;
      j = 16;
      uint32_t ampMult = sections[6].read_16(&j);
      uint32_t samplePeriod = sections[6].read_16(&j);
      if (0 == samplePeriod)
      {
        (*SCP_Parser::error) << "Error: Invalid File Contents. Sample period is set to 0." << std::endl;
        return UNEXPECTED_FILE_CONTENTS;
      }
      if (nullptr != header_info)
      {
        header_info->ampMult = ampMult;
        header_info->samplePeriod = samplePeriod;
      }
      uint32_t encoding = sections[6].read_8(&j);
      uint32_t compression = sections[6].read_8(&j);
      leadLength = new uint32_t [leadCount];
      dataArrays = new uint32_t *[leadCount];
      for(i = 0; i < leadCount; i++)
      {
        leadLength[i] = sections[6].read_16(&j);
        dataArrays[i] = new uint32_t [sampleEnd[i] - sampleStart[i] + 1];
      }
      for(i = 0; i < leadCount; i++)
      {
        unsigned char *rawData = new unsigned char [leadLength[i]];
        uint32_t l;
        for(l = 0; l < leadLength[i]; l++)
        {
          rawData[l] = sections[6].read_8(&j);
        }

        if(!sections[2].exists())
        {
          uint16_t *shortCaster = (uint16_t *) rawData;
          for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++)
          {
            if(l < (leadLength[i]/2))
            {
              dataArrays[i][l] = shortCaster[l];
              if(dataArrays[i][l] & (1<<(16-1)))
              {
                dataArrays[i][l] |= (~0)<<(16);
              }
            }
            else dataArrays[i][l] = 0;
          }
        }
        else{
          unsigned char currentBit = 8;
          unsigned char *currentByte = rawData;
          uint32_t currentTable = 0;
          for(l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++)
          {
            uint32_t k = 0;
            uint32_t prefixValue = 0, bits = 0;
            bool matchFound = 0;
            // Match the next code in the bitstream to our Huffman
            // table from Section 2
            while(!matchFound)
            {
              if(prefix_min_bits == prefix_max_bits)
              {
                if(!bits)
                {
                  currentByte = get_bits(&prefixValue, prefix_max_bits,
                      currentByte, &currentBit, 0);
                  bits += prefix_max_bits;
                }
                else break;
              }
              else
              {
                currentByte = get_bits(&prefixValue, 1,
                    currentByte, &currentBit, 0);
                bits += 1;
                if(bits > prefix_max_bits) break;
              }

              if( (bits >= prefix_min_bits) && (bits <= prefix_max_bits) )
              {
                for(k = 0; k < codeCount[currentTable]; k++)
                {
                  if(!prefixBits[currentTable][k])
                  {
                    matchFound = 1;
                    break;
                  }
                  else if(prefixBits[currentTable][k] == bits)
                  {
                    if(baseCode[currentTable][k] == prefixValue)
                    {
                      matchFound = 1;
                      break;
                    }
                  }
                }
              }
            }

            // SwitchByte == 1 means we decode a value
            if(switchByte)
            {
              currentByte = get_bits(&dataArrays[i][l], 
                  totalBits[currentTable][k] - prefixBits[currentTable][k],
                  currentByte, &currentBit, 1);
            }
            // SwitchByte == 0 means we switch to a different
            // table. (We don't use this on the Aera CT).
            else{
              // This is where we would switch tables
            }
          }
        }

        delete[] rawData;
      }



      uint32_t maxSamples = 0, l, totalSamples = 0;

      // If the data is supposed to be interleaved, we want 
      // (l = lead, s = sample)
      // s1, l1
      // s1, l2
      // s1, l3
      // s2, l1
      // s2, l2
      // s2, l3
      // etc
      //
      // So if we are interleaved, we only need one vector with a size equal
      // to the total number of samples. If we are not interleaved, we want
      // "leadCount" vectors, each with enough space to store the data from its
      // respective channel.

      if (interleaved)
      {
        preliminary_results[0] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
      }

      for(i = 0; i < leadCount; i++)
      {
        // Remember the largest sample length
        if(sampleEnd[i] > maxSamples) maxSamples = sampleEnd[i];
        totalSamples += sampleEnd[i];

        if (!interleaved)
        {
          preliminary_results[leadID[i]] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
          preliminary_results[leadID[i]]->reserve(sampleEnd[i]);
        }
      }

      if (interleaved)
      {
        preliminary_results[0]->reserve(totalSamples);
      }

      int32_t *D1 = new int32_t [leadCount];
      int32_t *D2 = new int32_t [leadCount];
      for(l = 0; l < maxSamples; l++)
      {
        for(i = 0; i < leadCount; i++)
        {
          uint32_t relSample = 0;
          if((sampleStart[i]-1) <= l) relSample = 1 + l - sampleStart[i];
          if( ((sampleStart[i]-1) <= l) && ((sampleEnd[i]-1) >= l) )
          {
            int32_t tempData = dataArrays[i][l +sampleStart[i] - 1];
            int32_t outputData = 0;
            if(encoding == 1)
            {
              // First differences
              if(relSample == 0)
              {
                outputData = tempData;
              }
              else{
                outputData = D1[i] + tempData;
              }
              D1[i] = outputData;
            }
            else if(encoding == 2)
            {
              // Second Differences
              if(relSample < 2)
              {
                outputData = tempData;
              }
              else{
                outputData = tempData + (2*D1[i]) - D2[i];
              }
              D2[i] = D1[i];
              D1[i] = outputData;
            }
            else{
              outputData = tempData;
            }

            if (interleaved)
            {
              preliminary_results[0]->push_back(outputData);
            }
            else
            {
              preliminary_results[leadID[i]]->push_back(outputData);
            }
          }
          else
          {
            //preliminary_results[i][l] = 0;
          }
        }
      }

      // php conversion went here

      delete[] D1;
      delete[] D2;
    }

    // Store the initial values of each lead in the header info.
    for (auto &kv : preliminary_results)
    {
      if (nullptr != header_info && kv.first > 0 && kv.first < MAX_LEADCOUNT && kv.second->size() > 0)
      {
        header_info->initValue[kv.first] = kv.second->at(0);
      }
    }

    if(sections[2].exists())
    {
      uint32_t l;
      for(l = 0; l < tableCount; l++)
      {
        delete[] prefixBits[l];
        delete[] totalBits[l];
        delete[] switchByte[l];
        delete[] baseValue[l];
        delete[] baseCode[l];
      }
      delete[] codeCount;
      delete[] prefixBits;
      delete[] totalBits;
      delete[] switchByte;
      delete[] baseValue;
      delete[] baseCode;
    }

    if(sections[3].exists())
    {
      delete[] sampleStart;
      delete[] sampleEnd;
      delete[] leadID;
    }
    if(sections[6].exists())
    {
      delete[] leadLength;
      uint32_t l;
      for(l = 0; l < leadCount; l++)
      {
        delete[] dataArrays[l];
      }
      delete[] dataArrays;
    }

  }
  else{
    return CRC_INVALID;
  }

  return 0;
}

SCP_Parser::SCP_Parser()
{
  square_wave_functions[FILE_FORMAT_WFDB32] = &SCP_Parser::wfdb32_write_square_wave;
  square_wave_functions[FILE_FORMAT_WFDB16] = &SCP_Parser::wfdb16_write_square_wave;
  square_wave_functions[FILE_FORMAT_WFDB212] = &SCP_Parser::wfdb212_write_square_wave;
  square_wave_functions[FILE_FORMAT_WFDB8] = &SCP_Parser::wfdb8_write_square_wave;
  square_wave_functions[FILE_FORMAT_ISHNE] = &SCP_Parser::ishne_write_square_wave;
  square_wave_functions[FILE_FORMAT_WAVE] = &SCP_Parser::wave_write_square_wave;

  data_functions[FILE_FORMAT_WFDB32] = &SCP_Parser::wfdb32_write_data;
  data_functions[FILE_FORMAT_WFDB16] = &SCP_Parser::wfdb16_write_data;
  data_functions[FILE_FORMAT_WFDB212] = &SCP_Parser::wfdb212_write_data;
  data_functions[FILE_FORMAT_WFDB8] = &SCP_Parser::wfdb8_write_data;
  data_functions[FILE_FORMAT_ISHNE] = &SCP_Parser::ishne_write_data;
  data_functions[FILE_FORMAT_WAVE] = &SCP_Parser::wave_write_data;

  header_functions[FILE_FORMAT_WFDB32] = &SCP_Parser::wfdb32_write_header;
  header_functions[FILE_FORMAT_WFDB16] = &SCP_Parser::wfdb16_write_header;
  header_functions[FILE_FORMAT_WFDB212] = &SCP_Parser::wfdb212_write_header;
  header_functions[FILE_FORMAT_WFDB8] = &SCP_Parser::wfdb8_write_header;
  header_functions[FILE_FORMAT_ISHNE] = &SCP_Parser::ishne_write_header;
  header_functions[FILE_FORMAT_WAVE] = &SCP_Parser::wave_write_header;
}

SCP_Parser::~SCP_Parser()
{
}

uint32_t SCP_Parser::wfdb8_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
    int32_t low_val, int32_t high_val)
{
  // We need the high and low parameters to match the signature of the other functions, 
  // but for wfdb8 we just write the highest possible and lowest possible values
  // for the amplitude of the square wave.

  // This doesn't have to be exact, but it will help compensate for the iterations
  // to travel up or down to keep the file at the correct time.
  int16_t iterations = high_val / INT8_MAX;

  int8_t zero = 0; // Pointer to zero.
  vector<int32_t> file_sum = current_val;
  int32_t difference;
  int8_t output_val;
  bool achieved_value_flag = false;

  for (uint32_t i = 0; i < duration_seconds; ++i)
  {
    // Iterate a few times to make sure each channel has reached the low_val,
    // because of overflow and the fact that we're dealing with differences
    // rather than absolute values.
    bool achieved_value_flag = false;
    while (!achieved_value_flag)
    {
      achieved_value_flag = true;
      for (uint32_t k = 0; k < num_channels; ++k)
      {
        difference = low_val - current_val[k];
        if (difference > INT8_MAX)
        {
          output_val = INT8_MAX;
          achieved_value_flag = false;
        }
        else if (difference < INT8_MIN)
        {
          output_val = INT8_MIN;
          achieved_value_flag = false;
        }
        else
        {
          output_val = difference & 0xff;
        }
        current_val[k] += output_val;
        outfile.write(reinterpret_cast<const char*>(&output_val), sizeof(output_val));
      }
    }

    // We've reached the low value. Write some zeros. -2*iterations
    // since we have to go from max to zero to min.
    for (uint32_t j = 0; j < frequency_hz / 2 - (2 * iterations); ++j)
    {
      for (uint32_t k = 0; k < num_channels; ++k)
      {
        outfile.write(reinterpret_cast<const char*>(&zero), sizeof(zero));
      }
    }

    // Time to go up.
    achieved_value_flag = false;
    while (!achieved_value_flag)
    {
      achieved_value_flag = true;
      for (uint32_t k = 0; k < num_channels; ++k)
      {
        difference = high_val - current_val[k];
        if (difference > INT8_MAX)
        {
          output_val = INT8_MAX;
          achieved_value_flag = false;
        }
        else if (difference < INT8_MIN)
        {
          output_val = INT8_MIN;
          achieved_value_flag = false;
        }
        else
        {
          output_val = difference & 0xff;
        }
        current_val[k] += output_val;
        outfile.write(reinterpret_cast<const char*>(&output_val), sizeof(output_val));
      }
    }

    // Write some more zeros. -2*iterations since it had to go from 
    // min to zero to max.
    for (uint32_t j = 0; j < frequency_hz / 2 - (2 * iterations); ++j)
    {
      for (uint32_t k = 0; k < num_channels; ++k)
      {
        outfile.write(reinterpret_cast<const char*>(&zero), sizeof(zero));
      }
    }
  }

  // Go back to zero
  achieved_value_flag = false;
  while (!achieved_value_flag)
  {
    achieved_value_flag = true;
    for (uint32_t k = 0; k < num_channels; ++k)
    {
      difference = 0 - current_val[k];
      if (difference > INT8_MAX)
      {
        output_val = INT8_MAX;
        achieved_value_flag = false;
      }
      else if (difference < INT8_MIN)
      {
        output_val = INT8_MIN;
        achieved_value_flag = false;
      }
      else
      {
        output_val = difference & 0xff;
      }
      current_val[k] += output_val;
      outfile.write(reinterpret_cast<const char*>(&output_val), sizeof(output_val));
    }
  }
  return 0;
}

uint32_t SCP_Parser::wfdb8_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
    std::ofstream& output, std::string record_name, header_file_info& header_info)
{
  // Count the number of channels to write to the file.
  uint32_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  // generate and write header information
  output << record_name << " " << num_channels
    << " " << header_info.getSampleFrequency()
    << " " << total_samples
    << " " << header_info.getTime()
    << " " << header_info.getDate()
    << std::endl;
  double gain = header_info.getGain();
  for (uint32_t i = 0; i < num_channels; ++i)
  {
    output << record_name << ".dat 8" << " " << (gain)
      << " " << (header_info.file_res) << " 0 "
      << " 0 " // Initial value. Hardcode 0 to allow easy interoperation between FILE_FORMAT_WFDB8 and FILE_FORMAT_WFDB16 file formats.
      << " 0 0 " << header_info.getChannelLabel(i)
      << std::endl;
  }

  return 0;
}

uint32_t SCP_Parser::wfdb8_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  // For calculating the correct value.
  int32_t difference;

  // The value to actually write.
  int8_t output_val;
  if (outfile.is_open())
  {
    // Write the signal file, all the samples interleaved together.
    std::vector<uint8_t> channel_ids;
    for (auto &kv : data)
    {
      channel_ids.push_back(kv.first);
    }
    std::sort(channel_ids.begin(), channel_ids.end());

    // Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
    for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
    {

      for (uint32_t j = 0; j < channel_ids.size(); ++j)
      {
        // As we loop through this, we will keep track of the sum of the bytes written to 
        // the file, then write the difference between that and the next value from the
        // "data" vector, while checking for overflow.

        difference = data[channel_ids[j]]->at(i) - current_val[j];
        // Get the difference to store in the current byte, checked for overflow.
        if (difference > INT8_MAX)
          output_val = INT8_MAX;
        else if (difference < INT8_MIN)
          output_val = INT8_MIN;
        else
          output_val = difference & 0xff; 

        current_val[j] += output_val;
        outfile.write(reinterpret_cast<const char*>(&output_val), sizeof(output_val));
      }
    }

  }
  else
  {
    // Error writing to file.
    return FILE_IO_ERROR;
  }

  return 0;
}


uint32_t SCP_Parser::wfdb32_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
    int32_t low_val, int32_t high_val)
{
  // Number of times to write each high/low value.
  uint32_t n = (uint32_t) (num_channels * frequency_hz / 2);
  for (uint32_t i = 0; i < duration_seconds; ++i)
  {
    for (uint32_t j = 0; j < n; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&low_val), sizeof(int32_t));
    }
    for (uint32_t j = 0; j < n; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&high_val), sizeof(int32_t));
    }
  }

  return 0;
}

uint32_t SCP_Parser::wfdb32_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
    std::ofstream& output, std::string record_name, header_file_info& header_info)
{
  // Count the number of channels to write to the file.
  uint32_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  // generate and write header information
  output << record_name << " " << num_channels
    << " " << header_info.getSampleFrequency()
    << " " << total_samples
    << " " << header_info.getTime()
    << " " << header_info.getDate()
    << std::endl;
  double gain = header_info.getGain();
  for (uint32_t i = 0; i < num_channels; ++i)
  {
    output << record_name << ".dat 32" << " " << (gain) 
      << " " << (header_info.file_res) << " 0 "
      << header_info.getInitialValue(i)
      << " 0 0 " << header_info.getChannelLabel(i)
      << std::endl;
  }


  return 0;
}

uint32_t SCP_Parser::wfdb32_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  if (outfile.is_open())
  {
    // Get a sorted list of the keys so we can iterate reliably.
    std::vector<uint8_t> channel_ids;
    for (auto &kv : data)
    {
      channel_ids.push_back(kv.first);
    }
    std::sort(channel_ids.begin(), channel_ids.end());

    // Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
    for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
    {
      for (uint32_t j = 0; j < channel_ids.size(); ++j)
      {
        outfile.write(reinterpret_cast<const char*>(&data[channel_ids[j]]->at(i)), sizeof(int32_t));
      }
    }

  }
  else
  {
    // Error writing to file.
    return FILE_IO_ERROR;
  }

  return 0;
}


uint32_t SCP_Parser::wfdb16_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
    int32_t low_val, int32_t high_val)
{
  // Number of times to write each high/low value.
  uint32_t n = (uint32_t) (num_channels * frequency_hz / 2);
  if(low_val < -32768) low_val = -32768;
  if(high_val > 32767) high_val = 32767;
  for (uint32_t i = 0; i < duration_seconds; ++i)
  {
    for (uint32_t j = 0; j < n; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&low_val), sizeof(int16_t));
    }
    for (uint32_t j = 0; j < n; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&high_val), sizeof(int16_t));
    }
  }

  return 0;
}

uint32_t SCP_Parser::wfdb16_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
    std::ofstream& output, std::string record_name, header_file_info& header_info)
{
  // Count the number of channels to write to the file.
  uint32_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  // generate and write header information
  output << record_name << " " << num_channels
    << " " << header_info.getSampleFrequency()
    << " " << total_samples
    << " " << header_info.getTime()
    << " " << header_info.getDate()
    << std::endl;
  double gain = header_info.getGain();
  for (uint32_t i = 0; i < num_channels; ++i)
  {
    output << record_name << ".dat 16" << " " << (gain)
      << " " << (header_info.file_res) << " 0 "
      << header_info.getInitialValue(i)
      << " 0 0 " << header_info.getChannelLabel(i)
      << std::endl;
  }


  return 0;
}

uint32_t SCP_Parser::wfdb16_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  if (outfile.is_open())
  {
    // Get a sorted list of the keys so we can iterate reliably.
    std::vector<uint8_t> channel_ids;
    for (auto &kv : data)
    {
      channel_ids.push_back(kv.first);
    }
    std::sort(channel_ids.begin(), channel_ids.end());

    // Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
    for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
    {
      for (uint32_t j = 0; j < channel_ids.size(); ++j)
      {
        int32_t output = data[channel_ids[j]]->at(i);
        if(output > 32767) output = 32767;
        else if(output < (-32768)) output = -32768;
        outfile.write(reinterpret_cast<const char*>(&output), sizeof(int16_t));
      }
    }

  }
  else
  {
    // Error writing to file.
    return FILE_IO_ERROR;
  }

  return 0;
}


uint32_t SCP_Parser::wfdb212_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
    int32_t low_val, int32_t high_val)
{
  int16_t dedicated_byte_high;
  int16_t transition_byte_high;
  int16_t dedicated_byte_low;
  int16_t transition_byte_low;

  // Overflow handling
  if (low_val > 2047)
  {
    dedicated_byte_low = 0xff;
    transition_byte_low = 0x07;
  }
  // underflow_handling
  else if (low_val < -2048)
  {
    dedicated_byte_low = 0x00;
    transition_byte_low = 0x08;
  }
  else
  {
    dedicated_byte_low = low_val & 0xff;
    transition_byte_low = (low_val >> 8) & 0x0f;
    transition_byte_low |= (low_val >> 4) & 0xf0;
  }

  if (high_val > 2047)
  {
    dedicated_byte_high = 0xff;
    transition_byte_high = 0x07;
  }
  else if (high_val < -2048)
  {
    dedicated_byte_high = 0x00;
    transition_byte_high = 0x08;
  }
  else
  {
    dedicated_byte_high = high_val & 0xff;
    transition_byte_high = (high_val >> 8) & 0x0f;
    transition_byte_high |= (high_val >> 4) & 0xf0;
  }

  for (uint32_t i = 0; i < duration_seconds; ++i)
  {
    // Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
    for (uint32_t j = 0; j < frequency_hz / 2; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&dedicated_byte_low), sizeof(int8_t));
      outfile.write(reinterpret_cast<const char*>(&transition_byte_low), sizeof(int8_t));
      outfile.write(reinterpret_cast<const char*>(&dedicated_byte_low), sizeof(int8_t));
    }
    for (uint32_t j = 0; j < frequency_hz / 2; ++j)
    {
      outfile.write(reinterpret_cast<const char*>(&dedicated_byte_high), sizeof(int8_t));
      outfile.write(reinterpret_cast<const char*>(&transition_byte_high), sizeof(int8_t));
      outfile.write(reinterpret_cast<const char*>(&dedicated_byte_high), sizeof(int8_t));
    }
  }

  return 0;
}

uint32_t SCP_Parser::wfdb212_write_header(uint64_t total_samples, std::map<uint32_t, 
    bool> channels, std::ofstream& output, std::string record_name, 
    header_file_info& header_info)
{

  // Count the number of channels to write to the file.
  uint32_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  if (2 != num_channels)
  {
    return WFDB212_TOO_MANY_CHANNELS;
  }

  // generate and write header information
  output << record_name << " " << num_channels
    << " " << header_info.getSampleFrequency()
    << " " << total_samples
    << " " << header_info.getTime()
    << " " << header_info.getDate()
    << std::endl;
  double gain = header_info.getGain();
  for (uint32_t i = 0; i < num_channels; ++i)
  {
    output << record_name << ".dat 212" << " " << (gain)
      << " " << (header_info.file_res) << " 0 "
      << header_info.getInitialValue(i)
      << " 0 0 " << header_info.getChannelLabel(i)
      << std::endl;
  }


  return 0;
}

uint32_t SCP_Parser::wfdb212_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  int16_t output = 0;
  int16_t transition_byte = 0;
  int16_t dedicated_byte = 0;
  bool word_boundary = true;

  if (((-1) >> 1) != (-1))
  {
    // This compiler does not support arithmetic shifting.
    return ARITHMETIC_SHIFTS_NOT_SUPPORTED;
  }

  if (2 != data.size())
  {
    return WFDB212_TOO_MANY_CHANNELS;
  }

  // Get a sorted list of the keys so we can iterate reliably.
  std::vector<uint8_t> channel_ids;
  for (auto &kv : data)
  {
    channel_ids.push_back(kv.first);
  }
  std::sort(channel_ids.begin(), channel_ids.end());

  // Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
  for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
  {
    for (uint32_t j = 0; j < channel_ids.size(); ++j)
    {
      output = data[channel_ids[j]]->at(i);
      // Case 1: Starting on a word boundary
      if (word_boundary)
      {
        // Overflow handling
        if (output > 2047)
        {
          dedicated_byte = 0xff;
          transition_byte = 0x07;
        }

        // underflow_handling
        else if (output < -2048)
        {
          dedicated_byte = 0x00;
          transition_byte = 0x08;
        }
        else
        {
          dedicated_byte = output & 0xff;
          transition_byte = (output >> 8) & 0x0f;
        }

        outfile.write(reinterpret_cast<const char*>(&dedicated_byte), sizeof(int8_t));
        word_boundary = !word_boundary;
      }
      else // Case 2: Starting halfway through a byte.
      {
        // Overflow handling.
        if (output > 2047)
        {
          dedicated_byte = 0xff;
          transition_byte &= 0x0f;
          transition_byte |= 0x70;
        }

        // underflow handling
        else if (output < -2048)
        {
          dedicated_byte = 0x00;
          transition_byte &= 0x0f;
          transition_byte |= 0x80;
        }
        else
        {
          dedicated_byte = output & 0xff;
          transition_byte |= (output >> 4) & 0xf0;
        }
        outfile.write(reinterpret_cast<const char*>(&transition_byte), sizeof(int8_t));
        outfile.write(reinterpret_cast<const char*>(&dedicated_byte), sizeof(int8_t));
        word_boundary = !word_boundary;
      }

    }
  }

  return 0;
}


uint32_t SCP_Parser::ishne_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
    std::ofstream& output, std::string record_name, header_file_info& header_info)
{
  char magic_number[] = "ISHNE1.0";
  uint16_t crc = 0xffff;
  uint32_t zero = 0;

  // Count the number of channels to write to the file.
  uint32_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  ishne_header_t header =
  {
    0,
    (int32_t) total_samples,
    522,
    ISHNE_DATA_START,
    1,
    "", // First name
    "", // Last name
    "", // ID #
    0,
    0,
    { 0, 0, 0 },
    { 0, 0, 0 },
    { 0, 0, 0 },
    { 0, 0, 0 },
    (int16_t) num_channels,
    { ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
    { ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
    { ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
    0,
    "", // Recorder type
    (int16_t) header_info.getSampleFrequency(),
    "", // Proprietary of ECG data
    "", // Copyright
    ""  // Reserved
  };

  std::string record_date = header_info.getDate(); // dd/mm/yyyy
  header.Record_Date[0] = std::stoi(record_date.substr(0, record_date.find_first_of("/")));	// day
  header.Record_Date[1] = std::stoi(record_date.substr(record_date.find_first_of("/") + 1, record_date.find_last_of("/") - record_date.find_first_of("/") - 1));	// month
  header.Record_Date[2] = std::stoi(record_date.substr(record_date.find_last_of("/") + 1));	// year

  time_t t = time(0);
  struct tm * now = localtime(&t);
  header.File_Date[0] = now->tm_mday; // day
  header.File_Date[1] = now->tm_mon + 1; // month
  header.File_Date[2] = now->tm_year + 1900; // year

  std::string start_time = header_info.getTime(); // hh:mm:ss
  header.Start_Time[0] = std::stoi(start_time.substr(0, start_time.find_first_of(":")));	// day
  header.Start_Time[1] = std::stoi(start_time.substr(start_time.find_first_of(":") + 1, start_time.find_last_of(":") - start_time.find_first_of(":") - 1));	// month
  header.Start_Time[2] = std::stoi(start_time.substr(start_time.find_last_of(":") + 1));	// year

  for (uint32_t i = 0; i < num_channels; ++i)
  {
    header.Lead_Spec[i] = 0;
    header.Lead_Qual[i] = 0;
    header.Resolution[i] = header_info.file_res;
  }

  char buffer[512];
  char* p_buffer = buffer;
  for (uint32_t i = 0; i < 512; ++i)
  {
    buffer[i] = 0;
  }

  memcpy(p_buffer, &header.Var_Length_Block_Size, 4);
  p_buffer += 4;
  memcpy(p_buffer, &header.Sample_Size_ECG, 4);
  p_buffer += 4;
  memcpy(p_buffer, &header.Offset_var_length_block, 4);
  p_buffer += 4;
  memcpy(p_buffer, &header.Offset_ECG_block, 4);
  p_buffer += 4;
  memcpy(p_buffer, &header.File_Version, 2);
  p_buffer += 2;
  memcpy(p_buffer, header.First_Name, 40);
  p_buffer += 40;
  memcpy(p_buffer, header.Last_Name, 40);
  p_buffer += 40;
  memcpy(p_buffer, header.ID, 20);
  p_buffer += 20;
  memcpy(p_buffer, &header.Sex, 2);
  p_buffer += 2;
  memcpy(p_buffer, &header.Race, 2);
  p_buffer += 2;
  memcpy(p_buffer, &header.Birth_Date, 6);
  p_buffer += 6;
  memcpy(p_buffer, &header.Record_Date, 6);
  p_buffer += 6;
  memcpy(p_buffer, &header.File_Date, 6);
  p_buffer += 6;
  memcpy(p_buffer, &header.Start_Time, 6);
  p_buffer += 6;
  memcpy(p_buffer, &header.nLeads, 2);
  p_buffer += 2;
  memcpy(p_buffer, &header.Lead_Spec, 24);
  p_buffer += 24;
  memcpy(p_buffer, &header.Lead_Qual, 24);
  p_buffer += 24;
  memcpy(p_buffer, &header.Resolution, 24);
  p_buffer += 24;
  memcpy(p_buffer, &header.Pacemaker, 2);
  p_buffer += 2;
  memcpy(p_buffer, header.Recorder, 40);
  p_buffer += 40;
  memcpy(p_buffer, &header.Sampling_Rate, 2);
  p_buffer += 2;
  memcpy(p_buffer, header.Proprietary, 80);
  p_buffer += 80;
  memcpy(p_buffer, header.Copyright, 80);
  p_buffer += 80;
  memcpy(p_buffer, header.Reserved, 88);
  p_buffer += 88;

  // calculate checksum
  crcBlock((uint8_t*)buffer, 512, &crc); // Calculate the CRC for the remainder of the file
  output.seekp(0);
  output.write(magic_number, 8);
  output.write((char*)&crc, 2);
  output.write(buffer, 512);
  //output.write((char*) &zero, 4); // Write a few bytes since we need something for the variable length block. 

  return 0;
}

uint32_t SCP_Parser::ishne_write_square_wave(std::ofstream& outfile, uint32_t num_channels, 
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val, 
    int32_t low_val, int32_t high_val)
{
  // These two formats store data the same way.
  return wfdb16_write_square_wave(outfile, num_channels, frequency_hz, duration_seconds, current_val, low_val, high_val);
}

uint32_t SCP_Parser::ishne_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name, 
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  // These two formats store data the same way.
  return wfdb16_write_data(data, channels, outfile, record_name, current_val, header_info);
}

uint32_t SCP_Parser::wave_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
    std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
    std::vector<int32_t>& current_val, header_file_info& header_info)
{
  uint32_t base_frequency = 1900;

  // Represents the number of of wav samples in on sample of ecg data.
  uint32_t samples_per_sample = WAVE_SAMPLE_RATE / header_info.getSampleFrequency();
  uint32_t amplitude = 30000;
  int16_t val;
  uint32_t frequency = 0;



  std::vector<uint8_t> channel_ids;
  for (auto &kv : data)
  {
    channel_ids.push_back(kv.first);
  }
  std::sort(channel_ids.begin(), channel_ids.end());

  // For each sample
  for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
  {
    // output a sine wave
    for (uint32_t k = 0; k < samples_per_sample; ++k)
    {
      // For each channel
      for (uint32_t j = 0; j < channel_ids.size(); ++j)
      {
        frequency = base_frequency + 0;
        //frequency = base_frequency + data[channel_ids[j]]->at(j) / header_info.getGain();

        //val = (int16_t)(sin((k / 28) * 2 * 3.14159 / frequency) * amplitude); // f = 2*pi / b, b = 2*pi / f
        val = (int16_t)(sin((k / 28) * 2 * 3.14159 / frequency) * amplitude); // f = 2*pi / b, b = 2*pi / f

        outfile.write((char*) &val, 2);
      }
    }
  }
  return 0;
}

uint32_t SCP_Parser::wave_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
    std::ofstream& output, std::string record_name, header_file_info& header_info)
{	
  uint8_t buffer[WAVE_HEADER_SIZE];
  uint8_t* p_buffer = buffer;

  // Count the number of channels to write to the file.
  uint16_t num_channels = 0;
  for (uint32_t i = 1; i <= header_info.leadCount; ++i)
  {
    if (channels[i])
    {
      ++num_channels;
    }
  }

  uint32_t int_holder_32;
  uint16_t int_holder_16;

  memcpy(p_buffer, "RIFF", 4); // ChunkID
  p_buffer += 4;

  int_holder_32 = (uint32_t) (total_samples * num_channels * 2 * WAVE_SAMPLE_RATE / header_info.getSampleFrequency() + 36);
  memcpy(p_buffer, &int_holder_32, 4); // ChunkSize
  p_buffer += 4;

  memcpy(p_buffer, "FILE_FORMAT_WAVE", 4); // Format
  p_buffer += 4;

  memcpy(p_buffer, "fmt ", 4); // Subchunk1 ID
  p_buffer += 4;

  int_holder_32 = 16;
  memcpy(p_buffer, &int_holder_32, 4); // Subchunk1 Size
  p_buffer += 4;

  int_holder_16 = 1;
  memcpy(p_buffer, &int_holder_16, 2); // AudioFormat
  p_buffer += 2;

  memcpy(p_buffer, &num_channels, 2); // NumChannels
  p_buffer += 2;

  int_holder_32 = WAVE_SAMPLE_RATE;
  memcpy(p_buffer, &int_holder_32, 4); // SampleRate
  p_buffer += 4;

  int_holder_32 = WAVE_SAMPLE_RATE * num_channels * 2; // 2 is bytes per sample.
  memcpy(p_buffer, &int_holder_32, 4); // ByteRate
  p_buffer += 4;

  int_holder_16 = num_channels * 2;
  memcpy(p_buffer, &int_holder_16, 2); // BlockAlign
  p_buffer += 2;

  int_holder_16 = 2;
  memcpy(p_buffer, &int_holder_16, 2); // BitsPerSample
  p_buffer += 2;

  memcpy(p_buffer, "data", 4); // Subchunk2ID
  p_buffer += 4;

  int_holder_32 = (uint32_t) (total_samples * num_channels * 2 * WAVE_SAMPLE_RATE / header_info.getSampleFrequency()); // 2 is bytes per sample.
  memcpy(p_buffer, &int_holder_32, 4); // SubChunk2Size
  p_buffer += 4;

  output.seekp(0);
  output.write((char*) buffer, WAVE_HEADER_SIZE);

  return 0;
}

uint32_t SCP_Parser::wave_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
    double frequency_hz, uint32_t duration_seconds, std::vector<int32_t>& current_val,
    int32_t low_val, int32_t high_val)
{
  // TODO: this
  return 0;
}

uint32_t SCP_Parser::export_file_list(std::string output_filename, 
    std::vector<std::string>& filenames, file_format_t format, std::map<uint32_t, bool>& channels, 
    const char *key_string, const char *iv_string, bool fill_gaps,
    double max_hours
#ifdef _WIN32
    , System::ComponentModel::BackgroundWorker^ worker
#endif
    )
{
  // Used to get data for a single file
  map<uint8_t, std::unique_ptr<std::vector<int32_t> > > data;

  // A couple things are done differently the first time through the loop.
  bool first_time = true;

  // The amount of time covered by the current file.
  double duration_sec = 0;
  header_file_info header_info;
  header_file_info prev_header_info;

  // For storing the start time of the sample data.
  header_file_info initial_header_info;
  uint32_t ret_val = 0;
  double time_diff = 0;
  uint64_t total_samples = 0;
  std::ofstream outfile(output_filename, std::ofstream::out | std::ofstream::binary);
  std::vector<uint8_t> keys;
  std::vector<int32_t> current_values;
  double max_diff = 0;

  // Windows filenames...
  std::replace(output_filename.begin(), output_filename.end(), '\\', '/');
  std::string header_filename = get_header_filename(format, output_filename);
  std::string record_name = header_filename.substr(header_filename.find_last_of("/") + 1);
  record_name = record_name.substr(0, record_name.find_first_of("."));

  // Any format with a header preceding data in a single file 
  // should be included in this if statement. The header is written
  // at the end since it often needs information (such as total 
  // number of samples written) that is obtained while performing
  // the output. The header function should seek to the beginning
  // of the file. 
  if (FILE_FORMAT_ISHNE == format)
  {
    outfile.seekp(ISHNE_DATA_START);
  }
  else if (FILE_FORMAT_WAVE == format)
  {
    outfile.seekp(WAVE_HEADER_SIZE);
  }

  for (auto &filename : filenames)
  {
    // Process a file

    data.clear();

    prev_header_info = header_info;
    ret_val = process_file(data, filename, false, key_string, iv_string, &header_info);

    if (!ret_val)
    {
      keys.clear();

      // Remove undesired channels from the output data to write.
      for (auto &kv : data)
      {
        keys.push_back(kv.first);
      }

	  // Not sure what this is for, but it seems to break things...
      //std::sort(keys.begin(), keys.end());

      for (uint32_t i = 0; i < keys.size(); ++i)
      {
        // channels are stored 1 based, not 0 based. If channels is not
        // selected (true), remove the channel corresponding to that key
        // from the data collection. 
        if (!channels[i + 1])
        {
          if (data.count(keys[i]) > 0) data.erase(keys[i]);
        }
      }

      if (first_time)
      {
        initial_header_info = header_info;
        for (uint32_t i = 0; i < data.size(); ++i)
        {
          current_values.push_back(0);
        }
      }

      if (!first_time && (NO_LIMIT != max_hours)
          && (time_difference(header_info, initial_header_info) + (data.begin()->second->size() / ((double)header_info.getSampleFrequency())))
          > (max_hours * SEC_PER_HOUR))
      {
        // Adding this file to the record would pass the maximum amount of time allowed.
        break;
      }

      // Count the total number of samples written to the output file. 
      total_samples += data.begin()->second->size();

      // fill a gap if fill_gaps has been specified.
	  time_diff = time_difference(header_info, prev_header_info); // <= time difference between file beginnings
	  time_diff -= duration_sec; // <= time elapsed during first file.
      if (fill_gaps && (time_diff > 2 || time_diff < -45) && !first_time && header_info.getSampleFrequency() != 0)
      {
        // If we've jumped more than three months forward or 45 seconds back, we probably got a clock reset,
        // so signify that with just one file's worth of data.
        if (time_diff > SEC_IN_3_MONTHS || time_diff < -45)
        {
          ret_val = write_square_wave(format, outfile, data.size(), header_info.getSampleFrequency(), AVG_FILE_LENGTH, current_values, (int32_t) ((-1) * header_info.getGain()), (int32_t) header_info.getGain());
        }
        // If the jump was smaller than that, simply write square wave data over the missing space.
        else
        {
          ret_val = write_square_wave(format, outfile, data.size(), header_info.getSampleFrequency(), (int32_t) time_diff, current_values, (int32_t) ((-1) * header_info.getGain()), (int32_t) header_info.getGain());
        }

        //ret_val = 0;
        if (ret_val)
        {
          // IO error occurred.
          outfile.close();
          remove(output_filename.c_str());
          return FILE_IO_ERROR;
        }
      }

      if (FILE_FORMAT_WFDB8 == format)
      {
        // store the current values of the channels for the
        // square wave function. 

        current_values.clear();
        keys.clear();
        for (auto &kv : data)
        {
          keys.push_back(kv.first);
        }
        std::sort(keys.begin(), keys.end());
        for (auto &k : keys)
        {
          current_values.push_back(data[k]->at(data[k]->size() - 1));
        }
      }

      // Store the time elapsed during the last file processed.
      duration_sec = data.begin()->second->size() / ((double)header_info.getSampleFrequency());
      first_time = false;

      ret_val = write_data(format, data, channels, outfile, record_name, current_values, header_info);
      if (ret_val)
      {
        // IO error occurred.
        outfile.close();
        remove(output_filename.c_str());
        return ret_val;
      }
    }
    else
    {
      // Error occurred. skip this file. 
      // Its missing data will be covered up with a square wave in the output
      // file on the next pass through the loop.
      header_info = prev_header_info;
      (*SCP_Parser::error) << "Error processing " << filename << ". skipping." << std::endl;
    }

#ifdef _WIN32
    if (nullptr != worker)
    {
      worker->ReportProgress(1);
      if (worker->CancellationPending)
      {
        // Delete the data file.
        outfile.close();
        remove(output_filename.c_str());
        return CANCELLED;
      }
    }
#endif
  }

  if (filenames.size() > 0 && 0 != total_samples)
  {
    std::ofstream header_file;
    if (header_filename != output_filename)
    {
      header_file.open(header_filename);
      if (!header_file.is_open())
      {
        // IO error occurred.
        remove(output_filename.c_str());
        remove(header_filename.c_str());
        return FILE_IO_ERROR;
      }
      ret_val = write_header(format, total_samples, channels, header_file, record_name, initial_header_info);
    }
    else
    {
      ret_val = write_header(format, total_samples, channels, outfile, record_name, initial_header_info);
    }

    if (ret_val)
    {
      return ret_val;
    }
    outfile.close();
    header_file.close();
  }
  else
  {
    return NO_SCP_FILES_FOUND;
  }

  return 0;
}

uint32_t SCP_Parser::write_square_wave(file_format_t format, std::ofstream& outfile, 
    uint32_t num_channels, double frequency_hz, uint32_t duration_seconds, 
    std::vector<int32_t>& current_val, int32_t low_val, int32_t high_val)
{
  return (this->*square_wave_functions[format]) (outfile, num_channels, frequency_hz, duration_seconds, current_val, low_val, high_val);
}

uint32_t SCP_Parser::write_data(file_format_t format, 
    std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
    std::map<uint32_t, bool> channels, std::ofstream& outfile,
    std::string record_name, std::vector<int32_t>& current_val, header_file_info& header_info)
{
  return (this->*data_functions[format])(data, channels, outfile, record_name, current_val, header_info);
}

uint32_t SCP_Parser::write_header(file_format_t format, uint64_t total_samples,
    std::map<uint32_t, bool> channels, std::ofstream& output, std::string record_name, 
    header_file_info& header_info)
{
  return (this->*header_functions[format])(total_samples, channels, output, record_name, header_info);
}

file_format_t SCP_Parser::string_to_format(std::string format)
{
  // remove spaces
  format.erase(remove_if(format.begin(), format.end(), ::isspace), format.end());

  // convert string to lowercase
  std::transform(format.begin(), format.end(), format.begin(), ::tolower);

  // check which one it should be
  if ("wfdb8" == format)
  {
    return FILE_FORMAT_WFDB8;
  }
  else if ("wfdb16" == format)
  {
    return FILE_FORMAT_WFDB16;
  }
  else if ("wfdb32" == format)
  {
    return FILE_FORMAT_WFDB32;
  }
  else if ("wfdb212" == format)
  {
    return FILE_FORMAT_WFDB212;
  }
  else if ("ishne" == format)
  {
    return FILE_FORMAT_ISHNE;
  }
  else if ("wave" == format || "wav" == format)
  {
    return FILE_FORMAT_WAVE;
  }

  return NUM_FORMATS;
}

std::string SCP_Parser::get_header_filename(file_format_t format, std::string data_filename)
{
  std::string header_filename;
  switch (format)
  {
    case FILE_FORMAT_WFDB32:
    case FILE_FORMAT_WFDB16:
    case FILE_FORMAT_WFDB8:
    case FILE_FORMAT_WFDB212:
      header_filename = data_filename.substr(0, data_filename.find_last_of("."));
      if (0 == header_filename.size())
      {
        header_filename = data_filename;
      }
      header_filename.append(WFDB_HEADER_EXTENSION);
      break;

    case FILE_FORMAT_ISHNE:
    case FILE_FORMAT_WAVE:
      header_filename = data_filename;
      break;

    default:
      break;
  }

  return header_filename;
}
