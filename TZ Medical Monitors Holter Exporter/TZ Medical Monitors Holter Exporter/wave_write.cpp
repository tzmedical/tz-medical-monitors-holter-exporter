/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include "wave_write.h"

using namespace std;




///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
uint32_t wave_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
	double frequency_hz, double duration_seconds, std::vector<int32_t>& current_val,
	int32_t low_val, int32_t high_val, std::vector<int16_t>& checksum)
{
	// TODO: this
	return 0;
}

//-----------------------------------------------------------------------------
uint32_t wave_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
	std::ofstream& output, std::string record_name, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	uint8_t buffer[WAVE_HEADER_SIZE];
	uint8_t* p_buffer = buffer;

	// Count the number of channels to write to the file.
	uint16_t num_channels = 0;
	for (uint32_t i = 1; i <= header_info.leadCount; ++i)
	{
		if (channels[i])
		{
			++num_channels;
		}
	}

	uint32_t int_holder_32;
	uint16_t int_holder_16;

	memcpy(p_buffer, "RIFF", 4); // ChunkID
	p_buffer += 4;

	int_holder_32 = (uint32_t)(total_samples * num_channels * 2 * WAVE_SAMPLE_RATE / header_info.getSampleFrequency() + 36);
	memcpy(p_buffer, &int_holder_32, 4); // ChunkSize
	p_buffer += 4;

	memcpy(p_buffer, "FILE_FORMAT_WAVE", 4); // Format
	p_buffer += 4;

	memcpy(p_buffer, "fmt ", 4); // Subchunk1 ID
	p_buffer += 4;

	int_holder_32 = 16;
	memcpy(p_buffer, &int_holder_32, 4); // Subchunk1 Size
	p_buffer += 4;

	int_holder_16 = 1;
	memcpy(p_buffer, &int_holder_16, 2); // AudioFormat
	p_buffer += 2;

	memcpy(p_buffer, &num_channels, 2); // NumChannels
	p_buffer += 2;

	int_holder_32 = WAVE_SAMPLE_RATE;
	memcpy(p_buffer, &int_holder_32, 4); // SampleRate
	p_buffer += 4;

	int_holder_32 = WAVE_SAMPLE_RATE * num_channels * 2; // 2 is bytes per sample.
	memcpy(p_buffer, &int_holder_32, 4); // ByteRate
	p_buffer += 4;

	int_holder_16 = num_channels * 2;
	memcpy(p_buffer, &int_holder_16, 2); // BlockAlign
	p_buffer += 2;

	int_holder_16 = 2;
	memcpy(p_buffer, &int_holder_16, 2); // BitsPerSample
	p_buffer += 2;

	memcpy(p_buffer, "data", 4); // Subchunk2ID
	p_buffer += 4;

	int_holder_32 = (uint32_t)(total_samples * num_channels * 2 * WAVE_SAMPLE_RATE / header_info.getSampleFrequency()); // 2 is bytes per sample.
	memcpy(p_buffer, &int_holder_32, 4); // SubChunk2Size
	p_buffer += 4;

	output.seekp(0);
	output.write((char*)buffer, WAVE_HEADER_SIZE);

	return 0;
}

//-----------------------------------------------------------------------------
uint32_t wave_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
	std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
	std::vector<int32_t>& current_val, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	if (outfile.is_open())
	{
		uint32_t base_frequency = 1900;

		// Represents the number of of wav samples in on sample of ecg data.
		uint32_t samples_per_sample = WAVE_SAMPLE_RATE / header_info.getSampleFrequency();
		uint32_t amplitude = 30000;
		int16_t val;
		uint32_t frequency = 0;



		std::vector<uint8_t> channel_ids;
		for (auto &kv : data)
		{
			channel_ids.push_back(kv.first);
		}
		std::sort(channel_ids.begin(), channel_ids.end());


		size_t buffer_entries = (data.begin()->second->size()) * (channel_ids.size());
		buffer_entries *= samples_per_sample;
		size_t buffer_index = 0;
		int16_t *p_buffer = new int16_t[buffer_entries];

		std::vector<int32_t> **p_data;
		p_data = new vector<int32_t>*[channel_ids.size()];
		for (uint32_t j = 0; j < channel_ids.size(); ++j)
		{
			p_data[j] = data[channel_ids[j]].get();
		}

		// For each sample
		for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
		{
			// output a sine wave
			for (uint32_t k = 0; k < samples_per_sample; ++k)
			{
				// For each channel
				for (uint32_t j = 0; j < channel_ids.size(); ++j)
				{
					//frequency = base_frequency + 0;
					frequency = base_frequency + p_data[j]->at(i) / header_info.getGain();

					val = (int16_t)(sin((k / 28) * 2 * 3.14159 / frequency) * amplitude); // f = 2*pi / b, b = 2*pi / f

					p_buffer[buffer_index++] = val;
				}
			}
		}
		outfile.write(reinterpret_cast<const char*>(p_buffer), buffer_index * sizeof(int16_t));

		delete[] p_data;
		delete[] p_buffer;
	}
	else
	{
		// Error writing to file.
		return FILE_IO_ERROR;
	}

	return 0;
}