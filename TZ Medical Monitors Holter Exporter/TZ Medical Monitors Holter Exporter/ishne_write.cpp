/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include "ishne_write.h"
#include "wfdb16_write.h"

using namespace std;




///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
uint32_t ishne_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
	double frequency_hz, double duration_seconds, std::vector<int32_t>& current_val,
	int32_t low_val, int32_t high_val, std::vector<int16_t>& checksum)
{
	// These two formats store data the same way.
	return wfdb16_write_square_wave(outfile, num_channels, frequency_hz, duration_seconds, current_val, low_val, high_val, checksum);
}

//-----------------------------------------------------------------------------
uint32_t ishne_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
	std::ofstream& output, std::string record_name, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	char magic_number[] = "ISHNE1.0";
	uint16_t crc = 0xffff;
	uint32_t zero = 0;

	// Count the number of channels to write to the file.
	uint32_t num_channels = 0;
	for (uint32_t i = 1; i <= header_info.leadCount; ++i)
	{
		if (channels[i])
		{
			++num_channels;
		}
	}

	ishne_header_t header =
	{
		0,
		(int32_t)total_samples,
		522,
		ISHNE_DATA_START,
		1,
		"", // First name
		"", // Last name
		"", // ID #
		0,
		0,
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	(int16_t)num_channels,
	{ ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
	{ ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
	{ ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT, ISHNE_NO_LEAD_PRESENT },
	0,
	"", // Recorder type
	(int16_t)header_info.getSampleFrequency(),
	"", // Proprietary of ECG data
	"", // Copyright
	""  // Reserved
	};

	std::string record_date = header_info.getDate(); // dd/mm/yyyy
	header.Record_Date[0] = std::stoi(record_date.substr(0, record_date.find_first_of("/")));	// day
	header.Record_Date[1] = std::stoi(record_date.substr(record_date.find_first_of("/") + 1, record_date.find_last_of("/") - record_date.find_first_of("/") - 1));	// month
	header.Record_Date[2] = std::stoi(record_date.substr(record_date.find_last_of("/") + 1));	// year

	time_t t = time(0);
	struct tm * now = localtime(&t);
	header.File_Date[0] = now->tm_mday; // day
	header.File_Date[1] = now->tm_mon + 1; // month
	header.File_Date[2] = now->tm_year + 1900; // year

	std::string start_time = header_info.getTime(); // hh:mm:ss
	header.Start_Time[0] = std::stoi(start_time.substr(0, start_time.find_first_of(":")));	// day
	header.Start_Time[1] = std::stoi(start_time.substr(start_time.find_first_of(":") + 1, start_time.find_last_of(":") - start_time.find_first_of(":") - 1));	// month
	header.Start_Time[2] = std::stoi(start_time.substr(start_time.find_last_of(":") + 1));	// year

	for (uint32_t i = 0; i < num_channels; ++i)
	{
		header.Lead_Spec[i] = 0;
		header.Lead_Qual[i] = 0;
		header.Resolution[i] = header_info.file_res;
	}

	char buffer[512];
	char* p_buffer = buffer;
	for (uint32_t i = 0; i < 512; ++i)
	{
		buffer[i] = 0;
	}

	memcpy(p_buffer, &header.Var_Length_Block_Size, 4);
	p_buffer += 4;
	memcpy(p_buffer, &header.Sample_Size_ECG, 4);
	p_buffer += 4;
	memcpy(p_buffer, &header.Offset_var_length_block, 4);
	p_buffer += 4;
	memcpy(p_buffer, &header.Offset_ECG_block, 4);
	p_buffer += 4;
	memcpy(p_buffer, &header.File_Version, 2);
	p_buffer += 2;
	memcpy(p_buffer, header.First_Name, 40);
	p_buffer += 40;
	memcpy(p_buffer, header.Last_Name, 40);
	p_buffer += 40;
	memcpy(p_buffer, header.ID, 20);
	p_buffer += 20;
	memcpy(p_buffer, &header.Sex, 2);
	p_buffer += 2;
	memcpy(p_buffer, &header.Race, 2);
	p_buffer += 2;
	memcpy(p_buffer, &header.Birth_Date, 6);
	p_buffer += 6;
	memcpy(p_buffer, &header.Record_Date, 6);
	p_buffer += 6;
	memcpy(p_buffer, &header.File_Date, 6);
	p_buffer += 6;
	memcpy(p_buffer, &header.Start_Time, 6);
	p_buffer += 6;
	memcpy(p_buffer, &header.nLeads, 2);
	p_buffer += 2;
	memcpy(p_buffer, &header.Lead_Spec, 24);
	p_buffer += 24;
	memcpy(p_buffer, &header.Lead_Qual, 24);
	p_buffer += 24;
	memcpy(p_buffer, &header.Resolution, 24);
	p_buffer += 24;
	memcpy(p_buffer, &header.Pacemaker, 2);
	p_buffer += 2;
	memcpy(p_buffer, header.Recorder, 40);
	p_buffer += 40;
	memcpy(p_buffer, &header.Sampling_Rate, 2);
	p_buffer += 2;
	memcpy(p_buffer, header.Proprietary, 80);
	p_buffer += 80;
	memcpy(p_buffer, header.Copyright, 80);
	p_buffer += 80;
	memcpy(p_buffer, header.Reserved, 88);
	p_buffer += 88;

	// calculate checksum
	SCP_Parser::crcBlock((uint8_t*)buffer, 512, &crc); // Calculate the CRC for the remainder of the file
	output.seekp(0);
	output.write(magic_number, 8);
	output.write((char*)&crc, 2);
	output.write(buffer, 512);
	//output.write((char*) &zero, 4); // Write a few bytes since we need something for the variable length block. 

	return 0;
}
//-----------------------------------------------------------------------------
uint32_t ishne_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
	std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
	std::vector<int32_t>& current_val, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	// These two formats store data the same way.
	return wfdb16_write_data(data, channels, outfile, record_name, current_val, header_info, checksum);
}
