/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*
*
*
*****************************************************************************/


#ifndef TZ_MEDICAL_MONITORS_HOLTER_EXPORTER_H
#define TZ_MEDICAL_MONITORS_HOLTER_EXPORTER_H

#include <vcclr.h>
#include <msclr/marshal_cppstd.h>
#include <Windows.h>
#include <Shellapi.h>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "tchar.h"
#include "stdlib.h"
#include "getopt.h"
#include "Shlwapi.h"
#include "wchar.h"
#include "scp_parser.h"

#include "Frame.h"

// The version number of the application
// 0.1 - Initial version, support for wfdb8, wfdb16, wfdb32, wfdb212, and ishne formats.
// 0.2 - Added support for selecting a random segment from the record, bug fixes
// 0.3 - Added support for HELX format, bug fixes
// 0.4 - Optimized performance, added support for scpecg.bin feature
#define VERSION_NUMBER ("0.4")

// The name of the folder to look for on the top level of the card.
#define TOP_FOLDER_NAME ("ecgs")

// A regex matching the type of file to use as input
#define INPUT_FILE_FORMAT ("*.scp")

#define ONE_ECG_FILE_FORMAT	("*.bin")

// The maximum number of channels on a Holter
#define NUM_CHANNELS (12)

// The number of arguments required for the program to run.
#define REQUIRED_ARGS (3)

// The number of times to retry selecting a random record
// after a gap is encountered during the random time period
// If this many tries does not yield a valid, continuous
// record, give up.
#define MAX_RANDOM_ATTEMPTS (10)

using namespace System;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;

//! \brief Export ecg data to an output file. 
//! This function walks through the files in the input folder, parses them,
//! and writes the data to the output file.
//! \param folder The folder containing the existing ecgs data to export.
//! \param output_filename The file to write the output to.
//! \param format The file format to use for exporting.
//! \param channels A dictionary of booleans indexed by the available 
//!        channels. True signifies that they should be included in the
//!        exported data, false that they should not. 
//! \param start_time How many minutes from the start of the record should we start exporting?
//! \param length How many seconds of data should we export?
//! \param random_start The maximum number of hours of data to export.
//! \param random_start Should gaps in the record be filled with square wave data?
//! \param new_lsb The new target Least Significan Byte (LSB). 
//!	The amount of nV needed to move the value by one bit.
//! \param new_sample_rate The new sample rate, in Hz.
//! \param worker The background worker running this function, if it exists. 
int export_data(String^ folder, String^ output_filename, String^ format, Dictionary<int, Boolean>^ channels, double max_hours,
	int32_t start_time, int32_t length, bool fill_gaps, bool random_start, int32_t new_lsb, int32_t new_sample_rate, BackgroundWorker^ worker);

//! \brief Checks to see if a directory exists.
//! \param filename The directory to check
//! \return True if the directory exists, false otherwise.
bool dirExists(String^ filename);

//! \brief Checks if a string is contained within an array.
//! \brief \param the_array The array to check.
//! \brief \param the_array_length The length of the array.
//! \brief \param val The string to search for.
BOOL contains(const char * the_array[], int the_array_length, char * val);

//! \brief Converts a wchar_t to a lowercase char*. The resulting string must 
//! be deleted by the caller.
//! \param orig The string to convert
//! \return A lowercase char* that must be deleted by the caller.
char* wchar_to_lower_c_str(wchar_t* orig);

//! \brief Prints the programs usage.
void print_usage();

//! \brief Prints the programs help.
void print_help();

//! \brief Helper function for the usage and help functions.
void print_one_line_usage();

//! \brieft Prints an error message.
//! \param error_msg the error message to print
void print_error(String^ error_msg);

#endif // TZ_MEDICAL_MONITORS_HOLTER_EXPORTER_H
