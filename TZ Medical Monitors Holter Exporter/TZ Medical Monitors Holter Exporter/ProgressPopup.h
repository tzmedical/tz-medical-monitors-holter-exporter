/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*
*
*
*****************************************************************************/

#ifndef PROGRESS_POPUP_H
#define PROGRESS_POPUP_H

#include "TZ_Medical_Monitors_Holter_Exporter.h"

namespace TZMedicalMonitorsHolterExporter {

  using namespace System;
  using namespace System::IO;
  using namespace System::ComponentModel;
  using namespace System::Collections;
  using namespace System::Collections::Generic;
  using namespace System::Windows::Forms;
  using namespace System::Data;
  using namespace System::Drawing;

  /// <summary>
  /// A popup which displays the progress of parsing the files. 
  /// </summary>
  public ref class ProgressPopup : public System::Windows::Forms::Form
  {
  public:

	  ProgressPopup(String^ drive, String^ output_filename, String^ format, Dictionary<int, Boolean>^ channels, double hours)
	  {
		  InitializeComponent();

		  this->set_drive(drive);
		  this->set_output_filename(output_filename);
		  this->set_format(format);
		  this->set_channels(channels);
		  this->set_max_hours(hours);
          this->split_file = 0;

          // Report the progress as a percentage 0-100
          this->progressBar1->Minimum = 0;
          this->progressBar1->Maximum = 100;

          //helx needs channels in seperate files
          if ("helx" == format)
          {
              this->label1->Text = L"Copying data. This might take a few minutes.";
              this->set_base_output_filename(output_filename);
              initialize_helx_worker(1);
          }

          start_new_worker();
	  }

  protected:
	  /// <summary>
	  /// Clean up any resources being used.
	  /// </summary>
	  ~ProgressPopup()
	  {
		  if (components)
		  {
			  delete components;
		  }
	  }

  private:
	  // m_ denotes member variable.
	  BackgroundWorker^ m_worker = nullptr;
	  String^ m_drive = "";
	  String^ m_output_filename = "";
      String^ m_base_output_filename = "";
	  String^ m_format;
	  double m_hours;
	  Dictionary<int, Boolean>^ m_channels;

	  Dictionary<int, Boolean>^ get_channels()
	  {
		  return m_channels;
	  }

	  void set_channels(Dictionary<int, Boolean>^ channels)
	  {
		  m_channels = channels;
	  }

	  void set_max_hours(double hours)
	  {
		  m_hours = hours;
	  }

	  double get_max_hours()
	  {
		  return m_hours;
	  }

	   String^ get_format()
	   {
		   return this->m_format;
	   }

	   void set_format(String^ format)
	   {
		   this->m_format = format;
	   }


	  String^ get_drive()
	  {
		  return this->m_drive;
	  }

	  void set_drive(String^ drive)
	  {
		  this->m_drive = drive;
	  }

	  String^ get_output_filename()
	  {
		  return this->m_output_filename;
	  }

	  void set_output_filename(String^ output_filename)
	  {
		  this->m_output_filename = output_filename;
	  }

      void set_base_output_filename(String^ output_filename)
      {
          this->m_base_output_filename = output_filename;
      }

      void initialize_helx_worker(int channel)
      {
          this->split_file = channel;
          this->set_output_filename(m_base_output_filename + (channel - 1) + ".dat");

          Dictionary<int, Boolean>^ tmp_channels = gcnew Dictionary<int, Boolean>();
          tmp_channels[channel] = true;
          this->set_channels(tmp_channels);
      }

      void start_new_worker()
      {
          m_worker = gcnew BackgroundWorker();

          m_worker->DoWork += gcnew DoWorkEventHandler(this, &TZMedicalMonitorsHolterExporter::ProgressPopup::worker_do_work);
          m_worker->ProgressChanged += gcnew ProgressChangedEventHandler(this, &TZMedicalMonitorsHolterExporter::ProgressPopup::worker_progress_changed);
          m_worker->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler(this, &TZMedicalMonitorsHolterExporter::ProgressPopup::worker_run_completed);
          m_worker->WorkerReportsProgress = true;
          m_worker->WorkerSupportsCancellation = true;

          // Launch background worker thread
          m_worker->RunWorkerAsync();
      }

      void worker_do_work(Object^ sender, System::ComponentModel::DoWorkEventArgs^ e)
      {
		  // TODO: Add support for these new features to the GUI
		  // Drive has already been checked for validity
		  int32_t test = export_data(System::IO::Path::Combine(this->get_drive(), TOP_FOLDER_NAME), this->get_output_filename(), 
			  this->get_format(), this->get_channels(), this->get_max_hours(), NO_LIMIT, NO_LIMIT, true, false, NO_LIMIT, NO_LIMIT, m_worker);
		  e->Result = test;
      }

      void worker_run_completed(Object^ sender, System::ComponentModel::RunWorkerCompletedEventArgs^ e)
      {
          if ("helx" != this->m_format || this->split_file == 3)
          {
              this->popup_ok->Enabled = true;
              this->Cancel->Enabled = false;
              this->UseWaitCursor = false;
              this->Cursor = DefaultCursor;
              if (0 == !e->Result)
              {
                  this->progressBar1->Value = this->progressBar1->Maximum;
                  this->label1->Text = "Done!";
              }
              else
              {
                  this->label1->Text = "An error occurred.";
                  this->progressBar1->Value = 0;
              }
          }
          else
          {
              this->split_file++;
              initialize_helx_worker(this->split_file);
              start_new_worker();
          }
      }

      void worker_progress_changed(Object^ sender, System::ComponentModel::ProgressChangedEventArgs^ e)
      {
          int percent = 0;

          if (split_file)
          {
              int file_percent = e->ProgressPercentage / 3;
              int base_percent = (split_file - 1) * 100 / 3;
              percent = base_percent + file_percent;
          }
          else
          {
              percent = e->ProgressPercentage;
          }


          progressBar1->Value = percent;
      }

    private: System::Windows::Forms::ProgressBar^  progressBar1;
    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Button^  Cancel;

    private: System::Windows::Forms::Button^  popup_ok;

    private: boolean skip_confirmation;
    private: int split_file;


    protected:
    private: System::ComponentModel::IContainer^  components;

    private:
             /// <summary>
             /// Required designer variable.
             /// </summary>


#pragma region Windows Form Designer generated code
             /// <summary>
             /// Required method for Designer support - do not modify
             /// the contents of this method with the code editor.
             /// </summary>
             void InitializeComponent(void)
             {
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ProgressPopup::typeid));
				 this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
				 this->label1 = (gcnew System::Windows::Forms::Label());
				 this->popup_ok = (gcnew System::Windows::Forms::Button());
				 this->Cancel = (gcnew System::Windows::Forms::Button());
				 this->SuspendLayout();
				 // 
				 // progressBar1
				 // 
				 this->progressBar1->Cursor = System::Windows::Forms::Cursors::Default;
				 this->progressBar1->Location = System::Drawing::Point(12, 34);
				 this->progressBar1->Maximum = 10;
				 this->progressBar1->Name = L"progressBar1";
				 this->progressBar1->Size = System::Drawing::Size(289, 23);
				 this->progressBar1->Step = 1;
				 this->progressBar1->Style = System::Windows::Forms::ProgressBarStyle::Continuous;
				 this->progressBar1->TabIndex = 0;
				 // 
				 // label1
				 // 
				 this->label1->AutoSize = true;
				 this->label1->Location = System::Drawing::Point(12, 9);
				 this->label1->Name = L"label1";
				 this->label1->Size = System::Drawing::Size(289, 18);
				 this->label1->TabIndex = 1;
				 this->label1->Text = L"Exporting data. This might take a few minutes.";
				 this->label1->Click += gcnew System::EventHandler(this, &ProgressPopup::label1_Click);
				 // 
				 // popup_ok
				 // 
				 this->popup_ok->Enabled = false;
				 this->popup_ok->Location = System::Drawing::Point(226, 63);
				 this->popup_ok->Name = L"popup_ok";
				 this->popup_ok->Size = System::Drawing::Size(75, 25);
				 this->popup_ok->TabIndex = 3;
				 this->popup_ok->Text = L"OK";
				 this->popup_ok->UseVisualStyleBackColor = true;
				 this->popup_ok->Click += gcnew System::EventHandler(this, &ProgressPopup::popup_ok_Click);
				 // 
				 // Cancel
				 // 
				 this->Cancel->Location = System::Drawing::Point(145, 63);
				 this->Cancel->Name = L"Cancel";
				 this->Cancel->Size = System::Drawing::Size(75, 25);
				 this->Cancel->TabIndex = 4;
				 this->Cancel->Text = L"Cancel";
				 this->Cancel->UseVisualStyleBackColor = true;
				 this->Cancel->Click += gcnew System::EventHandler(this, &ProgressPopup::Cancel_Click);
				 // 
				 // ProgressPopup
				 // 
				 this->AcceptButton = this->popup_ok;
				 this->AutoScaleDimensions = System::Drawing::SizeF(8, 18);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(312, 105);
				 this->ControlBox = false;
				 this->Controls->Add(this->Cancel);
				 this->Controls->Add(this->popup_ok);
				 this->Controls->Add(this->label1);
				 this->Controls->Add(this->progressBar1);
				 this->Cursor = System::Windows::Forms::Cursors::Arrow;
				 this->Font = (gcnew System::Drawing::Font(L"Corbel", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->Margin = System::Windows::Forms::Padding(4);
				 this->Name = L"ProgressPopup";
				 this->Text = L"Please Wait...";
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion
    private: System::Void notifyIcon1_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
             }
    private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
             }
    private: System::Void popup_ok_Click(System::Object^  sender, System::EventArgs^  e)
             {
               this->Close();
             }
  private: System::Void Cancel_Click(System::Object^  sender, System::EventArgs^  e)
  {
      this->m_worker->CancelAsync();
	  this->Close();
  }

};
}

#endif // PROGRESS_POPUP_H
