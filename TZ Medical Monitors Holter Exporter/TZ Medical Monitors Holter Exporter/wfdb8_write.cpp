/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include "wfdb8_write.h"

using namespace std;




///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
uint32_t wfdb8_write_square_wave(std::ofstream& outfile, uint32_t num_channels,
	double frequency_hz, double duration_seconds, std::vector<int32_t>& current_val,
	int32_t low_val, int32_t high_val, std::vector<int16_t>& checksum)
{
	int32_t output = 0;
	int32_t target = 0;

	low_val = -128;
	high_val = 127;

	// For each second
	for (uint32_t i = 0; i < (int32_t)duration_seconds; ++i)
	{
		target = low_val;
		// Write half the duration as low value, and half as high value
		for (uint32_t cc = 0; cc < 2; ++cc)
		{
			// For half the samples in the second
			for (uint32_t j = 0; j < frequency_hz / 2; ++j)
			{
				// For each channel
				for (uint32_t k = 0; k < num_channels; ++k)
				{
					output = target - current_val[k];
					output = min(output, (int32_t)127);
					output = max(output, (int32_t)-128);
					current_val.at(k) += output;


					checksum[k] += current_val[k];
					outfile.write(reinterpret_cast<const char*>(&output), sizeof(int8_t));
				}
			}
			target = high_val;
		}
	}

	// If we got a fractional duration of seconds to fill up, extend the last "high" wave
	// to cover the difference.
	target = high_val;
	uint32_t n = std::round((duration_seconds - (int32_t)duration_seconds) * frequency_hz);
	for (uint32_t i = 0; i < n; ++i)
	{
		for (uint32_t k = 0; k < num_channels; ++k)
		{
			output = target - current_val[k];
			output = min(output, (int32_t)127);
			output = max(output, (int32_t)-128);
			current_val.at(k) += output;

			checksum[k] += current_val[k];
			outfile.write(reinterpret_cast<const char*>(&output), sizeof(int8_t));
		}
	}

	return 0;
}

//-----------------------------------------------------------------------------
uint32_t wfdb8_write_header(uint64_t total_samples, std::map<uint32_t, bool> channels,
	std::ofstream& output, std::string record_name, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	// Count the number of channels to write to the file.
	uint32_t num_channels = 0;
	for (uint32_t i = 1; i <= header_info.leadCount; ++i)
	{
		if (channels[i])
		{
			++num_channels;
		}
	}

	// generate and write header information
	output << record_name << " " << num_channels
		<< " " << header_info.getSampleFrequency()
		<< " " << total_samples
		<< " " << header_info.getTime()
		<< " " << header_info.getDate()
		<< std::endl;
	double gain = header_info.getGain();
	for (uint32_t i = 0; i < num_channels; ++i)
	{
		output << record_name << ".dat 8" << " " << (gain)
			<< " " << (header_info.file_res) << " 0 "
			<< header_info.getInitialValue(i)
			<< " " << checksum[i] << " 0 " << header_info.getChannelLabel(i)
			<< std::endl;
	}

	return 0;
}

//-----------------------------------------------------------------------------
uint32_t wfdb8_write_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
	std::map<uint32_t, bool> channels, std::ofstream& outfile, std::string record_name,
	std::vector<int32_t>& current_val, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	if (outfile.is_open())
	{
		// Get a sorted list of the keys so we can iterate reliably.
		std::vector<uint8_t> channel_ids;
		for (auto &kv : data)
		{
			channel_ids.push_back(kv.first);
		}
		std::sort(channel_ids.begin(), channel_ids.end());

		// Write the data in the order lead 1, sample 1, lead 2 sample 1, lead 1 sample 2, lead 2 sample 2, ...
		size_t buffer_entries = (data.begin()->second->size()) * (channel_ids.size());
		size_t buffer_index = 0;
		int8_t *p_buffer = new int8_t[buffer_entries];

		std::vector<int32_t> **p_data;
		p_data = new vector<int32_t>*[channel_ids.size()];
		for (uint32_t j = 0; j < channel_ids.size(); ++j)
		{
			p_data[j] = data[channel_ids[j]].get();
		}

		for (uint32_t i = 0; i < data.begin()->second->size(); ++i)
		{
			for (uint32_t j = 0; j < channel_ids.size(); ++j)
			{
				int32_t output = p_data[j]->at(i) - current_val[j];
				output = min(output, (int32_t)127);
				output = max(output, (int32_t)-128);
				current_val.at(j) += output;
				checksum[j] += output;
				p_buffer[buffer_index++] = output;
			}
		}
		outfile.write(reinterpret_cast<const char*>(p_buffer), buffer_index * sizeof(int8_t));

		delete[] p_data;
		delete[] p_buffer;
	}
	else
	{
		// Error writing to file.
		return FILE_IO_ERROR;
	}

	return 0;
}
