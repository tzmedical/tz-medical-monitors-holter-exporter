/******************************************************************************
 *       Copyright (c) 2014, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 *
 *
 *****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include "scp_parser.h"

#include "wfdb32_write.h"
#include "wfdb16_write.h"
#include "wfdb212_write.h"
#include "wfdb8_write.h"
#include "wave_write.h"
#include "helx_write.h"
#include "ishne_write.h"


using namespace System;

// define LINUX_CMDLINE to compile on Linux.
#if defined(_WIN32) || defined(LINUX_CMDLINE)
std::ostream * SCP_Parser::error = &std::cerr;
#else
#include <phpcpp.h>
std::ostream * SCP_Parser::error = &Php::error;
#endif



using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC          1

#define SCP_ID_STRING      "SCPECG"
#define LEAD_ID_BASE       (127)

///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

uint16_t const SCP_Parser::ccitt_crc16_table[256] = {
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int32_t SCP_Parser::crcBlock(unsigned char *data, uint32_t length, uint16_t *crcVal)
{
	uint32_t j;
	if (length)
	{
		for (j = 0; j < length; j++)
		{
			*crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal >> 8)) & 0xff] ^ (*crcVal << 8);
		}
	}
	return 0;
}

unsigned char SCP_Parser::read8(unsigned char *pData, uint32_t offset)
{
	unsigned char *pC = &pData[offset];
	return *pC;
}

uint16_t SCP_Parser::read16(unsigned char *pData, uint32_t offset)
{
	unsigned char *pC = &pData[offset];
	return (uint16_t)pC[0] + (uint16_t)pC[1] * 256;
}

uint32_t SCP_Parser::read32(unsigned char *pData, uint32_t offset)
{
	unsigned char *pC = &pData[offset];
	return (((uint32_t)pC[3] * 256 + (uint32_t)pC[2]) * 256
		+ (uint32_t)pC[1]) * 256 + (uint32_t)pC[0];
}

int32_t SCP_Parser::sectionHeader(unsigned char *pData, uint32_t *length)
{
	uint16_t theircrcValue = read16(pData, 0);           // CRC for section
	uint32_t id = read16(pData, 2);                      // Section ID for Section
	*length = read32(pData, 4);                          // Length for Section
	uint32_t sVersion = read8(pData, 8);                 // Section Version (2.2)
	uint32_t pVersion = read8(pData, 9);                 // Protocol Version (2.2)

	uint16_t ourcrcValue = 0xffff;
#if CHECK_CRC == 1
	crcBlock(&pData[2], (*length) - 2, &ourcrcValue);
#else
	ourcrcValue = theircrcValue;
#endif

	if (ourcrcValue != theircrcValue)
	{
		(*SCP_Parser::error) << "ERROR: CRC error! file: 0x" << hex << theircrcValue
			<< " - calculated: 0x" << ourcrcValue << dec << endl;
		return CRC_INVALID;
	}

	return 0;
}


unsigned char * SCP_Parser::get_bits(uint32_t *out_data, uint32_t bits,
	unsigned char *in_byte, unsigned char *bit_num, uint32_t sign_extend)
{
	uint32_t sign_bit = bits - 1;
	*out_data = 0;

	while (bits)
	{
		if (bits <= *bit_num)
		{
			*out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
			*bit_num -= bits;
			bits = 0;
		}
		else
		{
			*out_data |= ((uint32_t)*in_byte & (0x00ff >> (8 - *bit_num))) << (bits - *bit_num);
			bits -= *bit_num;
			*bit_num = 0;
		}

		if (0 == *bit_num)
		{
			*bit_num += 8;
			in_byte++;
		}
	}

	// Sign extend the value we read
	if (sign_extend && (*out_data & (1 << sign_bit)))
	{
		*out_data |= (~0) << sign_bit;
	}

	return in_byte;
}

///////////////////////////////////////////////////////////////////////////////
//       Public Functions
///////////////////////////////////////////////////////////////////////////////

uint32_t SCP_Parser::process_file(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_data,
	std::fstream *in_file,
	int32_t interleaved,
	header_file_info* header_info)
{
	uint8_t * p_read = NULL;
	uint32_t file_length;

	uint32_t ret_val = read_file(in_file, &p_read, &file_length);
	if (ret_val)
	{
		return ret_val;
	}
	if (file_length < MIN_SCP_LENGTH_BYTES)
	{
		ret_val = INVALID_LENGTH;
	}
	else if (file_length > MAX_SCP_LENGTH_BYTES)
	{
		ret_val = INVALID_LENGTH;
	}
	else
	{
		ret_val = process_file_data(preliminary_data, p_read, file_length, interleaved,
			header_info);
	}

	delete[] p_read;
	return ret_val;
}

uint32_t SCP_Parser::time_difference(header_file_info& newer, header_file_info& older)
{
	// returns the difference in seconds between the timestamps of the two header files. 
	// Note that this is the time gap between the beginning of each file.
  struct tm t = {0};
  t.tm_year = newer.year - 1900;
  t.tm_mon = newer.month - 1;
  t.tm_mday = newer.day;
  t.tm_hour = newer.hour;
  t.tm_min = newer.minutes;
  t.tm_sec = newer.seconds;
  time_t new_epoch = mktime(&t);
  struct tm t2 = { 0 };
  t2.tm_year = older.year - 1900;
  t2.tm_mon = older.month - 1;
  t2.tm_mday = older.day;
  t2.tm_hour = older.hour;
  t2.tm_min = older.minutes;
  t2.tm_sec = older.seconds;
  time_t old_epoch = mktime(&t2);

	return new_epoch - old_epoch;
}

uint32_t SCP_Parser::read_file(std::fstream *in_file, uint8_t ** p_read,
	uint32_t * fileLength)
{
	// open the file.
	if (!in_file->is_open())
	{
		(*SCP_Parser::error) << "File not opened" << std::flush;
		return FILE_IO_ERROR;
	}

	uint8_t temp_array[32];
	in_file->read((char *)temp_array, sizeof(temp_array));

	if (in_file->eof())
	{
		return SCP_END_OF_FILE;
	}


	std::string format_str;
	format_str.assign((const char *)&temp_array[16], 6);
	if (format_str.compare(0, 6, "SCPECG"))
	{
		in_file->close();
		return SCP_END_OF_FILE;
	}

	*fileLength = temp_array[2] + (temp_array[3] << 8) + (temp_array[4] << 16) + (temp_array[5] << 24);

	// Make sure the file is not too small for the format
	if ((*fileLength) < MIN_SCP_LENGTH_BYTES)
	{
		in_file->close();
		(*SCP_Parser::error) << "File is to small to be an SCP file" << std::flush;
		return UNEXPECTED_FILE_CONTENTS;
	}
	else if ((*fileLength) > MAX_SCP_LENGTH_BYTES)
	{
		in_file->close();
		(*SCP_Parser::error) << "File is to large to be an SCP file" << std::flush;
		return UNEXPECTED_FILE_CONTENTS;
	}

	(*p_read) = new uint8_t[*fileLength];   // Allocate enough space to read in the whole file     
	memcpy(*p_read, temp_array, sizeof(temp_array));
	in_file->read((char *)&(*p_read)[sizeof(temp_array)], *fileLength - sizeof(temp_array));    // Store the remainder of the file in memory

	return 0;
}

uint32_t SCP_Parser::process_file_data(std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& preliminary_results,
	uint8_t *file_data,
	uint32_t file_length,
	int32_t interleaved,
	header_file_info* header_info)
{

	uint16_t calculatedCrcValue;           // The CRC we calculate
	uint16_t theircrcValue;                // The CRC from the file
	uint32_t length;                         // The length read from the file

	uint32_t i;

	string str;                                  // Check that we are reading an SCP file
	str.assign((const char*)&file_data[16], 6);  // Copy what should be the "SCPECG" string
	if (str.compare(0, 6, SCP_ID_STRING))
	{
		(*SCP_Parser::error) << "Invalid File Format String. Aborting." << endl;
		return UNEXPECTED_FILE_CONTENTS;
	}
	else{
		theircrcValue = read16(file_data, 0);
		length = read32(file_data, 2);

		if (length > file_length)
		{
			(*SCP_Parser::error) << "File Corrupted. Invalid Length value. Aborting." << endl;
			return UNEXPECTED_FILE_CONTENTS;
		}

		calculatedCrcValue = 0xffff;                            // CRC is based off an initial value of 0xffff
		crcBlock(&file_data[2], length - 2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
	}

#if CHECK_CRC == 0
	calculcatedCrcValue = theircrcValue;
#endif

	if (calculatedCrcValue == theircrcValue)
	{
		// If the CRC doesn't match, something has gone wrong
		uint32_t sI = 6;
		uint32_t j;

		/**********************************************************************************************
		 *          Section 0
		 **********************************************************************************************/
		if (sectionHeader(&file_data[sI], &length))
		{               // Parse the header for section 0
			return CRC_INVALID;
		}

		subSection sections[12];                              // Array of sections to store file data

		for (j = 16; j < length; j += 10)
		{
			uint16_t id = read16(&file_data[sI], j);         // Section Number: 0 - 11
			uint32_t len = read32(&file_data[sI], j + 2);        // Section Length
			uint32_t ind = read32(&file_data[sI], j + 6);        // Section Start Index
			if (id < 12)
			{
				sections[id].init(id, len, ind, file_data);   // Copy data into section classes for later access
			}
			else{                                        // We've hit some sort of error
				(*SCP_Parser::error) << "ERROR: Unexpected section index(" << id << ")! Aborting." << endl;
				return UNEXPECTED_FILE_CONTENTS;
			}
		}

		/**********************************************************************************************
		 *          Section 1
		 **********************************************************************************************/

		if (sections[1].exists()){
			if (sections[1].readHeader(&length)) return CRC_INVALID;
			for (j = 16; j < length;)
			{
				unsigned char tag = sections[1].read_8(&j);
				unsigned short tagLength = sections[1].read_16(&j);
				switch (tag){
				case 0: {
					string name;
					sections[1].readString(&j, &name, tagLength);
				}break;
				case 1: {
					string name;
					sections[1].readString(&j, &name, tagLength);
				}break;
				case 2: {                // Patient ID
					string patientID;
					sections[1].readString(&j, &patientID, tagLength);
				}break;
				case 3: {
					string name;
					sections[1].readString(&j, &name, tagLength);
				}break;

				case 14: {              // Device ID
					unsigned int institution = sections[1].read_16(&j);      // Not sure what this is, set to 0
					unsigned int department = sections[1].read_16(&j);       // Not sure what this is, set to 0
					unsigned int deviceID = sections[1].read_16(&j);         // Not sure what this is, set to 0
					bool deviceType = (1 == sections[1].read_8(&j));         // 0 = cart, 1 = Host
					unsigned int mfgCode = sections[1].read_8(&j);           // 255 = check string at end
					string modelDesc;
					sections[1].readString(&j, &modelDesc, 6);               // ASCII description of device
					unsigned int protocolRev = sections[1].read_8(&j);       // SCP protocol version
					unsigned int protocolCompat = sections[1].read_8(&j);    // Category I or II
					unsigned int languageSupport = sections[1].read_8(&j);   // 0 = ASCII
					unsigned int deviceCapability = sections[1].read_8(&j);  // print, interpret, store, acquire
					unsigned int mainsFreq = sections[1].read_8(&j);         // 50 Hz, 60 Hz, Unspecified
					j += 16;
					unsigned int apRevLength = sections[1].read_8(&j);       // Length of Revision String
					string apRev;
					sections[1].readString(&j, &apRev, apRevLength);         // ASCII Revision of analysis prog.
					unsigned int devSerNoLength = sections[1].read_8(&j);    // Length of Serial Number String
					string devSerNo;
					sections[1].readString(&j, &devSerNo, devSerNoLength);   // ASCII Device Serial Number
					unsigned int devSoftNoLength = sections[1].read_8(&j);   // Length of software string
					string devSoftNo;
					sections[1].readString(&j, &devSoftNo, devSoftNoLength); // ASCII Software Version
					unsigned int devSCPidLength = sections[1].read_8(&j);    // Length of SCP software ID
					string devSCPid;
					sections[1].readString(&j, &devSCPid, devSCPidLength);   // ASCII SCP software ID
					unsigned int mfgStringLength = sections[1].read_8(&j);   // Length of mfg name
					string mfgString;
					sections[1].readString(&j, &mfgString, mfgStringLength); // Manufacturer Name

				}break;
				case 25: {              // Date of Acquisition
					if (tagLength != 4){
						(*SCP_Parser::error) << "Error Parsing Date! Length = " << (int)tagLength << endl;
						return UNEXPECTED_FILE_CONTENTS;
					}
					else{
						unsigned short year = sections[1].read_16(&j);
						unsigned char month = sections[1].read_8(&j);
						unsigned char day = sections[1].read_8(&j);
						if (nullptr != header_info)
						{
							header_info->year = year;
							header_info->month = month;
							header_info->day = day;
						}
					}
				}break;
				case 26: {              // Time of Acquisition
					if (tagLength != 3){
						(*SCP_Parser::error) << "Error Parsing Time! Length = " << tagLength << endl;
						return UNEXPECTED_FILE_CONTENTS;
					}
					else{
						unsigned char hour = sections[1].read_8(&j);
						unsigned char minutes = sections[1].read_8(&j);
						unsigned char seconds = sections[1].read_8(&j);
						if (nullptr != header_info)
						{
							header_info->hour = hour;
							header_info->minutes = minutes;
							header_info->seconds = seconds;
						}
						//Console::WriteLine("File time {0}:{1}:{2}", (int)hour, (int)minutes, (int)seconds);
					}
				}break;
				case 27: {              // High Pass Filter
					if (tagLength != 2){
						(*SCP_Parser::error) << "Error Parsing HP Filter! Length = " << (int)tagLength << endl;
						return UNEXPECTED_FILE_CONTENTS;
					}
					else{
						unsigned short hpFilter = sections[1].read_16(&j);
					}
				}break;
				case 28: {              // Low Pass Filter
					if (tagLength != 2){
						(*SCP_Parser::error) << "Error Parsing LP Filter! Length = " << (int)tagLength << endl;
						return UNEXPECTED_FILE_CONTENTS;
					}
					else{
						unsigned short lpFilter = sections[1].read_16(&j);
					}
				}break;
				case 31:{
					string ecgSequence;
					sections[1].readString(&j, &ecgSequence, tagLength);
					//Console::WriteLine("Sequence: {0}", gcnew String(ecgSequence.c_str()));
				}break;
				case 34:{               // Time Zone
					signed short offset = sections[1].read_16(&j);
					unsigned short index = sections[1].read_16(&j);
					string desc;
					sections[1].readString(&j, &desc, tagLength - 4);
				}break;
				case 255: {             // Terminator - we're done.
					if (tagLength != 0){
						(*SCP_Parser::error) << "Error Parsing Terminator! Length = " << (int)tagLength << endl;
					}
					else{
						j = length;
					}
				}break;
				default: {
					j += tagLength;
				}break;
				}
			}

		}

		/**********************************************************************************************
		 *          Section 2
		 **********************************************************************************************/

		uint32_t **prefixBits = NULL;
		uint32_t **totalBits = NULL;
		uint32_t **switchByte = NULL;
		uint32_t **baseValue = NULL;
		uint32_t **baseCode = NULL;
		uint32_t tableCount = 0;
		uint32_t *codeCount = NULL;
		uint8_t prefix_min_bits = 0xff;
		uint8_t prefix_max_bits = 0;
		if (sections[2].exists())
		{
			if (sections[2].readHeader(&length))
			{
				return CRC_INVALID;
			}
			j = 16;
			tableCount = sections[2].read_16(&j);
			uint32_t l;
			prefixBits = new uint32_t *[tableCount];
			totalBits = new uint32_t *[tableCount];
			switchByte = new uint32_t *[tableCount];
			baseValue = new uint32_t *[tableCount];
			baseCode = new uint32_t *[tableCount];

			codeCount = new uint32_t[tableCount];

			for (l = 0; l < tableCount; l++)
			{
				codeCount[l] = sections[2].read_16(&j);
				prefixBits[l] = new uint32_t[codeCount[l]];
				totalBits[l] = new uint32_t[codeCount[l]];
				switchByte[l] = new uint32_t[codeCount[l]];
				baseValue[l] = new uint32_t[codeCount[l]];
				baseCode[l] = new uint32_t[codeCount[l]];
				uint32_t m;
				for (m = 0; m < codeCount[l]; m++)
				{
					prefixBits[l][m] = sections[2].read_8(&j);
					if (prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
					if (prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
					totalBits[l][m] = sections[2].read_8(&j);
					switchByte[l][m] = sections[2].read_8(&j);
					baseValue[l][m] = sections[2].read_16(&j);
					baseCode[l][m] = sections[2].read_32(&j);

					// Reverse the bit-order for ease of use later
					uint32_t k;
					uint32_t temp = 0;
					for (k = 0; k < prefixBits[l][m]; k++)
					{
						temp = (temp << 1) | ((baseCode[l][m] & (1 << k)) ? 1 : 0);
					}
					baseCode[l][m] = temp;

					// Check if this is the largest code
					if (header_info->file_res < (totalBits[l][m] - prefixBits[l][m]))
					{
						header_info->file_res = (totalBits[l][m] - prefixBits[l][m]);
					}
				}

			}

		}
		else
		{
			header_info->file_res = 16;
		}


		/**********************************************************************************************
		 *          Section 3
		 **********************************************************************************************/

		uint32_t leadCount = 0;

		// Starting sample number
		uint32_t *sampleStart = NULL;

		// Ending sample number
		uint32_t *sampleEnd = NULL;

		uint32_t *leadID = NULL;



		if (sections[3].exists())
		{
			if (sections[3].readHeader(&length)) return CRC_INVALID;
			j = 16;
			leadCount = sections[3].read_8(&j);
			if (nullptr != header_info)
			{
				header_info->leadCount = leadCount;
			}
			uint32_t flags = sections[3].read_8(&j);
			sampleStart = new uint32_t[leadCount];
			sampleEnd = new uint32_t[leadCount];
			leadID = new uint32_t[leadCount];
			for (i = 0; i < leadCount && i < MAX_LEADCOUNT; i++)
			{
				sampleStart[i] = sections[3].read_32(&j);
				sampleEnd[i] = sections[3].read_32(&j);
				leadID[i] = sections[3].read_8(&j);
				if (nullptr != header_info)
					header_info->leadID[i] = leadID[i];

				// If the leadID was zero, start it at the standard location.
				leadID[i] = (leadID[i]) ? leadID[i] : LEAD_ID_BASE + i;
			}
		}

		/**********************************************************************************************
		 *          Section 6
		 **********************************************************************************************/

		uint32_t *leadLength = NULL;
		uint32_t **dataArrays = NULL;
		if (sections[6].exists())
		{
			if (sections[6].readHeader(&length)) return CRC_INVALID;
			j = 16;
			uint32_t ampMult = sections[6].read_16(&j);
			uint32_t samplePeriod = sections[6].read_16(&j);
			if (0 == samplePeriod)
			{
				(*SCP_Parser::error) << "Error: Invalid File Contents. Sample period is set to 0." << std::endl;
				return UNEXPECTED_FILE_CONTENTS;
			}
			if (nullptr != header_info)
			{
				header_info->ampMult = ampMult;
				header_info->samplePeriod = samplePeriod;
			}
			uint32_t encoding = sections[6].read_8(&j);
			uint32_t compression = sections[6].read_8(&j);
			leadLength = new uint32_t[leadCount];
			dataArrays = new uint32_t *[leadCount];
			for (i = 0; i < leadCount; i++)
			{
				leadLength[i] = sections[6].read_16(&j);
				dataArrays[i] = new uint32_t[sampleEnd[i] - sampleStart[i] + 1];
			}
			for (i = 0; i < leadCount; i++)
			{
				unsigned char *rawData = new unsigned char[leadLength[i]];
				uint32_t l;
				for (l = 0; l < leadLength[i]; l++)
				{
					rawData[l] = sections[6].read_8(&j);
				}

				if (!sections[2].exists())
				{
					uint16_t *shortCaster = (uint16_t *)rawData;
					for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++)
					{
						if (l < (leadLength[i] / 2))
						{
							dataArrays[i][l] = shortCaster[l];
							if (dataArrays[i][l] & (1 << (16 - 1)))
							{
								dataArrays[i][l] |= (~0) << (16);
							}
						}
						else dataArrays[i][l] = 0;
					}
				}
				else{
					unsigned char currentBit = 8;
					unsigned char *currentByte = rawData;
					uint32_t currentTable = 0;
					for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++)
					{
						uint32_t k = 0;
						uint32_t prefixValue = 0, bits = 0;
						bool matchFound = 0;
						// Match the next code in the bitstream to our Huffman
						// table from Section 2
						while (!matchFound)
						{
							if (prefix_min_bits == prefix_max_bits)
							{
								if (!bits)
								{
									currentByte = get_bits(&prefixValue, prefix_max_bits,
										currentByte, &currentBit, 0);
									bits += prefix_max_bits;
								}
								else break;
							}
							else
							{
								currentByte = get_bits(&prefixValue, 1,
									currentByte, &currentBit, 0);
								bits += 1;
								if (bits > prefix_max_bits) break;
							}

							if ((bits >= prefix_min_bits) && (bits <= prefix_max_bits))
							{
								for (k = 0; k < codeCount[currentTable]; k++)
								{
									if (!prefixBits[currentTable][k])
									{
										matchFound = 1;
										break;
									}
									else if (prefixBits[currentTable][k] == bits)
									{
										if (baseCode[currentTable][k] == prefixValue)
										{
											matchFound = 1;
											break;
										}
									}
								}
							}
						}

						// SwitchByte == 1 means we decode a value
						if (switchByte)
						{
							currentByte = get_bits(&dataArrays[i][l],
								totalBits[currentTable][k] - prefixBits[currentTable][k],
								currentByte, &currentBit, 1);
						}
						// SwitchByte == 0 means we switch to a different
						// table. (We don't use this on the TZ Medical Monitors).
						else{
							// This is where we would switch tables
						}
					}
				}

				delete[] rawData;
			}



			uint32_t maxSamples = 0, l, totalSamples = 0;

			// If the data is supposed to be interleaved, we want 
			// (l = lead, s = sample)
			// s1, l1
			// s1, l2
			// s1, l3
			// s2, l1
			// s2, l2
			// s2, l3
			// etc
			//
			// So if we are interleaved, we only need one vector with a size equal
			// to the total number of samples. If we are not interleaved, we want
			// "leadCount" vectors, each with enough space to store the data from its
			// respective channel.

			// Use vecotr pointers to actually load the data (so we don't do TONS of map operations)
			std::vector<int32_t> **p_data;
			p_data = new vector<int32_t>*[leadCount];


			for (i = 0; i < leadCount; i++)
			{
				// Remember the largest sample length
				if (sampleEnd[i] > maxSamples) maxSamples = sampleEnd[i];
				totalSamples += sampleEnd[i];

				if (!interleaved)
				{
					preliminary_results[leadID[i]] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
					preliminary_results[leadID[i]]->reserve(sampleEnd[i]);
					p_data[i] = preliminary_results[leadID[i]].get();
				}
			}

			if (interleaved)
			{
				preliminary_results[0] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
				preliminary_results[0]->reserve(maxSamples*leadCount);
				p_data[0] = preliminary_results[0].get();
			}

			int32_t *D1 = new int32_t[leadCount];
			int32_t *D2 = new int32_t[leadCount];


			for (l = 0; l < maxSamples; l++)
			{
				for (i = 0; i < leadCount; i++)
				{
					uint32_t relSample = 0;
					if ((sampleStart[i] - 1) <= l) relSample = 1 + l - sampleStart[i];
					if (((sampleStart[i] - 1) <= l) && ((sampleEnd[i] - 1) >= l))
					{
						int32_t tempData = dataArrays[i][l + sampleStart[i] - 1];
						int32_t outputData = 0;
						if (encoding == 1)
						{
							// First differences
							if (relSample == 0)
							{
								outputData = tempData;
							}
							else{
								outputData = D1[i] + tempData;
							}
							D1[i] = outputData;
						}
						else if (encoding == 2)
						{
							// Second Differences
							if (relSample < 2)
							{
								outputData = tempData;
							}
							else{
								outputData = tempData + (2 * D1[i]) - D2[i];
							}
							D2[i] = D1[i];
							D1[i] = outputData;
						}
						else{
							outputData = tempData;
						}

						if (interleaved)
						{
							p_data[0]->push_back(outputData);
						}
						else
						{
							p_data[i]->push_back(outputData);
						}
					}
					else
					{
						//p_data[i]->push_back(0);
					}
				}
			}

			// Delete leads from the map if they don't contain any data
			auto it = preliminary_results.begin();
			while (it != preliminary_results.end())
			{
				if (it->second->size() <= 1)
				{
					it = preliminary_results.erase(it);
				}
				else
				{
					++it;
				}
			}

			header_info->leadCount = preliminary_results.size();

			// php conversion went here

			delete[] D1;
			delete[] D2;
		}

		// Store the initial values of each lead in the header info.
		for (auto &kv : preliminary_results)
		{
			if (nullptr != header_info && kv.first > 0 && kv.second->size() > 0)
			{
				header_info->initValue[kv.first] = kv.second->at(0);
			}
		}

		if (sections[2].exists())
		{
			uint32_t l;
			for (l = 0; l < tableCount; l++)
			{
				delete[] prefixBits[l];
				delete[] totalBits[l];
				delete[] switchByte[l];
				delete[] baseValue[l];
				delete[] baseCode[l];
			}
			delete[] codeCount;
			delete[] prefixBits;
			delete[] totalBits;
			delete[] switchByte;
			delete[] baseValue;
			delete[] baseCode;
		}

		if (sections[3].exists())
		{
			delete[] sampleStart;
			delete[] sampleEnd;
			delete[] leadID;
		}
		if (sections[6].exists())
		{
			delete[] leadLength;
			uint32_t l;
			for (l = 0; l < leadCount; l++)
			{
				delete[] dataArrays[l];
			}
			delete[] dataArrays;
		}

	}
	else{
		return CRC_INVALID;
	}

	return 0;
}

SCP_Parser::SCP_Parser()
{
	square_wave_functions[FILE_FORMAT_WFDB32] = &wfdb32_write_square_wave;
	square_wave_functions[FILE_FORMAT_WFDB16] = &wfdb16_write_square_wave;
	square_wave_functions[FILE_FORMAT_WFDB212] = &wfdb212_write_square_wave;
	square_wave_functions[FILE_FORMAT_WFDB8] = &wfdb8_write_square_wave;
	square_wave_functions[FILE_FORMAT_ISHNE] = &ishne_write_square_wave;
	square_wave_functions[FILE_FORMAT_HELX] = &helx_write_square_wave;
	square_wave_functions[FILE_FORMAT_WAVE] = &wave_write_square_wave;

	data_functions[FILE_FORMAT_WFDB32] = &wfdb32_write_data;
	data_functions[FILE_FORMAT_WFDB16] = &wfdb16_write_data;
	data_functions[FILE_FORMAT_WFDB212] = &wfdb212_write_data;
	data_functions[FILE_FORMAT_WFDB8] = &wfdb8_write_data;
	data_functions[FILE_FORMAT_ISHNE] = &ishne_write_data;
	data_functions[FILE_FORMAT_HELX] = &helx_write_data;
	data_functions[FILE_FORMAT_WAVE] = &wave_write_data;

	header_functions[FILE_FORMAT_WFDB32] = &wfdb32_write_header;
	header_functions[FILE_FORMAT_WFDB16] = &wfdb16_write_header;
	header_functions[FILE_FORMAT_WFDB212] = &wfdb212_write_header;
	header_functions[FILE_FORMAT_WFDB8] = &wfdb8_write_header;
	header_functions[FILE_FORMAT_ISHNE] = &ishne_write_header;
	header_functions[FILE_FORMAT_HELX] = &helx_write_header;
	header_functions[FILE_FORMAT_WAVE] = &wave_write_header;
}

SCP_Parser::~SCP_Parser()
{
}


uint32_t SCP_Parser::resample_rescale(header_file_info& header_info, 
	std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data, 
	file_format_t format, int32_t new_lsb, int32_t new_sample_rate)
{
	map<uint8_t, std::unique_ptr<std::vector<int32_t> > > copy_data;

	// HELX only supports one sample rate and lsb.
	if (FILE_FORMAT_HELX == format && (new_lsb != NO_LIMIT || new_sample_rate != NO_LIMIT))
	{
		if (new_lsb != NO_LIMIT)
		{
			return HELX_INVALID_LSB;
		}
		else
		{
			return HELX_INVALID_SAMPLE_RATE;
		}
	}

	if (FILE_FORMAT_HELX == format)
	{
		new_sample_rate = HELX_SAMPLE_RATE;
		new_lsb = HELX_TARGET_LSB;
	}

	// Only do the rescaling if these numbers have changed.
	if (!(NO_LIMIT == new_sample_rate && NO_LIMIT == new_lsb))
	{

		// If one of these wasn't specified, just use the current value.
		if (NO_LIMIT == new_sample_rate)
		{
			new_sample_rate = header_info.getSampleFrequency();
		}
		if (NO_LIMIT == new_lsb)
		{
			new_lsb = header_info.ampMult;
		}

		// Calculate how many samples we need.
		uint32_t target_sample_count = std::round((double) data.begin()->second->size() * new_sample_rate / header_info.getSampleFrequency());
		double target_index = 0;

		// Calculate the scaling factor for the data.
		double scalar = (double) header_info.ampMult / (new_lsb);

		// Copy the data elsewhere, then clear "data" so we can 
		// fill it up again with the rescaled data.
		for (auto itr = data.begin(); itr != data.end(); ++itr)
		{
			copy_data[itr->first] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
			for (auto it = itr->second->begin(); it != itr->second->end(); ++it)
			{
				copy_data[itr->first]->push_back(*it);
			}
		}
		data.clear();

		// Add the channels back into "data"
		for (auto kv = copy_data.begin(); kv != copy_data.end(); ++kv)
		{
			data[kv->first] = std::unique_ptr<vector<int32_t> >(new vector<int32_t>());
		}

		// temp variable for storing the adjusted sample
		int32_t datum = 0;

		// Rescale by calculating the two closest values to the time we need in the original data
		// (higher and lower; or later and earlier), then doing a weighted average of them.
		// So if we were rescaling from 10 Hz to 7 Hz, our times would be 0, 0.143, 0.285, etc
		// instead of the original 0. 0.1, 0.2. To calculate the value at 0.143, do a weighted average 
		// of the values at 0.1 and 0.2. The value at 0.1 would get a weight of 0.57, and the value at
		// 0.2 would get a weight of 0.43.
		int high_index, low_index = 0;
		double high_weight, low_weight = 0;
		int high_val, low_val = 0;
		for (auto kv = data.begin(); kv != data.end(); ++kv)
		{
			// Add first value outside of loop to avoid array out of bounds errors
			datum = std::round(scalar * copy_data[kv->first]->at(0));
			kv->second->push_back(datum);

			// Go from 1 to target-1 to avoid index out of bounds errors
			for (uint32_t i = 1; i < target_sample_count-1; ++i)
			{
				target_index = (double) i * header_info.getSampleFrequency() / new_sample_rate;
				high_index = std::round(target_index + 0.5);
				low_index = std::round(target_index - 0.5);
				high_val = copy_data[kv->first]->at(high_index);
				low_val = copy_data[kv->first]->at(low_index);
				low_weight = (1 + (int32_t)target_index - target_index);
				high_weight = (target_index - (int32_t)target_index);
				datum = std::round(scalar * (low_val * low_weight + high_val * high_weight));

				// Interpolate
				// If target index is 0.7, result is 0.3 * val[0] + 0.7 * val[1]
				kv->second->push_back(datum);
			}

			// Add last value outside of loop to avoid array out of bounds errors
			datum = std::round(scalar * copy_data[kv->first]->at(copy_data[kv->first]->size() - 1));
			kv->second->push_back(datum);
		}

		header_info.setSampleFrequency(new_sample_rate);
		header_info.ampMult = new_lsb;
	}

	return 0;
}

uint32_t SCP_Parser::export_file_list(std::string output_filename,
	std::vector<std::string>& filenames, file_format_t format, std::map<uint32_t, bool>& channels,
	bool fill_gaps,
	double max_hours, int32_t* start_time, int32_t length, bool random_start,
	int32_t new_lsb, int32_t new_sample_rate
#ifdef _WIN32
	, System::ComponentModel::BackgroundWorker^ worker
#endif
	)
{
	// Used to get data for a single file
	map<uint8_t, std::unique_ptr<std::vector<int32_t> > > data;

	// A couple things are done differently the first time through the loop.
	bool first_time = true;

	// The amount of time covered by the current file.
	double prev_duration_sec = 0;

	segment_state_t segment_state = NUM_SEGMENT_STATES;
	header_file_info header_info;
	header_file_info prev_header_info;

	// For storing the start time of the sample data.
	header_file_info initial_header_info;
	uint32_t ret_val = 0;
	int32_t high_val = 0;
	int32_t low_val = 0;
	double duration_sec = 0;
	double time_diff = 0;
	double time_diff_beginning = 0;
	uint64_t total_samples = 0;
	double total_time = 0;
	std::vector<uint8_t> keys;
	std::vector<int32_t> current_values;
	double max_diff = 0;
	std::vector<int16_t> checksum;

	// If we got a length and no start time, set start time to zero.
	if (NO_LIMIT == *start_time && NO_LIMIT != length)
	{
		*start_time = 0;
	}

	// Windows filenames...
	std::replace(output_filename.begin(), output_filename.end(), '\\', '/');

	std::string header_filename = get_header_filename(format, output_filename);
	std::string record_name = header_filename.substr(header_filename.find_last_of("/") + 1);
	record_name = record_name.substr(0, record_name.find_first_of("."));

	Console::WriteLine("Record: {0}", gcnew String(record_name.c_str()));

	std::ofstream outfile(output_filename, std::ofstream::out | std::ofstream::binary);
	if (!outfile.is_open())
	{
		// Error opening output file
		Console::WriteLine("Path: {0}", gcnew String(output_filename.c_str()));
		return FILE_IO_ERROR;
	}

	// Any format with a header preceding data in a single file 
	// should be included in this if statement. The header is written
	// at the end since it often needs information (such as total 
	// number of samples written) that is obtained while performing
	// the output. The header function should seek to the beginning
	// of the file. 
	if (FILE_FORMAT_ISHNE == format)
	{
		outfile.seekp(ISHNE_DATA_START);
	}
	else if (FILE_FORMAT_WAVE == format)
	{
		outfile.seekp(WAVE_HEADER_SIZE);
	}

	uint32_t num_files = 0;
	uint8_t stop = 0;
	size_t max_files = 0;

	for (auto &filename : filenames)
	{
		// Process a file
		if (stop)
		{
			break;
		}

		// open the file.
		std::fstream inFile(filename.c_str(), std::ios::in | std::ios::binary);
		if (!inFile.is_open())
		{
			(*SCP_Parser::error) << "File could not be opened" << std::flush;
		}

		while ((inFile.peek() !=  EOF) && (SCP_END_OF_FILE != ret_val))
		{
			// Process a file
			data.clear();
			prev_header_info = header_info;
			ret_val = process_file(data, &inFile, false, &header_info);

			if (!ret_val)
			{
				num_files++;
				keys.clear();

				// Remove undesired channels from the output data to write.
				for (auto &kv : data)
				{
					keys.push_back(kv.first);
				}

				std::sort(keys.begin(), keys.end());

				for (uint32_t i = 0; i < keys.size(); ++i)
				{
					// channels are stored 1 based, not 0 based. If channels is not
					// selected (true), remove the channel corresponding to that key
					// from the data collection. 
					if (!channels[i + 1])
					{
						if (data.count(keys[i]) > 0)
						{
							data.erase(keys[i]);
						}
					}
				}

				// Update the list of keys
				keys.clear();
				for (auto &kv : data)
				{
					keys.push_back(kv.first);
				}
				std::sort(keys.begin(), keys.end());

				if (0 == keys.size())
				{
					return NO_CHANNELS;
				}

				// Optionally resample / rescale data here.
				ret_val = resample_rescale(header_info, data, format, new_lsb, new_sample_rate);
				if (ret_val)
				{
					return ret_val;
				}

				if (first_time)
				{
					first_time = false;
					initial_header_info = header_info;
					prev_header_info = header_info;
					for (uint32_t i = 0; i < keys.size(); ++i)
					{
						// These should be constantly updated by the write function, if
						// the format requires it (wfdb8). 
						current_values.push_back(data.at(keys[i])->at(0));
						if (FILE_FORMAT_WFDB8 == format)
						{
							//checksum.push_back(current_values[i]);
							checksum.push_back(0);

							// Erase first element since we're saving differences from the initial value.
							data.at(keys[i])->erase(data.at(keys[i])->begin());
						}
						else
						{
							checksum.push_back(0);
						}
					}

					// If we have a random start, calculate a random place
					if (filenames.size() == 1)
					{
						size_t fptr = inFile.tellg();
						inFile.seekg(0, ios::end);
						max_files = inFile.tellg() / fptr;
						inFile.seekg(fptr);
					}
					else max_files = filenames.size();
					if (random_start)
					{
						duration_sec = data.begin()->second->size() / ((double)header_info.getSampleFrequency());
						size_t max_time = duration_sec * max_files;

						do
						{
							*start_time = (int32_t)((((double)rand()) / ((double)RAND_MAX))  * ((max_time)-length));
						} while (*start_time < 0 || *start_time >(max_time));
						Console::WriteLine("Trying start time {0} out of {1}", *start_time, round(max_time));
					}
				}

				// Amount of time from start of file until now
				time_diff_beginning = time_difference(header_info, initial_header_info);

				duration_sec = data.begin()->second->size() / ((double)header_info.getSampleFrequency());

				// Check if we're writing only a segment of the data
				if (*start_time != NO_LIMIT)
				{
					// There are 5 possible cases
					segment_state = FILE_DURING_SEGMENT;

					// Does this file occur in its entirety before the start time?
					if ((time_diff_beginning + duration_sec) < *start_time)
					{
						// If not, move on to the next file
						segment_state = FILE_BEFORE_SEGMENT;
						continue;
					}

					// Does this file occur in its entirety after the end time?
					if (time_diff_beginning > *start_time + length && NO_LIMIT != length)
					{
						segment_state = FILE_AFTER_SEGMENT;
						break;
					}

					// Does the start time occur during this file?
					if (time_diff_beginning < *start_time && time_diff_beginning + duration_sec > *start_time)
					{
						// Remove elements that occur in the record before the desired start time
						for (auto it = data.begin(); it != data.end(); ++it)
						{
							auto size = it->second->size();
							auto erase_start = it->second->begin();
							auto erase_offset = (((double)*start_time) - time_diff_beginning) * ((double)header_info.getSampleFrequency());
							auto erase_end = it->second->begin() + erase_offset;
							it->second->erase(erase_start, erase_end);
							//it->second->erase(it->second->begin(), it->second->begin() + std::round((((double)*start_time) - time_diff_beginning) * ((double)header_info.getSampleFrequency())));
						}

						// Update the header info
						//length += std::round(*start_time - time_diff_beginning);
						*start_time = std::round(time_diff_beginning);

						// Update the duration of the file.
						duration_sec = data.begin()->second->size() / ((double)header_info.getSampleFrequency());

						// Update the initial values in the header
						for (int ii = 0; ii < header_info.getSignalCount(); ++ii)
						{
							if (channels.at(ii + 1))
							{
								header_info.initValue[header_info.leadID[ii]] = data.at(header_info.leadID[ii])->at(0);
							}
						}

						segment_state = FILE_CONTAINS_SEGMENT_END;

					}

					// Does the end time occur during this file?
					if (time_diff_beginning > *start_time && max(time_diff_beginning, (double)*start_time) + duration_sec > *start_time + length && NO_LIMIT != length)
					{
						// If it does, remove the unnecessary samples from the end
						for (auto it = data.begin(); it != data.end(); ++it)
						{
							uint32_t the_size = it->second->size();
							// Add one so that the last sample that we want remains in the vector.
							it->second->erase(it->second->begin() + std::round((length + *start_time - time_diff_beginning) * header_info.getSampleFrequency()) + 1, it->second->end());
							the_size = it->second->size();
						}
						// Update the duration of the file.
						duration_sec = data.begin()->second->size() / ((double)header_info.getSampleFrequency());

						if (*start_time == time_difference(header_info, initial_header_info))
						{
							segment_state = FILE_CONTAINS_ENTIRE_SEGMENT;
						}
						else
						{
							segment_state = FILE_CONTAINS_SEGMENT_END;
						}
					}

					// Does the file occur in its entirety during the desired time? 
					// If yes, leave it alone.
				}

				if ((NO_LIMIT != max_hours)
					&& (time_difference(header_info, initial_header_info) + duration_sec) > (max_hours * SEC_PER_HOUR))
				{
					// Adding this file to the record would pass the maximum amount of time allowed.
					break;
				}

				// fill a gap if fill_gaps has been specified.
				time_diff = time_difference(header_info, prev_header_info); // <= time difference between file beginnings
				time_diff -= prev_duration_sec; // <= time elapsed during first file.

				// Right now we're losing a second here and there by ignoring time_diff when it's less than 2,
				// but this is preferable to filling up tiny gaps in the record with square waves.
				if (fill_gaps && (time_diff > 2 || time_diff < -45) && header_info.getSampleFrequency() != 0
					// First time check
					&& (time_difference(header_info, initial_header_info) - *start_time) > 1)
				{

					// Calculate the values we want when writing square waves to the file.
					high_val = (int32_t)header_info.getGain();
					low_val = (int32_t)-1 * header_info.getGain();

					if (random_start)
					{
						Console::WriteLine(&filename);
						Console::WriteLine("time_diff = {0} seconds. Duration = {1} seconds.", time_diff, prev_duration_sec);
						// If we're randomly picking a segment to export, we don't want one with
						// a gap, so return an error and let the caller try again
						return GAP_IN_RANDOM_RECORD;
					}


					// If we've jumped more than three months forward or 45 seconds back, we probably got a clock reset,
					// so signify that with just one file's worth of data.
					else if (time_diff > SEC_IN_3_MONTHS || time_diff < -45)
					{
						if (FILE_DURING_SEGMENT == segment_state || FILE_CONTAINS_ENTIRE_SEGMENT || FILE_CONTAINS_SEGMENT_START || FILE_CONTAINS_SEGMENT_END)
						{
							// We don't want to include a large jump when exporting a time segment
							break;
						}


						ret_val = write_square_wave(format, outfile, data.size(), header_info.getSampleFrequency(),
							AVG_FILE_LENGTH, current_values, low_val, high_val, checksum);

						total_time += AVG_FILE_LENGTH;
						total_samples += AVG_FILE_LENGTH * header_info.getSampleFrequency();
					}
					// If the jump was smaller than that, simply write square wave data over the missing space.
					else
					{
						// We only want to write either up to the end of the desired time, or 
						// the time difference, which ever is smaller.
						if (NO_LIMIT != *start_time && NO_LIMIT != length)
						{
							time_diff = min(time_diff, *start_time + length - total_time);
						}

						ret_val = write_square_wave(format, outfile, data.size(), header_info.getSampleFrequency(),
							time_diff, current_values, low_val, high_val, checksum);

						total_time += time_diff;
						total_samples += time_diff * header_info.getSampleFrequency();
					}

					//ret_val = 0;
					if (ret_val)
					{
						// IO error occurred.
						outfile.close();
						remove(output_filename.c_str());
						return FILE_IO_ERROR;
					}
				}

				if (0 && FILE_FORMAT_WFDB8 == format)
				{
					// Store the current values of the channels for the
					// square wave function. 

					current_values.clear();
					keys.clear();
					for (auto &kv : data)
					{
						keys.push_back(kv.first);
					}
					std::sort(keys.begin(), keys.end());
					for (auto &k : keys)
					{
						current_values.push_back(data[k]->at(data[k]->size() - 1));
					}
				}

				// Store the time elapsed during the last file processed.
				prev_duration_sec = duration_sec;
			
				Console::Write("#");
				if (!(num_files % 80)) Console::WriteLine();

				ret_val = write_data(format, data, channels, outfile, record_name, current_values, header_info, checksum);
				if (ret_val)
				{
					// IO error occurred.
					outfile.close();
					remove(output_filename.c_str());
					return ret_val;
				}

				size_t size = data.begin()->second->size();
				for (auto itr = data.begin(); itr != data.end(); ++itr)
				{
					if (itr->second->size() != size)
					{
						Console::WriteLine("Error!");
					}
				}
				total_time += duration_sec;
				total_samples += data.begin()->second->size();
			}
			else if(SCP_END_OF_FILE != ret_val)
			{
				// Error occurred. skip this file. 
				// Its missing data will be covered up with a square wave in the output
				// file on the next pass through the loop.
				header_info = prev_header_info;
				(*SCP_Parser::error) << "Error processing " << filename << ". skipping." << std::endl;
			}

#ifdef _WIN32
			if (nullptr != worker)
			{
				uint32_t percent_complete = (100 * num_files) / max_files;
				if (percent_complete > 100)
				{
					percent_complete = 100;
				}
				worker->ReportProgress(percent_complete);
				if (worker->CancellationPending)
				{
					// Delete the data file.
					outfile.close();
					remove(output_filename.c_str());
					return CANCELLED;
				}
			}
#endif
		}
	}

	// Use total_time + 2 since we can lose a second due to rounding both at the beginning and end of the desired segment.
	if ((total_time + 2) < length && (NO_LIMIT != length) && !((NO_LIMIT != max_hours) && (abs(total_time - (max_hours * SEC_PER_HOUR))) <= 10))
	{
		// If the start time was too close to the end of the file when
		// picking a random start time. Could happen since we don't know
		// for sure how long the record is.
		return GAP_IN_RANDOM_RECORD;
	}

	if (filenames.size() > 0 && 0 != total_samples)
	{
		initial_header_info.increase_start_time(*start_time);
		std::ofstream header_file;
		if (header_filename != output_filename)
		{
			header_file.open(header_filename);
			if (!header_file.is_open())
			{
				// IO error occurred.
				remove(output_filename.c_str());
				remove(header_filename.c_str());
				return FILE_IO_ERROR;
			}
			ret_val = write_header(format, total_samples, channels, header_file, record_name, initial_header_info, checksum);
		}
		else
		{
			ret_val = write_header(format, total_samples, channels, outfile, record_name, initial_header_info, checksum);
		}

		if (ret_val)
		{
			return ret_val;
		}
		outfile.close();
		header_file.close();
	}
	else
	{
		return NO_SCP_FILES_FOUND;
	}
	
	if (num_files % 80) Console::WriteLine();

	return 0;
}

uint32_t SCP_Parser::write_square_wave(file_format_t format, std::ofstream& outfile,
	uint32_t num_channels, double frequency_hz, double duration_seconds,
	std::vector<int32_t>& current_val, int32_t low_val, int32_t high_val, std::vector<int16_t>& checksum)
{
	return (*square_wave_functions[format]) (outfile, num_channels, frequency_hz, duration_seconds, current_val, low_val, high_val, checksum);
}

uint32_t SCP_Parser::write_data(file_format_t format,
	std::map<uint8_t, std::unique_ptr<std::vector<int32_t> > >& data,
	std::map<uint32_t, bool> channels, std::ofstream& outfile,
	std::string record_name, std::vector<int32_t>& current_val, header_file_info& header_info, std::vector<int16_t>& checksum)
{
	return (*data_functions[format])(data, channels, outfile, record_name, current_val, header_info, checksum);
}

uint32_t SCP_Parser::write_header(file_format_t format, uint64_t total_samples,
	std::map<uint32_t, bool> channels, std::ofstream& output, std::string record_name,
	header_file_info& header_info, std::vector<int16_t>& checksum)
{
	return (*header_functions[format])(total_samples, channels, output, record_name, header_info, checksum);
}

file_format_t SCP_Parser::string_to_format(std::string format)
{
	// remove spaces
	format.erase(remove_if(format.begin(), format.end(), ::isspace), format.end());

	// convert string to lowercase
	std::transform(format.begin(), format.end(), format.begin(), ::tolower);

	// check which one it should be
	if ("wfdb8" == format)
	{
		return FILE_FORMAT_WFDB8;
	}
	else if ("wfdb16" == format)
	{
		return FILE_FORMAT_WFDB16;
	}
	else if ("wfdb32" == format)
	{
		return FILE_FORMAT_WFDB32;
	}
	else if ("wfdb212" == format)
	{
		return FILE_FORMAT_WFDB212;
	}
	else if ("ishne" == format)
	{
		return FILE_FORMAT_ISHNE;
	}
	else if ("helx" == format)
	{
		return FILE_FORMAT_HELX;
	}
	else if ("wave" == format || "wav" == format)
	{
		return FILE_FORMAT_WAVE;
	}

	return NUM_FORMATS;
}

std::string SCP_Parser::get_header_filename(file_format_t format, std::string data_filename)
{
	std::string header_filename;
	switch (format)
	{
	case FILE_FORMAT_WFDB32:
	case FILE_FORMAT_WFDB16:
	case FILE_FORMAT_WFDB8:
	case FILE_FORMAT_WFDB212:
	case FILE_FORMAT_HELX:
		header_filename = data_filename.substr(0, data_filename.find_last_of("."));
		if (0 == header_filename.size())
		{
			header_filename = data_filename;
		}
		header_filename.append(WFDB_HEADER_EXTENSION);
		break;

	case FILE_FORMAT_ISHNE:
	case FILE_FORMAT_WAVE:
		header_filename = data_filename;
		break;

	default:
		break;
	}

	return header_filename;
}
