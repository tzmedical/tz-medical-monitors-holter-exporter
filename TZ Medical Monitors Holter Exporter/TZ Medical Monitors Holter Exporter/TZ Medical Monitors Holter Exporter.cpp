/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

/*
 * Program Structure Overview.
 *
 * The purpose of this program is to convert SCP files into one of a number of formats,
 * such as WFDB or ISHNE.
 * The program starts at wWinMain (necessary for a Windows GUI application), where it
 * checks if it has any command line arguments. If it does, it parses them in the wWinMain
 * function and then sends them on to export_data, which creates the list of files to
 * parse, then passes them on to the SCP_Parser object. If it does not, it launches
 * a GUI for the user to interact with.
 *
 * This SCP_Parser object contains all the functionality for reading scp files and
 * writing wfdb8, wfdb16, wfdb212, ishne, and helx file formats. This class is written
 * in portable C++ and has been tested on both GCC and MSVCPP. In this converter
 * program, the functionality is accessed by calling the export_file_list function,
 * which accepts a list of files and some formatting data, then exports all the
 * files, in order, to a designated output file. The object does have many other
 * functions for writing data and parsing files more granularly, however.
 *
 * In order to add an additional format to the SCP_Parser class, [format]_write_header, [format]_write_square_wave,
 * and [format]_write_data functions must be implemented (using the appropriate typedef'd signature:
 * search SCP_Parser::*write_square_wave_function for an example), and the format must be added to the
 * file_format_t enum in SCP_Parser.h. Then, in the object's constructor, the functions must be registered
 * with the object in the same way as the already existing formats (this will be easy when looking
 * at the constructor code). The string_to_format and get_header_filename functions should also
 * be updated to accomodate the new format. For the GUI, only the format_options ComboBox need be updated.
 */

#include "Frame.h"
#include "FrameHELX.h"
#include "TZ_Medical_Monitors_Holter_Exporter.h"

// Include the Shell32 library.
#pragma comment( lib, "Shell32.lib" )
#pragma comment( lib, "Shlwapi.lib" )

using namespace System;
using namespace System::Windows::Forms;
using namespace System::IO;
using namespace msclr::interop;
using namespace System::ComponentModel;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

enum  optionIndex { UNKNOWN = 0, HELP, FORMAT, CHANNELS, OUTPUT_FILE, DRIVE, INPUT_FOLDER };

[STAThread]
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	int argc = 0;
	LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);

	// Seed random number generator using milliseconds.
	std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
	srand(std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count());

	// Use the parent process's console for output.
	AttachConsole(ATTACH_PARENT_PROCESS);
	//AllocConsole(); // Spawns a separate console, useful for debugging.
	Console::WriteLine();

	char* temp = NULL;
	array<String^>^ channel_list;

	// keep track of if we got enough required arguments. 
	int num_arguments = 0;

	String^ folder = nullptr;
	String^ drive = nullptr;
	String^ format = nullptr;
	String^ output_file = nullptr;
	double max_hours = NO_LIMIT;

	// What should the new LSB (least significant byte) value be?
	// What is the smallest voltage that will change the output by one bit?
	// This can be used to fit data into more compressed formats.
	// NO_LIMIT -> no change
	int32_t new_lsb = NO_LIMIT;

	// What should the sample rate be for the output data?
	// NO_LIMIT -> no change
	int32_t new_sample_rate = NO_LIMIT;

	// Where in the record should exporting data start?
	// NO_LIMIT -> the beginning of the record.
	double start_time = NO_LIMIT;

	// How many minutes of data should be exported?
	// NO_LIMIT -> no limit
	double length = NO_LIMIT;

	// Are we supposed to pick a random start time?
	bool random_start = false;

	// Should gaps in the record be filled up with square wave data?
	bool fill_gaps = true;

	// Should we run using a GUI?
	bool gui = false;

	Dictionary<int, Boolean>^ channels = nullptr;

	static struct option long_options[] =
	{
		{ _T("gui"), ARG_NONE, 0, _T('g') },
		{ _T("no-fill"), ARG_NONE, 0, _T('n') },
		{ _T("format"), ARG_REQ, 0, _T('f') },
		{ _T("output"), ARG_REQ, 0, _T('o') },
		{ _T("channels"), ARG_REQ, 0, _T('c') },
		{ _T("input-folder"), ARG_REQ, 0, _T('i') },
		{ _T("drive"), ARG_REQ, 0, _T('d') },
		{ _T("max-hours"), ARG_REQ, 0, _T('m') },
		{ _T("length"), ARG_REQ, 0, _T('l') },
		{ _T("start-time"), ARG_REQ, 0, _T('s') },
		{ _T("random-start"), ARG_NONE, 0, _T('r') },
		{ _T("sample-rate"), ARG_NONE, 0, _T('a') },
		{ _T("lsb"), ARG_NONE, 0, _T('b') },
		{ _T("help"), ARG_NONE, 0, _T('h') },
		{ ARG_NULL, ARG_NULL, ARG_NULL, ARG_NULL }
	};

	int option_index = 0;
	int c = 0;

	if (argc == 1)
	{
		gui = true;
	}

	while ((c = getopt_long(argc, argv, _T("gnf:o:c:i:d:m:l:s:rabh"), long_options, &option_index)) != -1)
	{
		switch (c)
		{
		// Process the output file case.
		case _T('o'):
			output_file = gcnew String(optarg);
			if (0 == output_file->Length)
			{
				print_error("Output filename must be specified.");
				return 1;
			}
			++num_arguments;
			break;

		// Process the input file case.
		case _T('i'):
			++num_arguments;
			if (nullptr != drive)
			{
				print_error("Only one of input-folder and drive may be specified.");
				return 1;
			}
			if (!dirExists(gcnew String(optarg)))
			{
				print_error("Input folder cannot be found.");
				return -1;
			}

			folder = gcnew String(optarg);
			break;

		// Process the help case.
		case _T('h'):
			print_help();
			return -1;
			break;

		// Process the no-fill case
		case _T('n'):
			fill_gaps = false;
			break;

		// Process the drive case.
		case _T('d'):
			if (nullptr != folder)
			{
				print_error("Only one of input_folder and drive may be specified.");
				return 1;
			}
			if (!dirExists(Path::Combine(String::Concat(gcnew String(optarg), ":"), TOP_FOLDER_NAME)))
			{
				print_error("The specified drive does not contain a folder named ecgs. Use the --input-folder option to specify a different folder name.");
				return -1;
			}
			drive = gcnew String(optarg);
			++num_arguments;
			break;

		// Process the format case.
		case _T('f'):
			// Check if the format is valid
			temp = wchar_to_lower_c_str(optarg);
			format = gcnew String(temp);
			delete temp;

			++num_arguments;
			break;

		// Process the channels case.
		case _T('c'):
			// Any channel that is present in the argument is valid, so add those
			// to the dictionary as true, and everything else as false.
			channels = gcnew Dictionary<int, Boolean>();
			channel_list = ((gcnew String(optarg))->Split(','));
			temp = new char[1024];

			// <= since channels are 1 based.
			for (int i = 1; i <= NUM_CHANNELS; ++i)
			{
				channels[i] = (Array::IndexOf(channel_list, System::Convert::ToString(i)) > -1);
			}

			break;

		// Process max hours case
		case _T('m'):
			try
			{
				max_hours = max(0, std::stod(optarg));
			}
			catch (std::invalid_argument)
			{
				print_usage();
				return 1;
			}
			catch (std::out_of_range)
			{
				max_hours = INT32_MAX;
				Console::WriteLine("Warning: max-hours value out of range. Truncating to {}.", INT32_MAX);
			}
			break;

		// Process the start time case
		case _T('s'):
			if (random_start)
			{
				print_error("Only one of start-time and random can be specified.");
				print_usage();
				return 1;
			}
			try
			{
				start_time = max(0, std::stod(optarg));
			}
			catch (std::invalid_argument)
			{
				print_usage();
				return 1;
			}

			break;
		// Process the random start case
		case _T('r'):
			if (start_time >= 0)
			{
				print_error("Only one of start-time and random can be specified.");
				print_usage();
				return 1;
			}
			random_start = true;
			break;

		// Process the GUI case
		case _T('g'):
			gui = true;
			break;

		// Process the length case
		case _T('l'):
			try
			{
				length = std::stod(optarg);
			}
			catch (std::invalid_argument)
			{
				print_usage();
				return 1;
			}

			break;

		// Process the sample rate case
		case _T('a'):
			try
			{
				new_sample_rate = std::stod(optarg);
			}
			catch (std::invalid_argument)
			{
				print_usage();
				return 1;
			}
			break;

		// Process the lsb case
		case _T('b'):
			try
			{
				new_lsb = std::stod(optarg);
			}
			catch (std::invalid_argument)
			{
				print_usage();
				return 1;
			}
			break;

		// If there was a missing argument.
		case _T(':'):
			print_usage();
			return 1;
			break;

		// Some other error
		case _T('?'):
			print_error(String::Format("Invalid argument at location {0}", option_index + num_arguments));
			print_usage();
			return 1;
			break;

		// Shouldn't happen.
		default:
			print_usage();
			return 1;
			break;
		}
	}

	if (gui)
	{
		// Run the gui
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false);

        if (format && "helx" == format)
        {
            Application::Run(gcnew TZMedicalMonitorsHolterExporter::FrameHELX());
        }
        else
        {
            Application::Run(gcnew TZMedicalMonitorsHolterExporter::Frame());
        }

		return 0;
	}
	else
	{
		Console::WriteLine("Exporting...");

		if (num_arguments < REQUIRED_ARGS)
		{
			print_error("Not enough arguments were provided.");
			print_usage();
			return 1;
		}

		// If channels hasn't already been set (meaning it wasn't specified in the
		// argument list), set all channels to true.
		if (nullptr == channels)
		{
			channels = gcnew Dictionary<int, Boolean>();
			for (int i = 1; i <= NUM_CHANNELS; ++i)
			{
				channels[i] = true;
			}
		}

		// Either folder or drive can be specified, figure out which one was here.
		if (nullptr == folder)
		{
			if (!drive->EndsWith(":"))
			{
				folder = System::IO::Path::Combine(String::Concat(drive, ":"), TOP_FOLDER_NAME);
			}
			else
			{
				folder = System::IO::Path::Combine(drive, TOP_FOLDER_NAME);
			}
		}

		// Export the data
		if (NO_LIMIT != length)
		{
			// Convert from minutes to seconds
			length *= SEC_PER_MINUTE;
		}
		if (NO_LIMIT != start_time)
		{
			// Convert from minutes to seconds
			start_time *= SEC_PER_MINUTE;
		}

		return export_data(folder, output_file, format, channels, max_hours, std::round(start_time), std::round(length), fill_gaps, random_start, new_lsb, new_sample_rate, nullptr);
	}

	return 0;
}

int export_data(String^ folder, String^ output_filename, String^ format, Dictionary<int, Boolean>^ channels, double max_hours,
	int32_t start_time, int32_t length, bool fill_gaps, bool random_start, int32_t new_lsb, int32_t new_sample_rate, BackgroundWorker^ worker)
{
	std::string input_file_string = marshal_as<std::string>(folder);
	SCP_Parser parser;
	header_file_info header_info;
	List<String^>^ files;
	uint32_t ret_val = 0;
	uint32_t attempt_counter = 0;
	file_format_t file_format = parser.string_to_format(marshal_as<std::string>(format));
	if (NUM_FORMATS == file_format)
	{
		print_error("The output format specified is invalid.");
		return INVALID_FORMAT;
	}

	// Make sure we could open the output folder
	// Create a list of all the files in the drive with the extension specified by INPUT_FILE_FORMAT.
	try
	{
		files = gcnew List<String^>(Directory::EnumerateFiles(folder, INPUT_FILE_FORMAT, SearchOption::AllDirectories));

		if (0 == files->Count)
		{
			files = gcnew List<String^>(Directory::EnumerateFiles(folder, ONE_ECG_FILE_FORMAT, SearchOption::AllDirectories));
		}
	}
	catch (System::IO::DirectoryNotFoundException^ e)
	{
		print_error("The input folder could not be found.");
		exit(-1);
	}

	// The current implementation of EnumerateFiles returns the list already sorted, so this is just an O(n) guard 
	// in case the implementation changes. This line is instantaneous, the real heavy lifting is the parsing and IO
	// later on. 
	files->Sort();

	// Convert the filenames list to a C++ vector
	std::vector<std::string> filenames_cpp;
	for (int i = 0; i < files->Count; ++i)
	{
		filenames_cpp.push_back(marshal_as<std::string>(files[i]));
	}

	// convert the channels dictionary to a C++ map
	std::map<uint32_t, bool> channels_cpp;
	for each(auto &pair in channels)
	{
		channels_cpp[pair.Key] = pair.Value;
	}

	// Export the data. If random-start was selected, try to export a random segment of the data without gaps.
	// If a GAP_IN_RANDOM_RECORD was returned, try again with a different start time. If there are too many
	// failed attempts, give up.
	ret_val = GAP_IN_RANDOM_RECORD;
	while ((GAP_IN_RANDOM_RECORD == ret_val) && (attempt_counter < MAX_RANDOM_ATTEMPTS))
	{
		if (random_start)
		{
			++attempt_counter;
		}
		ret_val = parser.export_file_list(marshal_as<std::string>(output_filename), filenames_cpp,
			file_format, channels_cpp,
			fill_gaps, max_hours, &start_time, length, random_start, new_lsb, new_sample_rate, worker);
	}

	switch (ret_val)
	{
	case FILE_NOT_FOUND:
		print_error("The specified input folder could not be found.");
		break;
	case CRC_INVALID:
		print_error("A file contained an incorrect CRC value.");
		break;
	case INVALID_FORMAT:
		print_error("The output format specified is invalid.");
		break;
	case FILE_IO_ERROR:
		print_error("An IO error occured when writing to the output file.");
		break;
	case CANCELLED:
		print_error("Cancelled.");
		break;
	case UNEXPECTED_FILE_CONTENTS:
		print_error("A file's contents did not match the expected format.");
		break;
	case NO_SCP_FILES_FOUND:
		print_error("The requested folder did not contain any scp files.");
		break;
	case WFDB212_TOO_MANY_CHANNELS:
		print_error("Please ensure that exactly 2 channels are selected for wfdb212 format.");
		break;
	case GAP_IN_RANDOM_RECORD:
		print_error("Could not find a suitable (free of gaps) segment in the record to export.");
		break;
	case ARITHMETIC_SHIFTS_NOT_SUPPORTED:
		print_error("Arithmetic Shifting not supported by this compiler. Format wfdb212 not supported.");
		break;
	case HELX_INVALID_SAMPLE_RATE:
		print_error("The helx format requires a sample rate of exactly 180 Hz. Please ensure that no sample rate is specified on the command line.");
		break;
	case HELX_INVALID_LSB:
		print_error("The helx format requires a sample rate of exactly 3125 nV. Please ensure that no lsb is specified on the command line.");
		break;
	case NO_CHANNELS:
		print_error("The input data did not contain any of the specified channels.");
		break;
	case INVALID_LENGTH:
		print_error("A file had an invalid length.");
		break;
	default:
		Console::WriteLine("Export complete.");
		if (random_start)
		{
			if (NO_LIMIT == length)
			{
				Console::WriteLine("Exported a segment of data from start time {0} seconds to the end of the record.", start_time);
			}
			else
			{
				Console::WriteLine("Exported a segment of data from start time {0} seconds to end time {1} seconds.", start_time, start_time + length);
			}
		}
		Console::Write("Press Enter for Prompt.");
		break;
	}
	return ret_val;
}

bool dirExists(String^ filename)
{
	pin_ptr<const wchar_t> w_name = PtrToStringChars(filename);
	return PathFileExists(w_name);
}

BOOL contains(const char * the_array[], int the_array_length, char * val)
{
	BOOL equal;
	BOOL result = FALSE;

	for (int i = 0; i < the_array_length && !result; ++i)
	{
		equal = TRUE;
		for (int j = 0; equal && the_array[i][j] && val[j]; ++j)
		{
			if ((the_array[i][j]) != val[j])
			{
				equal = FALSE;
			}

			// in case one ends before the other.
			if (the_array[i][j + 1] != val[j + 1])
			{
				equal = FALSE;
			}
		}

		if (equal)
		{
			result = TRUE;
		}
	}

	return result;
}

char* wchar_to_lower_c_str(wchar_t* orig)
{
	int size = (wcslen(orig) + 1) * 2;
	size_t converted_size = 0;

	char* MBBuffer = new char[size];
	wcstombs_s(&converted_size, MBBuffer, size, orig, size);

	for (int i = 0; i < size; ++i)
	{
		MBBuffer[i] = tolower(MBBuffer[i]);
	}

	return MBBuffer;
}

void print_usage()
{
	print_one_line_usage();
	Console::WriteLine("Use --help for more information.");
}

void print_one_line_usage()
{
	Console::WriteLine("Usage: TZ Medical Monitors Holter Exporter [--gui] [-c 1,2,3] -f format [-s start-time] [--random-start]\n\t[-a sample-rate] [-b lsb] [-l length] -o output_file [-d drive] | [-i input_folder]");
}

void print_help()
{
	Console::Write("TZ Medical Monitors Holter Exporter (version ");
	Console::Write(VERSION_NUMBER);
	Console::WriteLine(")\n");

	print_one_line_usage();
	Console::WriteLine("\nRun with no arguments for a GUI.");
	Console::WriteLine(" --help  \tPrint usage and exit.");
	Console::WriteLine(" --format \tThe format to use for the output file.\n\t\tOptions are: wfdb8, wfdb16, wfdb32, wfdb212, ishne, helx.\n\t\tNote that the helx format requires one file per channel, so the program \n\t\tshould be run multiple times, once for each channel (see --channels).");
	Console::WriteLine(" --channels \tSelect which channels to export.\n\t\tShould be a comma separated list of numbers of the form 1,2,3.\n\t\tNonexistant channels are ignored. Optional, default is all channels.");
	Console::WriteLine(" --output \tThe name of the file in which to store the exported data. For WFDB formats,\n\t\tthis should be a .dat file, and a .hea file with the same filename will also be created.");
	Console::WriteLine(" --drive \tThe drive letter of the sd card containing the data (ex: F).\n\t\tEither drive or input-folder must be specified.");
	Console::WriteLine(" --input-folder\tThe location of the ecgs folder containing the data to export. \n\t\tEither drive or input-folder must be specified.");
	Console::WriteLine(" --start-time\tHow far from the beginning of the record (in minutes) should the exporting begin?\n\t\tAccepts integer and floating point values. Optional, default is the beginning of the record.");
	Console::WriteLine(" --random-start\tBegins the export at a random time in the record.\n\t\tUseful for obtaining a random, representative sample of the data.\n\t\tOptional. Cannot be used if start-time is specified.\n\t\tTries to select a segment of the data without breaks.");
	Console::WriteLine(" --length\tThe length (in minutes) of the segment of data to export.\n\t\tAccepts integer and floating point values. Optional, default is no limit.");
	Console::WriteLine(" --max-hours\tThe maximum number of hours worth of data to export.\n\t\tAccepts integer and floating point values. Optional, default is no limit.");
	Console::WriteLine(" --sample-rate\tThe new sample rate for the output data (in Hz).\n\t\tOptional, default is the same sample rate as the input data. Not compatible with helx format.");
	Console::WriteLine(" --lsb\t\tThe new least significant byte for the output data (in nV).\n\t\tOptional, default is the same lsb as the input data. Not compatible with helx format.\n\t\tLarger values result in less overflow when using compact formats.");
	Console::WriteLine(" --gui\t\tLaunch a GUI for the tool.");
}

void print_error(String^ error_msg)
{
	Console::Write("ERROR: ");
	Console::WriteLine(error_msg);
}
