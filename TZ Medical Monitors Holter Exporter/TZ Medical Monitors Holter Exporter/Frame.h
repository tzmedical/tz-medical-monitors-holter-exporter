/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*
*
*
*****************************************************************************/

#ifndef FRAME_H
#define FRAME_H

#include "TZ_Medical_Monitors_Holter_Exporter.h"
#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include "ProgressPopup.h"
#include "Shlwapi.h"
#include "scp_parser.h"

namespace TZMedicalMonitorsHolterExporter {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace Windows::Forms;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class Frame : public System::Windows::Forms::Form
	{
	public:
		Frame(void)
		{
			InitializeComponent();
			this->Text = String::Concat("TZ Medical Monitors Holter Exporter v", VERSION_NUMBER);

			// Put a valid value in the drive box and the format box on startup.
			Refresh_Click(nullptr, nullptr);
			if (format_options->Items->Count > 0)
			{
				format_options->SelectedItem = format_options->Items[0];
			}
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Frame()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:
	private: System::Windows::Forms::RichTextBox^  Instructions;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::ComboBox^  removable_drives;


	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::CheckBox^  channel_3;

	private: System::Windows::Forms::CheckBox^  channel_2;

	private: System::Windows::Forms::CheckBox^  channel_1;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ComboBox^  format_options;

	private: System::Windows::Forms::Button^  Export;

	private: System::Windows::Forms::Button^  Exit;
	private: System::Windows::Forms::Button^  Refresh;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ToolTip^  export_length_tooltip;
	private: System::Windows::Forms::TextBox^  max_export_length;
	private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Frame::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->Instructions = (gcnew System::Windows::Forms::RichTextBox());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->Refresh = (gcnew System::Windows::Forms::Button());
			this->removable_drives = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->max_export_length = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->channel_3 = (gcnew System::Windows::Forms::CheckBox());
			this->channel_2 = (gcnew System::Windows::Forms::CheckBox());
			this->channel_1 = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->format_options = (gcnew System::Windows::Forms::ComboBox());
			this->Export = (gcnew System::Windows::Forms::Button());
			this->Exit = (gcnew System::Windows::Forms::Button());
			this->export_length_tooltip = (gcnew System::Windows::Forms::ToolTip(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(17, 18);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(4);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(100, 100);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// Instructions
			// 
			this->Instructions->Font = (gcnew System::Drawing::Font(L"Corbel", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Instructions->Location = System::Drawing::Point(125, 18);
			this->Instructions->Margin = System::Windows::Forms::Padding(4);
			this->Instructions->Name = L"Instructions";
			this->Instructions->ReadOnly = true;
			this->Instructions->Size = System::Drawing::Size(397, 100);
			this->Instructions->TabIndex = 1;
			this->Instructions->Text = L"Select the drive containing the ecg data and the format, then click export. Use t"
				L"he channel checkboxes to select which channels should be included in the output."
				L"";
			this->Instructions->TextChanged += gcnew System::EventHandler(this, &Frame::Instructions_TextChanged);
			// 
			// panel1
			// 
			this->panel1->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->panel1->Controls->Add(this->Refresh);
			this->panel1->Controls->Add(this->removable_drives);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Location = System::Drawing::Point(17, 126);
			this->panel1->Margin = System::Windows::Forms::Padding(4);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(505, 66);
			this->panel1->TabIndex = 2;
			// 
			// Refresh
			// 
			this->Refresh->Location = System::Drawing::Point(236, 10);
			this->Refresh->Name = L"Refresh";
			this->Refresh->Size = System::Drawing::Size(99, 26);
			this->Refresh->TabIndex = 2;
			this->Refresh->Text = L"Refresh";
			this->Refresh->UseVisualStyleBackColor = true;
			this->Refresh->Click += gcnew System::EventHandler(this, &Frame::Refresh_Click);
			// 
			// removable_drives
			// 
			this->removable_drives->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->removable_drives->FormattingEnabled = true;
			this->removable_drives->ItemHeight = 18;
			this->removable_drives->Location = System::Drawing::Point(69, 10);
			this->removable_drives->Margin = System::Windows::Forms::Padding(4);
			this->removable_drives->Name = L"removable_drives";
			this->removable_drives->Size = System::Drawing::Size(160, 26);
			this->removable_drives->TabIndex = 1;
			this->removable_drives->SelectedIndexChanged += gcnew System::EventHandler(this, &Frame::comboBox1_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Corbel", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(17, 10);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(44, 18);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Drive:";
			// 
			// panel2
			// 
			this->panel2->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->panel2->Controls->Add(this->max_export_length);
			this->panel2->Controls->Add(this->label3);
			this->panel2->Controls->Add(this->channel_3);
			this->panel2->Controls->Add(this->channel_2);
			this->panel2->Controls->Add(this->channel_1);
			this->panel2->Controls->Add(this->label2);
			this->panel2->Controls->Add(this->format_options);
			this->panel2->Location = System::Drawing::Point(17, 200);
			this->panel2->Margin = System::Windows::Forms::Padding(4);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(505, 97);
			this->panel2->TabIndex = 3;
			// 
			// max_export_length
			// 
			this->max_export_length->Location = System::Drawing::Point(189, 31);
			this->max_export_length->Name = L"max_export_length";
			this->max_export_length->Size = System::Drawing::Size(100, 26);
			this->max_export_length->TabIndex = 6;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(186, 8);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(208, 18);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Maximum Export Length (hours)";
			this->export_length_tooltip->SetToolTip(this->label3, L"How many hours of data should be exported\? (leave blank for all data)");
			this->label3->Click += gcnew System::EventHandler(this, &Frame::label3_Click);
			// 
			// channel_3
			// 
			this->channel_3->AutoSize = true;
			this->channel_3->Checked = true;
			this->channel_3->CheckState = System::Windows::Forms::CheckState::Checked;
			this->channel_3->Location = System::Drawing::Point(410, 67);
			this->channel_3->Margin = System::Windows::Forms::Padding(4);
			this->channel_3->Name = L"channel_3";
			this->channel_3->Size = System::Drawing::Size(87, 22);
			this->channel_3->TabIndex = 4;
			this->channel_3->Text = L"Channel 3";
			this->channel_3->UseVisualStyleBackColor = true;
			// 
			// channel_2
			// 
			this->channel_2->AutoSize = true;
			this->channel_2->Checked = true;
			this->channel_2->CheckState = System::Windows::Forms::CheckState::Checked;
			this->channel_2->Location = System::Drawing::Point(409, 37);
			this->channel_2->Margin = System::Windows::Forms::Padding(4);
			this->channel_2->Name = L"channel_2";
			this->channel_2->Size = System::Drawing::Size(88, 22);
			this->channel_2->TabIndex = 3;
			this->channel_2->Text = L"Channel 2";
			this->channel_2->UseVisualStyleBackColor = true;
			// 
			// channel_1
			// 
			this->channel_1->AutoSize = true;
			this->channel_1->Checked = true;
			this->channel_1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->channel_1->Location = System::Drawing::Point(410, 8);
			this->channel_1->Margin = System::Windows::Forms::Padding(4);
			this->channel_1->Name = L"channel_1";
			this->channel_1->Size = System::Drawing::Size(87, 22);
			this->channel_1->TabIndex = 2;
			this->channel_1->Text = L"Channel 1";
			this->channel_1->UseVisualStyleBackColor = true;
			this->channel_1->CheckedChanged += gcnew System::EventHandler(this, &Frame::channel_1_CheckedChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(4, 9);
			this->label2->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(104, 18);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Format Options";
			this->label2->Click += gcnew System::EventHandler(this, &Frame::label2_Click);
			// 
			// format_options
			// 
			this->format_options->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->format_options->FormattingEnabled = true;
			this->format_options->Items->AddRange(gcnew cli::array< System::Object^  >(6) {
				L"WFDB 16", L"WFDB 32", L"WFDB 8", L"WFDB 212", L"HELX",
					L"ISHNE"
			});
			this->format_options->Location = System::Drawing::Point(4, 31);
			this->format_options->Margin = System::Windows::Forms::Padding(4);
			this->format_options->Name = L"format_options";
			this->format_options->Size = System::Drawing::Size(160, 26);
			this->format_options->TabIndex = 0;
			// 
			// Export
			// 
			this->Export->Location = System::Drawing::Point(318, 304);
			this->Export->Name = L"Export";
			this->Export->Size = System::Drawing::Size(99, 31);
			this->Export->TabIndex = 4;
			this->Export->Text = L"Export";
			this->Export->UseVisualStyleBackColor = true;
			this->Export->Click += gcnew System::EventHandler(this, &Frame::Export_Click);
			// 
			// Exit
			// 
			this->Exit->Location = System::Drawing::Point(423, 304);
			this->Exit->Name = L"Exit";
			this->Exit->Size = System::Drawing::Size(99, 31);
			this->Exit->TabIndex = 5;
			this->Exit->Text = L"Exit";
			this->Exit->UseVisualStyleBackColor = true;
			this->Exit->Click += gcnew System::EventHandler(this, &Frame::button2_Click);
			// 
			// export_length_tooltip
			// 
			this->export_length_tooltip->AutoPopDelay = 5000;
			this->export_length_tooltip->InitialDelay = 500;
			this->export_length_tooltip->ReshowDelay = 100;
			// 
			// Frame
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 18);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(535, 348);
			this->Controls->Add(this->Exit);
			this->Controls->Add(this->Export);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->Instructions);
			this->Controls->Add(this->pictureBox1);
			this->Font = (gcnew System::Drawing::Font(L"Corbel", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(4);
			this->Name = L"Frame";
			this->Load += gcnew System::EventHandler(this, &Frame::Frame_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->Close();
	}
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
	{
	}

	private: System::Void Refresh_Click(System::Object^  sender, System::EventArgs^  e)
	{
		// Refresh the list of removable drives currently attached to the computer.
		UINT driveType;
		WCHAR* drive_string_ptr;
		WCHAR driveStrings[1024];

		// Fetch all drive strings    
		GetLogicalDriveStrings(1024, driveStrings);
		drive_string_ptr = driveStrings;

		this->removable_drives->Items->Clear();

		// Loop until we find the final '\0'
		// driveStrings is a double null terminated list of null terminated strings)
		while (*drive_string_ptr)
		{
			// Dump drive information
			driveType = GetDriveType(drive_string_ptr);


			if (DRIVE_REMOVABLE == driveType && dirExists(System::IO::Path::Combine(gcnew String(drive_string_ptr), TOP_FOLDER_NAME)))
			{
				// Add to array of contents in the combobox
				this->removable_drives->Items->Add(gcnew String(drive_string_ptr));
			}

			// Move to next drive string
			// +1 is to move past the null at the end of the string.
			drive_string_ptr += lstrlen(drive_string_ptr) + 1;
		}

		// Automatically select the top item in the list.
		if (removable_drives->Items->Count > 0)
		{
			removable_drives->SelectedItem = removable_drives->Items[0];
		}
		else
		{
			removable_drives->SelectedItem = nullptr;

			// Only show this if the app is already shown, not when it is opened the first time. 
			// This check is a proxy for that since it is initialized later in the constructor. 
			if (format_options->SelectedItem)
			{
				MessageBox::Show("No drive containing an ecgs folder could be found.", "Error");
			}
		}
	}

	private: System::Void Export_Click(System::Object^  sender, System::EventArgs^  e)
	{
		Dictionary<int, Boolean>^ channels = gcnew Dictionary<int, Boolean>();
		double hours = NO_LIMIT;
		uint32_t num_channels = 0;

		// get an output file
		String^ drive = dynamic_cast<String^>(this->removable_drives->SelectedItem);
		String^ format = dynamic_cast<String^>(this->format_options->SelectedItem);
		if (String::IsNullOrEmpty(drive))
		{
			MessageBox::Show("Please select a drive.", "Error");
			return;
		}
		else if (!dirExists(System::IO::Path::Combine(drive, TOP_FOLDER_NAME)))
		{
			MessageBox::Show("Please select a drive containing valid ECG data.", "Error");
			return;
		}

		channels[1] = (this->channel_1->Checked);
		channels[2] = (this->channel_2->Checked);
		channels[3] = (this->channel_3->Checked);
		for each (auto kv in channels)
		{
			if (kv.Value)
			{
				++num_channels;
			}
		}

		if (format == "WFDB 212" && 2 != num_channels)
		{
			MessageBox::Show("WFDB 212 format requires exactly 2 channels.", "Error");
			return;
		}

		if (!String::IsNullOrWhiteSpace(max_export_length->Text))
		{
			try
			{
				hours = max(0, double::Parse(max_export_length->Text));
			}
			catch (FormatException^)
			{
				MessageBox::Show("Maximum Export Length Invalid.", "Error");
				return;
			}
		}

		SaveFileDialog^ saveFileDialog1 = gcnew SaveFileDialog;
		if (format->StartsWith("WFDB"))
		{
			saveFileDialog1->Filter = "Waveform Database files (*.dat)|*.dat|All files (*.*)|*.*";
		}
		else if (format->StartsWith("ISHNE"))
		{
			saveFileDialog1->Filter = "ISHNE Files (*.ecg)|*.ecg|All files (*.*)|*.*";
		}
		else
		{
			saveFileDialog1->Filter = "All files (*.*) | *.*";
		}
		saveFileDialog1->FilterIndex = 1;
		saveFileDialog1->RestoreDirectory = true;
		if (saveFileDialog1->ShowDialog() == Windows::Forms::DialogResult::OK)
		{
			// For wfdb files, check to see if a header file exists, and confirm overwrite of it if that is the case.
			String^ header_filename = String::Concat((saveFileDialog1->FileName->Substring(0, saveFileDialog1->FileName->LastIndexOf("."))), WFDB_HEADER_EXTENSION);
			if (format->StartsWith("WFDB") && (IO::File::Exists(header_filename) &&
				MessageBox::Show(String::Concat(header_filename, " already exists. Overwrite it?"), "Confirm Overwrite", MessageBoxButtons::YesNo)
				!= Windows::Forms::DialogResult::Yes))
			{
				// The user did not want to overwrite the header file. 
				return;
			}

			ProgressPopup^ popup = gcnew ProgressPopup(drive, saveFileDialog1->FileName, format, channels, hours);
			popup->ShowDialog();
		}
	}
	private: System::Void channel_1_CheckedChanged(System::Object^  sender, System::EventArgs^ e) {
	}
	private: System::Void Frame_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label3_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void Instructions_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	};
}

#endif // FRAME_H
