/******************************************************************************
*       Copyright (c) 2014, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

/*
 * Program Structure Overview.
 *
 * The purpose of this program is to convert SCP files into one of a number of formats,
 * such as WFDB or ISHNE.
 * The program starts at wWinMain (necessary for a Windows GUI application), where it
 * checks if it has any command line arguments. If it does, it parses them in the wWinMain
 * function and then sends them on to export_data, which creates the list of files to
 * parse, then passes them on to the SCP_Parser object. If it does not, it launches
 * a GUI for the user to interact with.
 *
 * This SCP_Parser object contains all the functionality for reading scp files and
 * writing wfdb8, wfdb16, wfdb212, ishne, and helx file formats. This class is written
 * in portable C++ and has been tested on both GCC and MSVCPP. In this converter
 * program, the functionality is accessed by calling the export_file_list function,
 * which accepts a list of files and some formatting data, then exports all the
 * files, in order, to a designated output file. The object does have many other
 * functions for writing data and parsing files more granularly, however.
 *
 * In order to add an additional format to the SCP_Parser class, [format]_write_header, [format]_write_square_wave,
 * and [format]_write_data functions must be implemented (using the appropriate typedef'd signature:
 * search SCP_Parser::*write_square_wave_function for an example), and the format must be added to the
 * file_format_t enum in SCP_Parser.h. Then, in the object's constructor, the functions must be registered
 * with the object in the same way as the already existing formats (this will be easy when looking
 * at the constructor code). The string_to_format and get_header_filename functions should also
 * be updated to accomodate the new format. For the GUI, only the format_options ComboBox need be updated.
 */

#include "helxForm.h"

 // Include the Shell32 library.
#pragma comment( lib, "Shell32.lib" )
#pragma comment( lib, "Shlwapi.lib" )

using namespace System;
using namespace System::Windows::Forms;
using namespace System::IO;
using namespace System::ComponentModel;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

enum  optionIndex { UNKNOWN = 0, HELP, FORMAT, CHANNELS, OUTPUT_FILE, DRIVE, INPUT_FOLDER };

[STAThread]
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
    Application::Run(gcnew HELXFirstTimeSetup::helxSetup());
    return 0;
}