#pragma once

#include <windows.h>
#include <malloc.h>
#include <stdio.h>
#include <locale>
#include <codecvt>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <shlobj.h>
#include <iostream>
#include <sstream>

#define MAX_LINE_WIDTH (600)

#pragma comment(lib, "User32.lib")

namespace HELXFirstTimeSetup 
{
    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Collections::Generic;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;
    using namespace System::IO;

	/// <summary>
	/// Summary for helxSetup
	/// </summary>
	public ref class helxSetup : public System::Windows::Forms::Form
	{
	public:
		helxSetup(void)
		{
			InitializeComponent();
			
            std::string nemonDir("C:\\nm");
            DWORD ftyp = GetFileAttributesA(nemonDir.c_str());
            if ((ftyp != INVALID_FILE_ATTRIBUTES) && (ftyp & FILE_ATTRIBUTE_DIRECTORY))
            {
                driveFound = true;

                //Update form to allow plugin installation.
                this->SuspendLayout();
                this->buttonInstall->Enabled = true;
                this->buttonBrowse->Enabled = true;
                this->textBox1->Text = L"It looks like you have HE/LX Analysis Software installed. "
                    L"Would you like to install the TZ Medical Monitor importer plugin for HE/LX\?\r\n";
                this->ResumeLayout(false);
                this->PerformLayout();
            }
            else
            {
                driveFound = false;

                //Update form to allow plugin installation.
                this->SuspendLayout();
                this->buttonInstall->Enabled = false;
                this->buttonBrowse->Enabled = true;
                this->textBox1->Text = L"Would you like to search for HE/LX Analysis on your system\?\r\n";
                this->ResumeLayout(false);
                this->PerformLayout();
            } 
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~helxSetup()
		{
			if (components)
			{
				delete components;
			}
		}

    protected:

	private:
        boolean driveFound = false;		

#pragma region Windows Form Designer generated code

        System::ComponentModel::Container^ components;
        System::Windows::Forms::TextBox^ textBox1;
        System::Windows::Forms::Button^ buttonInstall;
    private: System::Windows::Forms::FolderBrowserDialog^ folderBrowserDialog1;
    private: System::Windows::Forms::Button^ buttonBrowse;

           System::Windows::Forms::Button^ buttonQuit;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            this->textBox1 = (gcnew System::Windows::Forms::TextBox());
            this->buttonInstall = (gcnew System::Windows::Forms::Button());
            this->buttonQuit = (gcnew System::Windows::Forms::Button());
            this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
            this->buttonBrowse = (gcnew System::Windows::Forms::Button());
            this->SuspendLayout();
            // 
            // textBox1
            // 
            this->textBox1->BackColor = System::Drawing::SystemColors::Menu;
            this->textBox1->BorderStyle = System::Windows::Forms::BorderStyle::None;
            this->textBox1->Enabled = false;
            this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
                static_cast<System::Byte>(0)));
            this->textBox1->ForeColor = System::Drawing::SystemColors::MenuText;
            this->textBox1->Location = System::Drawing::Point(12, 12);
            this->textBox1->Multiline = true;
            this->textBox1->Name = L"textBox1";
            this->textBox1->RightToLeft = System::Windows::Forms::RightToLeft::No;
            this->textBox1->Size = System::Drawing::Size(370, 51);
            this->textBox1->TabIndex = 0;
            this->textBox1->TabStop = false;
            this->textBox1->Text = L"It doesn\'t look like you have the HE/LX Analysis Software installed.";
            // 
            // buttonInstall
            // 
            this->buttonInstall->Enabled = false;
            this->buttonInstall->Location = System::Drawing::Point(286, 69);
            this->buttonInstall->Name = L"buttonInstall";
            this->buttonInstall->Size = System::Drawing::Size(96, 24);
            this->buttonInstall->TabIndex = 3;
            this->buttonInstall->Text = L"Install Plugin";
            this->buttonInstall->UseVisualStyleBackColor = true;
            this->buttonInstall->Click += gcnew System::EventHandler(this, &helxSetup::button1_Click);
            // 
            // buttonQuit
            // 
            this->buttonQuit->Location = System::Drawing::Point(82, 69);
            this->buttonQuit->Name = L"buttonQuit";
            this->buttonQuit->Size = System::Drawing::Size(96, 24);
            this->buttonQuit->TabIndex = 1;
            this->buttonQuit->Text = L"Quit";
            this->buttonQuit->UseVisualStyleBackColor = true;
            this->buttonQuit->Click += gcnew System::EventHandler(this, &helxSetup::button2_Click);
            // 
            // folderBrowserDialog1
            // 
            this->folderBrowserDialog1->Description = L"Browse for HeLx Folder";
            this->folderBrowserDialog1->SelectedPath = L"C://";
            this->folderBrowserDialog1->ShowNewFolderButton = false;
            // 
            // buttonBrowse
            // 
            this->buttonBrowse->Enabled = false;
            this->buttonBrowse->Location = System::Drawing::Point(184, 69);
            this->buttonBrowse->Name = L"buttonBrowse";
            this->buttonBrowse->Size = System::Drawing::Size(96, 24);
            this->buttonBrowse->TabIndex = 2;
            this->buttonBrowse->Text = L"Browse";
            this->buttonBrowse->UseVisualStyleBackColor = true;
            this->buttonBrowse->Click += gcnew System::EventHandler(this, &helxSetup::button_Browse_Click);
            // 
            // helxSetup
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(394, 105);
            this->ControlBox = false;
            this->Controls->Add(this->buttonBrowse);
            this->Controls->Add(this->buttonQuit);
            this->Controls->Add(this->buttonInstall);
            this->Controls->Add(this->textBox1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
            this->Name = L"helxSetup";
            this->Text = L"HE/LX Plugin Installer";
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion

        //"Install" the plugin
        private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
        {
            std::wstring dstDir = L"";

            //Let the user select a folder if driveFound is not set.
            if (driveFound)
            {
                dstDir = L"C:\\nm\\bin";
                InstallAtLocation(dstDir);
            }
        }

        private: System::Void CopyPasteFile(std::wstring sourceDir, std::wstring destDir, std::wstring fileName)
        {
            CopyPasteFile(sourceDir, destDir, fileName, fileName);
        }

        private: System::Void CopyPasteFile(std::wstring sourceDir, std::wstring destDir,
                                            std::wstring sourceFileName, std::wstring destFileName)
        {
            CopyFile((sourceDir + L"\\" + sourceFileName).c_str(), (destDir + L"\\" + destFileName).c_str(), FALSE);
        }

        private: System::Void InstallAtLocation(std::wstring dstDir)
        {
            wchar_t currentPath[MAX_PATH];
            wchar_t currentDirectory[MAX_PATH];
            wchar_t currentDrive[_MAX_DRIVE];

            GetModuleFileNameW(NULL, currentPath, MAX_PATH);
            _wsplitpath(currentPath, currentDrive, currentDirectory, NULL, NULL);

            std::wstring srcDir(currentDrive);
            srcDir = srcDir.append(currentDirectory);


            std::ifstream helxSettingsOld;
            std::ofstream helxSettingsNew;

            CopyPasteFile(srcDir, dstDir, L"TZMM Holter Exporter.exe", L"TZMM.exe");
            CopyPasteFile(srcDir, dstDir, L"userimport_tzmm.bat");
            CopyPasteFile(srcDir, dstDir, L"MSVCP140.dll");
            CopyPasteFile(srcDir, dstDir, L"VCRUNTIME140.dll");
            CopyPasteFile(dstDir + L"\\Plugins_Available", dstDir + L"\\plugins", L"foreignrecorderimportplugin.jar");
            CopyPasteFile(srcDir, dstDir + L"\\tmp", L"import_pat.001", L"import_pat.001");
            CopyPasteFile(dstDir, dstDir, L"h4w.ini", L"h4w_bak.ini");

            helxSettingsOld.open(dstDir + L"\\h4w_bak.ini");
            helxSettingsNew.open(dstDir + L"\\h4w.ini");

            char line[MAX_LINE_WIDTH];
            std::string lineString("");
            bool lineFound = FALSE;

            char* newSettingLine = "FRIImportCommand      = userimport_tzmm.bat $PatientDirectory\n";

            while (helxSettingsOld.getline(line, std::streamsize(MAX_LINE_WIDTH)))
            {
                helxSettingsNew.write(line, std::strlen(line));
                helxSettingsNew.write("\n", 1);

                lineString = line;

                if ("[Settings]" == lineString.substr(0, 10))
                {
                    lineFound = true;
                    helxSettingsNew.write(newSettingLine, std::strlen(newSettingLine));
                }
            }

            helxSettingsOld.close();
            helxSettingsNew.close();

            //Update form now that plugins are installed.
            this->SuspendLayout();
            this->buttonInstall->Enabled = false;
            this->buttonQuit->Text = L"Done";
            this->textBox1->Text = L"Plugins installed!\r\n ";
            this->ResumeLayout(false);
            this->PerformLayout();
        }

        //Cancel
        private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
        {
            this->Close();
        }
        
        private: System::Void button_Browse_Click(System::Object^ sender, System::EventArgs^ e) 
        {
            std::wstring dstDir = L"";
            driveFound = false;

            //Let the user select a folder if driveFound is not set.
            Windows::Forms::DialogResult result = folderBrowserDialog1->ShowDialog();
            if (Windows::Forms::DialogResult::OK == result)
            {
                System::String^ selectedDir = folderBrowserDialog1->SelectedPath;
                array<wchar_t>^ chars = selectedDir->ToCharArray();

                for (int i = 0; i < chars->Length; i++)
                {
                    dstDir += ((wchar_t)chars->GetValue(i));
                }
            }

            dstDir += L"\\bin";

            DWORD ftyp = GetFileAttributesW(dstDir.c_str());
            if ((ftyp != INVALID_FILE_ATTRIBUTES) && (ftyp & FILE_ATTRIBUTE_DIRECTORY))
            {
                driveFound = true;
            }

            if (!driveFound)
            {
                this->SuspendLayout();
                this->textBox1->Text = L"That's not the right folder. Look again?";
                this->ResumeLayout(false);
                this->PerformLayout();
            }
            else
            {
                InstallAtLocation(dstDir);
            }
        }
    };
}
